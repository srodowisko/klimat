import unittest
from planner4ucore.services.tests import test_services


def get_services_test_suite():
    return unittest.findTestCases(test_services)

