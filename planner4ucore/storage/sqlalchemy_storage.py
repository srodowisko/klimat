"""
    This module provides class and functions
    for accessing database using SQLAlchemy ORM

    Classes:
        SQLAlchemyStorage - contains core functions to work with data

    Functions:
        session_scope - context manager to work with session (creates, commits and closes)
        session_injector - decorator, which injects session object in function, if it wasn"t provided

    Global variables:
        Session - class of SQLAlchemy session maker to make database quires, configured in SQLAlchemyStorage class

"""
from datetime import timedelta
from datetime import datetime
from contextlib import contextmanager
import logging

from sqlalchemy.orm import relationship
from sqlalchemy.orm import mapper
from sqlalchemy.orm import clear_mappers
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy.orm import sessionmaker
from sqlalchemy import and_
from sqlalchemy import desc
from sqlalchemy import create_engine
from sqlalchemy import MetaData
from sqlalchemy import Column, Date, ForeignKey, BigInteger, String, Table, Time, text

from planner4ucore.models import models as mo
from planner4ucore.services import log_functions

Session = sessionmaker()


@contextmanager
def session_scope():
    """
        Context manager for work with session object. Automatically
        creates and returns it when entering "with" statement;
        and commits and closes it when exiting

        If there was exception, committing changes, transaction
        is rolled back and exception is re-raised

        Raises:
            Exception: if couldn"t save changes in database

    """
    session = Session()
    try:
        yield session
        session.commit()
    except Exception as e:
        session.rollback()
        raise e
    finally:
        session.close()


def session_injector(func):
    """
        Decorator which injects session object into function
        as named argument if it wasn"t provided, using session_scope()
        function.

        Attention: if you want to pass your session to function, assign it as
        named argument, when invoke function (example: func(session=my_session))

    """
    def injected_function(*args, **kwargs):
        if "session" not in kwargs:
            try:
                with session_scope() as session:
                    return func(*args, **kwargs, session=session)
            except SQLAlchemyError as e:
                lib_logger = log_functions.get_planner4u_logger()
                lib_logger.exception(("Exception in SQLAlchemyStorage ({func_name}()): {msg}"
                                      .format(func_name=func.__name__, msg=e.args)), exc_info=False)
                raise
        else:
            return func(*args, **kwargs)
    return injected_function


class SQLAlchemyStorage:
    """
        This class contains functions which allow
        add, update, delete, retrieve and perform
        simple operations on data models stored in
        database. By default initialized by models
        from models.models module

        Attributes:

            engine - connector defined by connection string
            database_meta - database metadata (tables, columns, etc.)

            # Models classes:
            condition_task_relation_class
            group_class
            notification_class
            plan_class
            reminder_class
            subtask_relation_class
            task_date_class
            task_group_relation_class
            message_class
            task_class
            user_class
            user_executor_relation_class

            # Tables:
            condition_task_relation_table
            group_table
            notification_table
            plan_table
            reminder_table
            subtask_relation_table
            task_date_table
            task_group_relation_table
            message_table
            task_table
            user_executor_relation_table
            user_table

        Each function has optional argument session=None,
        which is injected by session_injector() function if None.
        If you want to provide you instance of session,
        pass it as a named argument (session=my_session)

        For most update function you have to pass id of object and
        dictionary, where key is attribute name and value is new value

        Functions:
            # Condition task relation
            get_condition_task_relation - returns condition task relation by relation id
            get_condition_task_relation_by_defined - returns condition task relation by defined task id
            add_condition_task_relation - adds passed condition task relation
            get_task_condition_relations - returns all relations where specified task is condition
            delete_condition_task_relation - deletes specified by id relation
            update_condition_task_relation - updates condition relation, by passed dictionary

            # Group
            get_group - returns group by id
            get_user_groups - returns groups where specified user is creator
            get_task_groups - returns groups where specified task is
            get_group_tasks - returns all tasks of the group
            add_group - adds passed group to storage
            delete_group - deletes group by id
            update_group - updates group by passed dictionary

            # Notification
            get_notification - returns notification by its id
            get_date_notifications - returns notifications belonging to specified date
            get_task_notifications - returns notifications belonging to specified task
            get_user_creator_notifications - returns notifications for user-creator
            get_user_executor_notifications - returns notifications connected with tasks where he is executor
            get_ripe_notifications - returns notifications which should be raised due to date
            get_user_executors_to_notify - returns all users-executors connected with this notification
            get_user_creator_to_notify - returns user-creator of task with which notification connected
            add_notification - adds new notification, returns it
            delete_notification - deletes specified by id notification
            update_notification - updates notification by passed dictionary

            # Reminder
            get_reminder - returns reminder by specified id
            get_task_reminders - returns reminders connected with specified task
            get_user_creator_reminders - returns all reminders for user-creator
            get_user_executor_reminders - returns all reminders for user-executor
            get_ripe_reminders - returns all reminders which should be raised due to date
            get_task_ripe_reminders - returns all connected with task reminders wich should be raised
            get_users_executors_to_remind - returns all users-executors connected with reminder
            get_user_creator_to_remind - returns user-creator of task which is connected with reminder
            add_reminder - adds reminder to storage, and returns its instance
            delete_reminder - deletes reminder by specified id
            update_reminder - updates reminder by passed dictionary

            # Subtask relations
            add_subtask_relation - adds relation and returns its instance
            get_subtask_relation_by_id - returns subtask relation by specified relation id
            get_subtask_relation - returns relation specified by parent and child tasks ids
            get_task_subtask_relations - returns all parent-child relations of task (task is parent)
            delete_subtask_relation_by_id - delete relation specifying its id
            delete_subtask_relation - delete relation specifying parent and child tasks ids
            update_subtask_relation - updates relation by passed dictionary

            # Task date
            get_date - returns date by id
            get_task_dates - returns all dates connected with task
            get_overdue_dates - returns dates which is overdue
            get_date_task - returns task with which date is connected
            add_date - adds date and returns its instance
            delete_date - deletes date by id
            update_date - updates date by passed dictionary

            # Task group relation
            add_task_group_relation - adds relation and returns its instance
            get_task_group_relation - returns relation by specified id
            delete_task_group_relation_by_id - deletes relation by its id
            delete_task_group_relation - deletes relation by group and task ids
            update_task_group_relation - updates task group relation by passed dictionary

            # Task messages
            get_task_message - returns message by id
            get_user_task_messages - returns all user"s messages
            add_task_message - adds task message and returns its instance
            delete_task_message - deletes task message by specified id

            # Task
            get_task - returns task by its id
            get_user_tasks - returns tasks where user is creator
            get_user_executable_tasks - returns tasks where user is executor
            get_sub_tasks - returns sub tasks of specified task
            get_parent_tasks - returns parent tasks of specified task
            add_task - adds task and returns its instance
            delete_task - deletes task by id
            update_task - updates task by passed dictionary

            # User executor relation
            add_user_executor_relation - adds relation and returns its instance
            get_user_executor_relation - returns relation by its id
            delete_user_executor_relation_by_id - deletes relation by its id
            delete_user_executor_relation - deletes relation by user-executor and task ids
            update_user_executor_relation - updates relation by passed dictionary

            # User
            get_user - returns user by id
            get_users_executors - returns users-executors of specified task
            add_user - adds new user and returns its instance
            delete_user - deletes user by id
            update_users - updates user by passed dictionary

            is_duplication - checks whether the model with specified attributes exists in table

    """
    def __init__(self,
                 connection_string,
                 condition_task_relation_class=mo.ConditionTaskRelation,
                 group_class=mo.Group,
                 notification_class=mo.Notification,
                 plan_class=mo.Plan,
                 reminder_class=mo.Reminder,
                 subtask_relation_class=mo.SubtaskRelation,
                 task_date_class=mo.TaskDate,
                 task_group_relation_class=mo.TaskGroupRelation,
                 message_class=mo.Message,
                 task_class=mo.Task,
                 user_class=mo.User,
                 user_executor_relation_class=mo.UserExecutorRelation):
        """
            Args:
                condition_task_relation_class = models.CondititonTaskRelation
                group_class = models.Group
                notification_class = models.Notification
                plan_class = models.Plan
                reminder_class = models.Reminder
                subtask_relation_class = models.SubtaskRelation
                task_date_class = models.TaskDate
                task_group_relation_class = models.TaskGroupRelation
                message_class = models.Message
                task_class = models.Task
                user_class = models.User
                user_executor_relation_class = models.ExecutorRelation
                connection_string

        """
        # Configure connection
        self.engine = create_engine(connection_string)
        Session.configure(bind=self.engine, expire_on_commit=False)
        self.database_meta = MetaData(bind=self.engine)

        # Save models classes
        self.condition_task_relation_class = condition_task_relation_class
        self.group_class = group_class
        self.notification_class = notification_class
        self.plan_class = plan_class
        self.reminder_class = reminder_class
        self.subtask_relation_class = subtask_relation_class
        self.task_date_class = task_date_class
        self.task_group_relation_class = task_group_relation_class
        self.message_class = message_class
        self.task_class = task_class
        self.user_class = user_class
        self.user_executor_relation_class = user_executor_relation_class

        # Get tables metadata from database
        self.database_meta.reflect()

        # Get tables classes
        self.condition_task_relation_table = self.database_meta.tables["ConditionTaskRelations"]
        self.group_table = self.database_meta.tables["Groups"]
        self.notification_table = self.database_meta.tables["Notifications"]
        self.plan_table = self.database_meta.tables["Plans"]
        self.reminder_table = self.database_meta.tables["Reminders"]
        self.subtask_relation_table = self.database_meta.tables["SubtaskRelations"]
        self.task_date_table = self.database_meta.tables["TaskDates"]
        self.task_group_relation_table = self.database_meta.tables["TaskGroupRelations"]
        self.message_table = self.database_meta.tables["Messages"]
        self.task_table = self.database_meta.tables["Tasks"]
        self.user_executor_relation_table = self.database_meta.tables["UserExecutorRelations"]
        self.user_table = self.database_meta.tables["Users"]

        # Clear mappings if neccessary
        clear_mappers()

        # Map model classes to tables
        # Map ConditionTaskRelations table
        mapper(self.condition_task_relation_class, self.condition_task_relation_table,
               properties={
                   "condition_task":
                       relationship(self.task_class, primaryjoin=self.condition_task_relation_table.c.condition_task_id
                                    == self.task_table.c.task_id),
                   "defined_task":
                       relationship(self.task_class, primaryjoin=self.condition_task_relation_table.c.defined_task_id
                                    == self.task_table.c.task_id)
               })

        # Map Groups table
        mapper(self.group_class, self.group_table,
               properties={
                   "user_creator": relationship(self.user_class)
               })

        # Map Notifications table
        mapper(self.notification_class, self.notification_table,
               properties={
                   "date": relationship(self.task_date_class)
               })

        # Map TaskDates table
        mapper(self.task_date_class, self.task_date_table,
               properties={
                   "task": relationship(self.task_class)
               })

        # Map Tasks table
        mapper(self.task_class, self.task_table,
               properties={
                   "User": relationship(self.user_class)
               })

        # Map SubtasksRelations table
        mapper(self.subtask_relation_class, self.subtask_relation_table,
               properties={
                   "sub_task":
                       relationship(self.task_class,
                                    primaryjoin=self.subtask_relation_table.c.sub_task_id == self.task_table.c.task_id),
                   "parent_task":
                       relationship(self.task_class, primaryjoin=self.subtask_relation_table.c.parent_task_id
                                    == self.task_table.c.task_id)

               })

        # Map Users table
        mapper(self.user_class, self.user_table)

        # Map UserExecutorRelations table
        mapper(self.user_executor_relation_class, self.user_executor_relation_table,
               properties={
                   "task": relationship(self.task_class, ),
                   "user_executor": relationship(self.user_class)
               })

        # Map TaskMessages table
        mapper(self.message_class, self.message_table,
               properties={
                   "sender": relationship(self.user_class, primaryjoin=self.message_table.c.sender_id
                                          == self.user_table.c.user_id),
                   "user": relationship(self.user_class, primaryjoin=self.message_table.c.user_id
                                        == self.user_table.c.user_id)
               })

        # Map GroupRelations table
        mapper(self.task_group_relation_class, self.task_group_relation_table,
               properties={
                   "group": relationship(self.group_class),
                   "task": relationship(self.task_class)
               })

        # Map Reminders table
        mapper(self.reminder_class, self.reminder_table,
               properties={
                   "task": relationship(self.task_class)
               })

        # Map Plans table
        mapper(self.plan_class, self.plan_table,
               properties={
                   "task": relationship(self.task_class)
               })

    # region ConditionTaskRelations quires
    @session_injector
    def get_condition_task_relation(self, relation_id, session=None):
        return session.query(self.condition_task_relation_class).get(relation_id)

    def get_condition_task_relations(self, session=None):
        return session.query(self.condition_task_relation_class).all()

    @session_injector
    def get_condition_task_relation_by_defined(self, defined_task_id, session=None):
        return session.query(self.condition_task_relation_class).filter_by(defined_task_id=defined_task_id).first()

    @session_injector
    def add_condition_task_relation(self, condition_task_relation, session=None):
        session.add(condition_task_relation)
        return condition_task_relation

    @session_injector
    def get_task_condition_relations(self, task_id, session=None):
        return session.query(self.condition_task_relation_class).filter_by(condition_task_id=task_id).all()

    @session_injector
    def delete_condition_task_relation(self, relation_id, session=None):
        delete_query = (session.query(self.condition_task_relation_class)
                        .filter(self.condition_task_relation_class.relation_id == relation_id))
        return self.execute_delete_query(delete_query)

    @session_injector
    def update_condition_task_relation(self, relation_id, args, session=None):
        session.query(self.condition_task_relation_class).filter_by(relation_id=relation_id).update(args)
        return session.query(self.condition_task_relation_class).get(relation_id)
    # endregion

    # region Groups quires
    @session_injector
    def get_group(self, group_id, session=None):
        return session.query(self.group_class).get(group_id)

    @session_injector
    def get_user_groups(self, user_id, session=None):
        return session.query(self.group_class).filter_by(user_creator_id=user_id).all()

    @session_injector
    def get_task_groups(self, user_id, task_id, session=None):
        return (session.query(self.group_class)
                .join(self.task_group_relation_class)
                .filter(and_(self.task_group_relation_class.task_id == task_id,
                             self.group_class.user_creator_id == user_id))).all()

    @session_injector
    def get_group_tasks(self, group_id, session=None):
        return (session.query(self.task_class)
                .join(self.task_group_relation_class)
                .filter(self.task_group_relation_class.group_id == group_id)).all()

    @session_injector
    def add_group(self, group, session=None):
        session.add(group)
        return group

    @session_injector
    def delete_group(self, group_id, session=None):
        delete_query = session.query(self.group_class).filter_by(group_id=group_id)
        return self.execute_delete_query(delete_query)

    @session_injector
    def update_group(self, group_id, args, session=None):
        session.query(self.group_class).filter_by(group_id=group_id).update(args)
        return session.query(self.group_class).get(group_id)

    @session_injector
    def delete_task_from_user_groups(self, user_id, task_id, session=None):
        relations_to_delete = (session.query(self.task_group_relation_class)
                               .join(self.group_class).join(self.user_class)
                               .filter(and_(self.task_group_relation_class.task_id==task_id,
                                            self.user_class.user_id==user_id)))
        relations_ids_to_delete = [relation.relation_id for relation in relations_to_delete]
        for id in relations_ids_to_delete:
            session.query(self.task_group_relation_class).filter_by(relation_id=id).delete()
    # endregion

    # region Notifications quires
    @session_injector
    def get_notification(self, notification_id, session=None):
        return session.query(self.notification_class).get(notification_id)

    @session_injector
    def get_date_notifications(self, date_id, session=None):
        return session.query(self.notification_class).filter(date_id=date_id).all()

    @session_injector
    def get_task_notifications(self, task_id, session=None):
        return (session.query(self.notification_class)
                .join(self.task_date_class)
                .join(self.task_class)
                .filter(self.task_class.task_id == task_id)).all()

    @session_injector
    def get_user_creator_notifications(self, user_id, session=None):
        return (session.query(self.notification_class)
                .join(self.task_date_class)
                .join(self.task_class)
                .filter(self.task_class.user_creator == user_id)).all()

    @session_injector
    def get_user_executor_notifications(self, user_id, session=None):
        return (session.query(self.notification_class)
                .join(self.task_date_class)
                .join(self.task_class)
                .join(self.user_executor_relation_class)
                .filter(self.user_executor_relation_class.user_executor_id == user_id)).all()

    @session_injector
    def get_ripe_notifications(self, date=None, time=None, session=None):
        date = datetime.now().date() if not date else date
        time = datetime.now().time() if not time else time
        # Get list of tuples (notification, notification date)
        joined_notifications = (session.query(self.notification_class, self.task_date_class)
                                .join(self.task_date_class)).all()
        ripe_notifications = []
        # Check, if notification should be raised
        for joined_notification in joined_notifications:
            notification = joined_notification[0]
            if notification.used == int(True):
                continue
            task_date = joined_notification[1]
            notify_date = task_date.date - timedelta(days=notification.days_to_date)
            if date > notify_date or (date == notify_date and time >= notification.time):
                ripe_notifications.append(notification)

        return ripe_notifications

    @session_injector
    def get_users_executors_to_notify(self, notification_id, session=None):
        return (session.query(self.user_class)
                .join(self.user_executor_relation_class)
                .join(self.task_class)
                .join(self.task_date_class)
                .join(self.notification_class)
                .filter(self.notification_class.notification_id == notification_id)).all()

    @session_injector
    def get_user_creator_to_notify(self, notification_id, session=None):
        return session.query(self.notification_class).get(notification_id).date.task.User

    @session_injector
    def add_notification(self, notification, session=None):
        session.add(notification)
        return notification

    @session_injector
    def delete_notification(self, notification_id, session=None):
        delete_query = session.query(self.notification_class).filter_by(notification_id=notification_id)
        return self.execute_delete_query(delete_query)

    @session_injector
    def update_notification(self, notification_id, args, session=None):
        session.query(self.notification_class).filter_by(notification_id=notification_id).update(args)
        return session.query(self.notification_class).get(notification_id)
    # endregion

    # region Plans quires
    @session_injector
    def get_plan(self, plan_id, session=None):
        return session.query(self.plan_class).get(plan_id)

    @session_injector
    def get_task_plan(self, task_id, session=None):
        return session.query(self.plan_class).filter_by(task_id=task_id).first()

    @session_injector
    def get_ripe_plans(self, date=None, session=None):
        date = datetime.now().date() if not date else date
        return session.query(self.plan_class).filter(and_(date >= self.plan_class.create_task_date,
                                                          self.plan_class.active == int(True))).all()

    @session_injector
    def get_user_plans(self, user_id, session=None):
        return (session.query(self.plan_class)
                .join(self.task_class)
                .filter(self.task_class.user_creator == user_id)).all()

    @session_injector
    def add_plan(self, plan, session=None):
        session.add(plan)
        return plan

    @session_injector
    def delete_plan(self, plan_id, session=None):
        delete_query = session.query(self.plan_class).filter_by(plan_id=plan_id)
        return self.execute_delete_query(delete_query)

    @session_injector
    def update_plan(self, plan_id, args, session=None):
        session.query(self.plan_class).filter_by(plan_id=plan_id).update(args)
        return session.query(self.plan_class).get(plan_id)
    # endregion

    # region Reminders quires
    @session_injector
    def get_reminder(self, reminder_id, session=None):
        return session.query(self.reminder_class).get(reminder_id)

    @session_injector
    def get_task_reminders(self, task_id, session=None):
        return session.query(self.reminder_class).filter_by(task_id=task_id).all()

    @session_injector
    def get_user_creator_reminders(self, user_id, session=None):
        return (session.query(self.reminder_class)
                .join(self.task_class)
                .filter(self.task_class.user_creator == user_id)).all()

    @session_injector
    def get_user_executor_reminders(self, user_id, session=None):
        return (session.query(self.reminder_class)
                .join(self.task_class)
                .join(self.user_executor_relation_class)
                .filter(self.user_executor_relation_class.user_executor_id == user_id)).all()

    def __get_ripe_reminders(self, session, date, time):
        # Should return query, not list (don"t user all())
        return (session.query(self.reminder_class)
                .filter(and_(date >= self.reminder_class.raise_date,
                             time >= self.reminder_class.raise_time,
                             self.reminder_class.active == int(True))))

    @session_injector
    def get_ripe_reminders(self, date=None, time=None, session=None):
        date = datetime.now().date() if not date else date
        time = datetime.now().time() if not time else time
        return self.__get_ripe_reminders(session, date, time).all()

    @session_injector
    def get_task_ripe_reminders(self, task_id, date=None, time=None, session=None):
        date = datetime.now().date() if not date else date
        time = datetime.now().time() if not time else time
        ripe_reminders = self.__get_ripe_reminders(session, date, time)
        return ripe_reminders.filter_by(task_id=task_id).all()

    @session_injector
    def get_users_executors_to_remind(self, reminder_id, session=None):
        return (session.query(self.user_class)
                .join(self.user_executor_relation_class)
                .join(self.task_class)
                .join(self.reminder_class)
                .filter(self.reminder_class.reminder_id == reminder_id)).all()

    @session_injector
    def get_user_creator_to_remind(self, reminder_id, session=None):
        return session.query(self.reminder_class).get(reminder_id).task.User

    @session_injector
    def add_reminder(self, reminder, session=None):
        session.add(reminder)
        return reminder

    @session_injector
    def delete_reminder(self, reminder_id, session=None):
        delete_query = session.query(self.reminder_class).filter_by(reminder_id=reminder_id)
        return self.execute_delete_query(delete_query)

    @session_injector
    def update_reminder(self, reminder_id, args, session=None):
        session.query(self.reminder_class).filter_by(reminder_id=reminder_id).update(args)
        return session.query(self.reminder_class).get(reminder_id)
    # endregion

    # region SubtaskRelations quires
    @session_injector
    def add_subtask_relation(self, subtask_relation, session=None):
        session.add(subtask_relation)
        return subtask_relation

    @session_injector
    def get_subtask_relation_by_id(self, relation_id, session=None):
        return session.query(self.subtask_relation_class).get(relation_id)

    @session_injector
    def get_subtask_relation(self, parent_task_id, sub_task_id, session=None):
        return (session.query(self.subtask_relation_class)
                .filter_by(parent_task_id=parent_task_id, sub_task_id=sub_task_id)
                .first())

    @session_injector
    def get_task_subtask_relations(self, task_id, session=None):
        return session.query(self.subtask_relation_class).filter_by(parent_task_id=task_id).all()

    @session_injector
    def delete_subtask_relation_by_id(self, relation_id, session=None):
        delete_query = session.query(self.subtask_relation_class).filter_by(relation_id=relation_id)
        return self.execute_delete_query(delete_query)

    @session_injector
    def delete_subtask_relation(self, parent_task_id, sub_task_id, session=None):
        delete_query = (session.query(self.subtask_relation_class)
                        .filter(and_(self.subtask_relation_class.sub_task_id == sub_task_id,
                                     self.subtask_relation_class.parent_task_id == parent_task_id)))
        return self.execute_delete_query(delete_query)

    @session_injector
    def update_subtask_relation(self, relation_id, args, session=None):
        session.query(self.subtask_relation_class).filter_by(relation_id=relation_id).update(args)
        return session.query(self.subtask_relation_class).get(relation_id)
    # endregion

    # region TaskDates quires
    @session_injector
    def get_date(self, date_id, session=None):
        return session.query(self.task_date_class).get(date_id)

    @session_injector
    def get_task_dates(self, task_id, session=None):
        return session.query(self.task_date_class).filter_by(task_id=task_id).all()

    @session_injector
    def get_task_datess(self, task_id=None, session=None):
        return session.query(self.task_date_class).filter_by(task_id=task_id).all()

    @session_injector
    def get_overdue_dates(self, date=None, session=None):
        date = datetime.now().date() if not date else date
        return session.query(self.task_date_class).filter(self.task_date_class.date < date).all()

    @session_injector
    def get_date_task(self, date_id, session=None):
        return (session.query(self.task_class)
                .join(self.task_date_class)
                .filter(self.task_date_class.date_id == date_id)).first()

    @session_injector
    def add_date(self, date, session=None):
        session.add(date)
        return date

    @session_injector
    def delete_date(self, date_id, session=None):
        delete_query = session.query(self.task_date_class).filter_by(date_id=date_id)
        return self.execute_delete_query(delete_query)

    @session_injector
    def update_date(self, date_id, args, session=None):
        session.query(self.task_date_class).filter_by(date_id=date_id).update(args)
        return session.query(self.task_date_class).get(date_id)
    # endregion

    # region TaskGroupRelations quires
    @session_injector
    def add_task_group_relation(self, task_group_relation, session=None):
        session.add(task_group_relation)
        return task_group_relation

    @session_injector
    def get_task_group_relation(self, relation_id, session=None):
        return session.query(self.task_group_relation_class).get(relation_id)

    @session_injector
    def delete_task_group_relation_by_id(self, relation_id, session=None):
        delete_query = session.query(self.task_group_relation_class).filter_by(relation_id=relation_id)
        return self.execute_delete_query(delete_query)

    @session_injector
    def delete_task_group_relation(self, task_id, group_id, session=None):
        delete_query = (session.query(self.task_group_relation_class)
                        .filter(and_(self.task_group_relation_class.task_id == task_id,
                                     self.task_group_relation_class.group_id == group_id)))
        return self.execute_delete_query(delete_query)

    @session_injector
    def update_task_group_relation(self, relation_id, args, session=None):
        session.query(self.task_group_relation_class).filter_by(relation_id=relation_id).update(args)
        return session.query(self.task_group_relation_class).get(relation_id)
    # endregion

    # region TaskMessages quires
    @session_injector
    def get_task_message(self, task_message_id, session=None):
        return session.query(self.message_class).get(task_message_id)

    @session_injector
    def get_user_task_messages(self, user_id, session=None):
        return (session.query(self.message_class)
                .filter_by(user_id=user_id)
                .order_by(desc(self.message_table.c.date), desc(self.message_table.c.time))).all()

    @session_injector
    def add_task_message(self, task_message, session=None):
        session.add(task_message)
        return task_message

    @session_injector
    def delete_task_message(self, task_message_id, session=None):
        delete_query = session.query(self.message_class).filter_by(task_message_id=task_message_id)
        return self.execute_delete_query(delete_query)
    # endregion

    # region Tasks quires
    @session_injector
    def get_task(self, task_id, session=None):
        return session.query(self.task_class).get(task_id)

    @session_injector
    def get_user_tasks(self, user_id, task_type=None, session=None):
        return (session.query(self.task_class)
                .filter(and_(self.task_class.user_creator == user_id,
                             (self.task_class.task_type == task_type) if task_type is not None else True))).all()

    @session_injector
    def get_filtered_dates(self, filter_dict, from_date=None, to_date=None,
                           date_type=None, user_executor_id=None, start_from=None, end_by=None,
                           title_pattern=None, session=None):
        result_query = session.query(self.task_date_class).filter_by(**filter_dict)

        if title_pattern is not None:
            result_query = result_query.filter(self.task_class.title.like("%{}%".format(title_pattern)))

        if from_date is not None and to_date is not None:
            if date_type is None:
                result_query = (result_query.join(self.task_date_class)
                                .filter(and_(self.task_date_class.date >= from_date,
                                             self.task_date_class.date <= to_date)))
            else:
                result_query = (result_query.join(self.task_date_class)
                                .filter(and_(self.task_date_class.date >= from_date,
                                             self.task_date_class.date <= to_date,
                                             self.task_date_class.date_type == date_type)))

        elif from_date is not None:
            if date_type is None:
                result_query = result_query.join(self.task_date_class).filter(self.task_date_class.date >= from_date)
            else:
                result_query = (result_query.join(self.task_date_class)
                                .filter(and_(self.task_date_class.date >= from_date,
                                             self.task_date_class.date_type == date_type)))
        elif to_date is not None:
            if date_type is None:
                result_query = result_query.join(self.task_date_class).filter(self.task_date_class.date <= to_date)
            else:
                result_query = (result_query.join(self.task_date_class)
                                .filter(and_(self.task_date_class.date <= to_date,
                                             self.task_date_class.date_type == date_type)))

        if user_executor_id is not None:
            result_query = (result_query
                            .join(self.user_executor_relation_class)
                            .filter(self.user_executor_relation_class.user_executor_id == user_executor_id))

        if start_from is None:
            start_from = 0

        if end_by is not None:
            return result_query[start_from:end_by]

        return result_query[start_from:]

    @session_injector
    def get_filtered_tasks(self, filter_dict, from_date=None, to_date=None,
                           date_type=None, user_executor_id=None, start_from=None, end_by=None,
                           title_pattern=None, session=None):

        result_query = session.query(self.task_class).filter_by(**filter_dict)

        if title_pattern is not None:
            result_query = result_query.filter(self.task_class.title.like("%{}%".format(title_pattern)))

        if from_date is not None and to_date is not None:
            if date_type is None:
                result_query = (result_query.join(self.task_date_class)
                                .filter(and_(self.task_date_class.date >= from_date,
                                             self.task_date_class.date <= to_date)))
            else:
                result_query = (result_query.join(self.task_date_class)
                                .filter(and_(self.task_date_class.date >= from_date,
                                             self.task_date_class.date <= to_date,
                                             self.task_date_class.date_type == date_type)))

        elif from_date is not None:
            if date_type is None:
                result_query = result_query.join(self.task_date_class).filter(self.task_date_class.date >= from_date)
            else:
                result_query = (result_query.join(self.task_date_class)
                                .filter(and_(self.task_date_class.date >= from_date,
                                             self.task_date_class.date_type == date_type)))
        elif to_date is not None:
            if date_type is None:
                result_query = result_query.join(self.task_date_class).filter(self.task_date_class.date <= to_date)
            else:
                result_query = (result_query.join(self.task_date_class)
                                .filter(and_(self.task_date_class.date <= to_date,
                                             self.task_date_class.date_type == date_type)))

        if user_executor_id is not None:
            result_query = (result_query
                            .join(self.user_executor_relation_class)
                            .filter(self.user_executor_relation_class.user_executor_id == user_executor_id))

        if start_from is None:
            start_from = 0

        if end_by is not None:
            return result_query[start_from:end_by]

        return result_query[start_from:]

    @session_injector
    def get_filtered_notifications(self, filter_dict, task_id=None, user_id=None,
                                   from_date=None, to_date=None, date_type=None, start_from=None, end_by=None,
                                   session=None,):

        result_query = session.query(self.notification_class).filter_by(**filter_dict)

        if from_date is not None or to_date is not None or user_id is not None or task_id is not None:
            result_query = result_query.join(self.task_date_class)

            if from_date is not None and to_date is not None:
                if date_type is None:
                    result_query = (result_query.filter(and_(self.task_date_class.date >= from_date,
                                                             self.task_date_class.date <= to_date)))
                else:
                    result_query = (result_query.filter(and_(self.task_date_class.date >= from_date,
                                                             self.task_date_class.date <= to_date,
                                                             self.task_date_class.date_type == date_type)))

            elif from_date is not None:
                if date_type is None:
                    result_query = result_query.filter(self.task_date_class.date >= from_date)
                else:
                    result_query = (result_query.filter(and_(self.task_date_class.date >= from_date,
                                                             self.task_date_class.date_type == date_type)))
            elif to_date is not None:
                if date_type is None:
                    result_query = result_query.filter(self.task_date_class.date <= to_date)
                else:
                    result_query = (result_query.filter(and_(self.task_date_class.date <= to_date,
                                                             self.task_date_class.date_type == date_type)))

            if task_id is not None:
                result_query = result_query.filter(self.task_date_class.task_id == task_id)
            if user_id is not None:
                result_query = result_query.join(self.task_class).filter(self.task_class.user_creator == user_id)

        if start_from is None:
            start_from = 0

        if end_by is not None:
            return result_query[start_from:end_by]

        return result_query[start_from:]

    @session_injector
    def get_filtered_reminders(self, filter_dict, user_id=None, from_raise_date=None,
                               to_raise_date=None, start_from=None, end_by=None, session=None):

        result_query = session.query(self.reminder_class).filter_by(**filter_dict)

        if from_raise_date is not None and to_raise_date is not None:
            result_query = (result_query.filter(and_(self.reminder_class.raise_date >= from_raise_date,
                                                     self.reminder_class.raise_date <= to_raise_date)))
        elif from_raise_date is not None:
            result_query = result_query.filter(self.reminder_class.raise_date >= from_raise_date)
        elif to_raise_date is not None:
            result_query = result_query.filter(self.reminder_class.raise_date <= to_raise_date)

        if user_id is not None:
            result_query = result_query.join(self.task_class).filter(self.task_class.user_creator == user_id)

        if start_from is None:
            start_from = 0

        if end_by is not None:
            return result_query[start_from:end_by]

        return result_query[start_from:]

    @session_injector
    def get_filtered_groups(self, filter_dict, task_id=None, start_from=None, end_by=None, title_pattern=None,
                            session=None):
        result_query = session.query(self.group_class).filter_by(**filter_dict)

        if title_pattern is not None:
            result_query = result_query.filter(self.group_class.title.like("%{}%".format(title_pattern)))

        if task_id is not None:
            result_query = (result_query.join(self.task_group_relation_class)
                            .filter(self.task_group_relation_class.task_id == task_id))

        if start_from is None:
            start_from = 0

        if end_by is not None:
            return result_query[start_from:end_by]

        return result_query[start_from:]

    @session_injector
    def get_filtered_plans(self, filter_dict, user_id=None, start_from=None, end_by=None, session=None):

        result_query = session.query(self.plan_class).filter_by(**filter_dict)

        if user_id is not None:
            result_query = result_query.join(self.task_class).filter(self.task_class.user_creator == user_id)

        if start_from is None:
            start_from = 0

        if end_by is not None:
            return result_query[start_from:end_by]

        return result_query[start_from:]

    @session_injector
    def get_filtered_users(self, username_pattern, start_from=None, end_by=None, session=None):
        result_query = session.query(self.user_class).filter(self.user_class.name.like("%{}%".format(username_pattern)))

        if start_from is None:
            start_from = 0

        if end_by is not None:
            return result_query[start_from:end_by]

        return result_query[start_from:]

    @session_injector
    def get_filtered_messages(self, filter_dict, title_pattern=None, start_from=None, end_by=None, session=None):
        result_query = session.query(self.message_class).filter_by(**filter_dict)

        if title_pattern is not None:
            result_query = result_query.filter(self.message_class.message_title.like("%{}%".format(title_pattern)))

        if start_from is None:
            start_from = 0

        if end_by is not None:
            return result_query[start_from:end_by]

        return result_query[start_from:]

    @session_injector
    def get_user_executable_tasks(self, user_id, task_type=None, session=None):
        return (session.query(self.task_class)
                .join(self.user_executor_relation_class)
                .filter(and_(self.user_executor_relation_class.user_executor_id == user_id,
                             (self.task_class.task_type == task_type) if task_type is not None else True))).all()

    @session_injector
    def get_sub_tasks(self, task_id, session=None):
        return (session.query(self.task_class)
                .join(self.subtask_relation_class,
                      self.subtask_relation_class.sub_task_id == self.task_class.task_id)
                .filter(self.subtask_relation_class.parent_task_id == task_id)).all()

    @session_injector
    def get_parent_tasks(self, task_id, session=None):
        return (session.query(self.task_class)
                .join(self.subtask_relation_class,
                      self.subtask_relation_class.parent_task_id == self.task_class.task_id)
                .filter(self.subtask_relation_class.sub_task_id == task_id)).all()

    @session_injector
    def add_task(self, task, session=None):
        session.add(task)
        return task

    @session_injector
    def delete_task(self, task_id, session=None):
        delete_query = session.query(self.task_class).filter_by(task_id=task_id)
        return self.execute_delete_query(delete_query)

    @session_injector
    def update_task(self, task_id, args, session=None):
        session.query(self.task_class).filter_by(task_id=task_id).update(args)
        return session.query(self.task_class).get(task_id)
    # endregion

    # region UserExecutorRelations quires
    @session_injector
    def add_user_executor_relation(self, user_executor_relation, session=None):
        session.add(user_executor_relation)
        return user_executor_relation

    @session_injector
    def get_user_executor_relation(self, relation_id, session=None):
        return session.query(self.user_executor_relation_class).get(relation_id)

    @session_injector
    def delete_user_executor_relation_by_id(self, relation_id, session=None):
        delete_query = session.query(self.user_executor_relation_class).filter_by(relation_id=relation_id)
        return self.execute_delete_query(delete_query)

    @session_injector
    def delete_user_executor_relation(self, user_id, task_id, session=None):
        delete_query = (session.query(self.user_executor_relation_class)
                        .filter(and_(self.user_executor_relation_class.task_id == task_id,
                                     self.user_executor_relation_class.user_executor_id == user_id)))
        return self.execute_delete_query(delete_query)

    @session_injector
    def update_user_executor_relation(self, relation_id, args, session=None):
        session.query(self.user_executor_relation_class).filter_by(relation_id=relation_id).update(args)
        return session.query(self.user_executor_relation_class).get(relation_id)
    # endregion

    # region Users quires
    @session_injector
    def get_user_by_id(self, user_id, session=None):
        return session.query(self.user_class).get(user_id)

    @session_injector
    def get_user(self, user_id, session=None):
        return session.query(self.user_class).get(user_id)

    @session_injector
    def get_user_by_username(self, username, session=None):
        return session.query(self.user_class).filter_by(name=username).first()

    @session_injector
    def get_users_executors(self, task_id, session=None):
        return (session.query(self.user_class)
                .join(self.user_executor_relation_class)
                .filter(self.user_executor_relation_class.task_id == task_id)).all()

    @session_injector
    def get_users(self, session=None):
        return session.query(self.user_class).all()

    @session_injector
    def add_user(self, user, session=None):
        session.add(user)
        return user

    @session_injector
    def delete_user(self, user_id, session=None):
        delete_query = session.query(self.user_class).filter_by(user_id=user_id)
        return self.execute_delete_query(delete_query)

    @session_injector
    def update_user(self, user_id, args, session=None):
        session.query(self.user_class).filter_by(user_id=user_id).update(args)
        return session.query(self.user_class).get(user_id)
    # endregion

    @session_injector
    def is_duplication(self, model, args, session=None,):
        return session.query(model).filter_by(**args).first() is not None

    def execute_delete_query(self, delete_query):
        delete_result = delete_query.all()
        if delete_result:
            delete_query.delete()
            return True
        else:
            return False

    @staticmethod
    def set_up_database(connection_string):
        """
            Sets up database specified by connection string with tables:
                Users
                Groups
                Messages
                Tasks
                ConditionTaskRelations
                Plans
                Reminders
                SubtaskRelations
                TaskDates
                TaskGroupRelation
                UserExecutorRelations
                Notifications

        """
        engine = create_engine(connection_string)
        metadata = MetaData(engine)

        if not engine.dialect.has_table(engine, 'Users'):
            Table(
                'Users', metadata,
                Column('user_id', BigInteger, primary_key=True),
                Column('name', String(100, 'utf8_unicode_ci'), nullable=False)
            )

        if not engine.dialect.has_table(engine, 'Groups'):
            Table(
                'Groups', metadata,
                Column('group_id', BigInteger, primary_key=True),
                Column('user_creator_id', ForeignKey('Users.user_id', ondelete='CASCADE'), nullable=False, index=True),
                Column('title', String(50, 'utf8_unicode_ci'), nullable=False),
                Column('group_type', BigInteger, nullable=False, server_default=text("'1'"))
            )

        if not engine.dialect.has_table(engine, 'Messages'):
            Table(
                'Messages', metadata,
                Column('task_message_id', BigInteger, primary_key=True),
                Column('user_id', ForeignKey('Users.user_id', ondelete='CASCADE'), nullable=False, index=True),
                Column('sender_id', ForeignKey('Users.user_id', ondelete='CASCADE'), index=True),
                Column('date', Date, nullable=False),
                Column('seen', BigInteger, nullable=False, server_default=text("'0'")),
                Column('message_text', String(5000, 'utf8_unicode_ci'), nullable=False),
                Column('message_title', String(500, 'utf8_unicode_ci'), nullable=False),
                Column('time', Time, nullable=False)
            )

        if not engine.dialect.has_table(engine, 'Tasks'):
            Table(
                'Tasks', metadata,
                Column('task_id', BigInteger, primary_key=True),
                Column('task_type', BigInteger, nullable=False),
                Column('user_creator', ForeignKey('Users.user_id', ondelete='CASCADE'), nullable=False, index=True),
                Column('title', String(300, 'utf8_unicode_ci'), nullable=False),
                Column('description', String(5500, 'utf8_unicode_ci')),
                Column('creation_date', Date, nullable=False),
                Column('priority', BigInteger, nullable=False),
                Column('status', BigInteger, nullable=False, server_default=text("'1'")),
                Column('last_subtask_dependency', BigInteger, nullable=False, server_default=text("'0'")),
                Column('priority_on_overdue', BigInteger, nullable=False, server_default=text("'1'"))
            )

        if not engine.dialect.has_table(engine, 'ConditionTaskRelations'):
            Table(
                'ConditionTaskRelations', metadata,
                Column('relation_id', BigInteger, primary_key=True),
                Column('defined_task_id', ForeignKey('Tasks.task_id', ondelete='CASCADE'), nullable=False, index=True),
                Column('condition_task_id', ForeignKey('Tasks.task_id', ondelete='CASCADE'), nullable=False,
                       index=True),
                Column('condition_task_status', BigInteger, nullable=False)
            )

        if not engine.dialect.has_table(engine, 'Plans'):
            Table(
                'Plans', metadata,
                Column('plan_id', BigInteger, primary_key=True),
                Column('task_id', ForeignKey('Tasks.task_id', ondelete='CASCADE'), nullable=False, index=True),
                Column('create_task_date', Date, nullable=False),
                Column('repeat_type', BigInteger, nullable=False),
                Column('plan_interval', BigInteger, nullable=False),
                Column('end_type', BigInteger, nullable=False),
                Column('end_date', Date),
                Column('repetitions_number', BigInteger),
                Column('repetitions_count', BigInteger, nullable=False, server_default=text("'0'")),
                Column('active', BigInteger, nullable=False, server_default=text("'1'"))
            )

        if not engine.dialect.has_table(engine, 'Reminders'):
            Table(
                'Reminders', metadata,
                Column('reminder_id', BigInteger, primary_key=True),
                Column('task_id', ForeignKey('Tasks.task_id', ondelete='CASCADE'), nullable=False, index=True),
                Column('raise_date', Date, nullable=False),
                Column('raise_time', Time, nullable=False),
                Column('days_interval', BigInteger, nullable=False),
                Column('active', BigInteger)
            )

        if not engine.dialect.has_table(engine, 'SubtaskRelations'):
            Table(
                'SubtaskRelations', metadata,
                Column('relation_id', BigInteger, primary_key=True),
                Column('parent_task_id', ForeignKey('Tasks.task_id', ondelete='CASCADE'), nullable=False, index=True),
                Column('sub_task_id', ForeignKey('Tasks.task_id', ondelete='CASCADE'), nullable=False, index=True),
                Column('relation_type', BigInteger, nullable=False)
            )

        if not engine.dialect.has_table(engine, 'TaskDates'):
            Table(
                'TaskDates', metadata,
                Column('date_id', BigInteger, primary_key=True),
                Column('task_id', ForeignKey('Tasks.task_id', ondelete='CASCADE'), nullable=False, index=True),
                Column('date_type', BigInteger, nullable=False),
                Column('date', Date, nullable=False)
            )

        if not engine.dialect.has_table(engine, 'TaskGroupRelations'):
            Table(
                'TaskGroupRelations', metadata,
                Column('relation_id', BigInteger, primary_key=True),
                Column('task_id', ForeignKey('Tasks.task_id', ondelete='CASCADE'), nullable=False, index=True),
                Column('group_id', ForeignKey('Groups.group_id', ondelete='CASCADE'), nullable=False, index=True)
            )

        if not engine.dialect.has_table(engine, 'UserExecutorRelations'):
            Table(
                'UserExecutorRelations', metadata,
                Column('relation_id', BigInteger, primary_key=True),
                Column('user_executor_id', ForeignKey('Users.user_id', ondelete='CASCADE'), nullable=False, index=True),
                Column('task_id', ForeignKey('Tasks.task_id', ondelete='CASCADE'), nullable=False, index=True),
                Column('relation_confirmed', BigInteger, nullable=False, server_default=text("'0'"))
            )

        if not engine.dialect.has_table(engine, 'Notifications'):
            Table(
                'Notifications', metadata,
                Column('notification_id', BigInteger, primary_key=True),
                Column('date_id', ForeignKey('TaskDates.date_id', ondelete='CASCADE'), nullable=False, index=True),
                Column('days_to_date', BigInteger, nullable=False),
                Column('time', Time, nullable=False, server_default=text("'00:00:00'")),
                Column('used', BigInteger, nullable=False, server_default=text("'0'"))
            )

        metadata.create_all()
