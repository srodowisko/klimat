"""
    This module contains bare data models as they are
    stored in database and lasses derived from Enum class
    for easier attributes representations of models

    Classes:
        ConditionTaskRelation
        Group
        Notification
        Plan
        Reminder
        SubtaskRelation
        TaskDate
        TaskGroupRelation
        Message
        Task
        UserExecutorRelation
        User

    Global variables:
        # Models' attributes names constants
        CONDITION_TASK_RELATION_ATTR_ID = "relation_id"
        CONDITION_TASK_RELATION_ATTR_STATUS = "condition_task_status"
        CONDITION_TASK_RELATION_ATTR_DEFINED_ID = "defined_task_id"
        CONDITION_TASK_RELATION_ATTR_TRIGGER_ID = "condition_task_id"

        GROUP_ATTR_ID = "group_id"
        GROUP_ATTR_CREATOR_ID = "user_creator_id"
        GROUP_ATTR_TITLE = "title"
        GROUP_ATTR_TYPE = "group_type"

        NOTIFICATION_ATTR_ID = "notification_id"
        NOTIFICATION_ATTR_DAYS = "days_to_date"
        NOTIFICATION_ATTR_DATE_ID = "date_id"
        NOTIFICATION_ATTR_TIME = "time"
        NOTIFICATION_ATTR_USED = "used"

        PLAN_ATTR_ID = "plan_id"
        PLAN_ATTR_TASK_ID = "task_id"
        PLAN_ATTR_DATE = "create_task_date"
        PLAN_ATTR_TYPE = "repeat_type"
        PLAN_ATTR_INTERVAL = "plan_interval"
        PLAN_ATTR_END_TYPE = "end_type"
        PLAN_ATTR_END_DATE = "end_date"
        PLAN_ATTR_ACTIVE = "active"
        PLAN_ATTR_REPETITIONS_NUMBER = "repetitions_number"
        PLAN_ATTR_REPETITIONS_COUNT = "repetitions_count"

        REMINDER_ATTR_ID = "reminder_id"
        REMINDER_ATTR_TASK = "task_id"
        REMINDER_ATTR_DATE = "raise_date"
        REMINDER_ATTR_TIME = "raise_time"
        REMINDER_ATTR_INTERVAL = "days_interval"
        REMINDER_ATTR_ACTIVE = "active"

        SUBTASK_RELATION_ATTR_ID = "relation_id"
        SUBTASK_RELATION_ATTR_PARENT_ID = "parent_task_id"
        SUBTASK_RELATION_ATTR_CHILD_ID = "sub_task_id"
        SUBTASK_RELATION_ATTR_TYPE = "relation_type"

        DATE_ATTR_ID = "date_id"
        DATE_ATTR_TASK_ID = "task_id"
        DATE_ATTR_TYPE = "task_type"
        DATE_ATTR_DATE = "date"

        GROUP_RELATION_ATTR_ID = "relation_id"
        GROUP_RELATION_ATTR_TASK_ID = "task_id"
        GROUP_RELATION_ATTR_GROUP_ID = "group_id"

        MESSAGE_ATTR_ID = "task_message_id"
        MESSAGE_ATTR_USER_ID = "user_id"
        MESSAGE_ATTR_TEXT = "message_text"
        MESSAGE_ATTR_TITLE = "message_title"
        MESSAGE_ATTR_SENDER_ID = "sender_id"
        MESSAGE_ATTR_SEEN = "seen"
        MESSAGE_ATTR_DATE = "date"
        MESSAGE_ATTR_TIME = "time"

        TASK_ATTR_ID = "task_id"
        TASK_ATTR_TYPE = "task_type"
        TASK_ATTR_USER = "user_creator"
        TASK_ATTR_TITLE = "title"
        TASK_ATTR_DESCRIPTION = "description"
        TASK_ATTR_CREATION_DATE = "creation_date"
        TASK_ATTR_PRIORITY = "priority"
        TASK_ATTR_STATUS = "status"
        TASK_ATTR_DEPENDENT = "last_subtask_dependency"
        TASK_ATTR_OVERDUE_PRIORITY = "priority_on_overdue"

        EXECUTOR_RELATION_ATTR_ID = "relation_id"
        EXECUTOR_RELATION_ATTR_EXECUTOR_ID = "user_executor_id"
        EXECUTOR_RELATION_ATTR_TASK_ID = "task_id"
        EXECUTOR_RELATION_ATTR_CONFIRMED = "relation_confirmed"

        USER_ATTR_ID = "user_id"
        USER_ATTR_USERNAME = "name"

        # For each dictionary key - model attribute of enum type, value - how this attribute should be represented;
        CONDITION_TASK_RELATION_ENUM_TRANSFORMER
        NOTIFICATION_ENUM_TRANSFORMER
        REMINDER_ENUM_TRANSFORMER
        PLAN_ENUM_TRANSFORMER
        SUBTASK_RELATION_ENUM_TRANSFORMER
        TASK_DATE_ENUM_TRANSFORMER
        TASK_ENUM_TRANSFORMER

    Functions:
        get_representative_model(model, model_attributes_transformer) - returns model for representation


"""
from datetime import datetime
from planner4ucore.models import enums as me


class ConditionTaskRelation:
    """ Condition task relation model

        Attributes:
            defined_task_id (int)
            condition_task_id (int)
            condition_task_status (int)
            relation_id (int)

    """
    def __init__(self, defined_task_id, condition_task_id, condition_task_status, relation_id=None):
        """
            Args:
                defined_task_id (int)
                condition_task_id (int)
                condition_task_status (int)
                relation_id (int) = None

        """
        self.relation_id = relation_id
        self.defined_task_id = defined_task_id
        self.condition_task_id = condition_task_id
        self.condition_task_status = condition_task_status


class Group:
    """ Group model

        Attributes:
            user_creator_id (int)
            title (str)
            group_id (int)
            group_type (int)

    """
    def __init__(self, user_creator_id, title, group_id=None, group_type=me.GroupType.CUSTOM.value):
        """

            Args:
                user_creator_id (int)
                title (str)
                group_id (int) = None
                group_type (int) = models.GroupType.CUSTOM

        """
        self.group_id = group_id
        self.user_creator_id = user_creator_id
        self.title = title
        self.group_type = group_type


class Notification:
    """ Notification model

    Attributes:
        date_id (int)
        days_to_date (int)
        time (datetime.time)
        notification_id (int)
        used (int)

    """
    def __init__(self, date_id, days_to_date, time, notification_id=None, used=me.Used.NO.value):
        """
             Args:
                date_id (int)
                days_to_date (int)
                time (datetime.time)
                notification_id (int) = None
                used (int) = models.Used.NO

        """
        self.notification_id = notification_id
        self.date_id = date_id
        self.days_to_date = days_to_date
        self.time = time
        self.used = used


class Plan:
    """ Plan model

        Attributes:
            create_task_date (datetime.date)
            repeat_type (int: models.RepetitionType)
            plan_interval (int)
            end_type (int: models.PlanCompletionCriteria)
            end_date (datetime.date)
            repetitions_number (int)
            task_id (int)
            plan_id (int)
            active (int)
            repetitions_count (int)

    """
    def __init__(self, create_task_date, repeat_type, plan_interval, end_type, end_date=None, repetitions_number=None,
                 task_id=None, plan_id=None, active=me.Active.YES.value, repetitions_count=0):
        """
            Args:
                create_task_date (datetime.date)
                repeat_type (int: models.RepetitionType)
                plan_interval (int)
                end_type (int: models.PlanCompletionCriteria)
                end_date (datetime.date) = None
                repetitions_number (int) = None
                task_id (int) = None
                plan_id (int) = None
                active (int) = models.Active.YES
                repetitions_count (int) = 0

        """
        self.plan_id = plan_id
        self.task_id = task_id
        self.create_task_date = create_task_date
        self.repeat_type = repeat_type
        self.plan_interval = plan_interval
        self.end_type = end_type
        self.end_date = end_date
        self.active = active
        self.repetitions_number = repetitions_number
        self.repetitions_count = repetitions_count


class Reminder:
    """ Reminder model

        Attributes:
            task_id (int)
            raise_date (datetime.date)
            raise_time (datetime.time)
            days_interval (int)
            reminder_id (int)

    """
    def __init__(self, task_id, raise_date, raise_time, days_interval,
                 active=me.Active.YES.value, reminder_id=None):
        """
            Args:
                task_id (int)
                raise_date (datetime.date)
                raise_time (datetime.time)
                days_interval (int)
                reminder_id (int) = None

        """
        self.reminder_id = reminder_id
        self.task_id = task_id
        self.raise_date = raise_date
        self.raise_time = raise_time
        self.days_interval = days_interval
        self.active = active


class SubtaskRelation:
    """ Subtask relation model

        Attributes:
            parent_task_id (int)
            sub_task_id (int)
            relation_type (int: models.RelationType)
            relation_id (int)

    """
    def __init__(self, parent_task_id, sub_task_id, relation_type=me.TaskRelation.CONNECTED.value,
                 relation_id=None):
        """
            Args:
                parent_task_id (int)
                sub_task_id (int)
                relation_type (int: models.RelationType)
                relation_id (int) = None
        """
        self.relation_id = relation_id
        self.parent_task_id = parent_task_id
        self.sub_task_id = sub_task_id
        self.relation_type = relation_type


class TaskDate:
    """ Task date model

        Attributes:
            task_id (int)
            date_type (int: models.DateType)
            date (datetime.date)
            date_id (int)

    """
    def __init__(self, task_id, date_type, date, date_id=None):
        """
            Args:
                task_id (int)
                date_type (int: models.DateType)
                date (datetime.date)
                date_id (int) = None

        """
        self.date_id = date_id
        self.task_id = task_id
        self.date_type = date_type
        self.date = date


class TaskGroupRelation:
    """ Task-group relation model

        Attributes:
            task_id (int)
            group_id (int)
            relation_id (int)

    """
    def __init__(self, task_id, group_id,  relation_id=None):
        """
            Args:
                task_id (int)
                group_id (int)
                relation_id (int) = None

        """
        self.relation_id = relation_id
        self.task_id = task_id
        self.group_id = group_id


class Message:
    """ Message model

        Attributes:
            user_id (int): to how this message belong
            message_text (str)
            message_title (str)
            task_message_id (int)
            seen (int)
            sender_id (int)
            date (datetime.date)
            time (datetime.time)

    """
    def __init__(self, user_id, message_title, message_text="", task_message_id=None,
                 seen=0, sender_id=None, date=None, time=None):
        """
            Args:
                user_id (int)
                message_text (str)
                message_title (str)
                task_message_id (int) = None
                seen (int) = 0
                sender_id (int) = None
                date (datetime.date) = None
                time (datetime.time) = None

        """
        self.task_message_id = task_message_id
        self.user_id = user_id
        self.message_text = message_text
        self.message_title = message_title
        self.sender_id = sender_id
        self.seen = seen
        self.date = date if date is not None else datetime.now().date()
        self.time = time if date is not None else datetime.now().time()


class Task:
    """ Task model

        Attributes:
            task_type (int)
            user_creator (int)
            creation_date (datetime.date)
            title (str)
            description (str)
            priority (int: models.Priority)
            status (int: models.Status)
            last_subtask_dependency (int): shows whether to close task if last subtask was executed
            priority_on_overdue (int: models.Priority)
            task_id (int)

    """
    def __init__(self, user_creator, title, status=me.TaskStatus.CREATED.value,
                 task_type=me.TaskType.SIMPLE.value,
                 priority=None, description="",
                 last_subtask_dependency=me.SubtaskDependency.NOT_DEPENDENT.value,
                 creation_date=None, priority_on_overdue=None, task_id=None):
        """
            Args:
                task_type (int)
                user_creator (int)
                creation_date (datetime.date)
                title (str)
                description (str)
                priority (int: models.Priority)
                status (int: models.Status)
                last_subtask_dependency (int): shows whether to close task if last subtask was executed
                priority_on_overdue (int: models.Priority)
                task_id (int) = None

        """
        self.task_id = task_id
        self.task_type = task_type
        self.user_creator = user_creator
        self.title = title
        self.description = description
        self.creation_date = creation_date if creation_date else datetime.now().date()
        self.priority = priority
        self.status = status
        self.last_subtask_dependency = last_subtask_dependency
        self.priority_on_overdue = priority_on_overdue if priority_on_overdue else priority


class UserExecutorRelation:
    """ User executor relation

        Attributes:
             user_executor_id (int)
            task_id (int)
            relation_id (int)
            relation_confirmed (int: models.Active)

    """
    def __init__(self, user_executor_id, task_id, relation_id=None, relation_confirmed=me.Active.NO.value):
        """
            Args:
                user_executor_id (int)
                task_id (int)
                relation_id (int) = None
                relation_confirmed (int: models.Active) = 0

        """
        self.relation_id = relation_id,
        self.user_executor_id = user_executor_id
        self.task_id = task_id
        self.relation_confirmed = relation_confirmed


class User:
    """ User model

        Attributes:
            user_id (int)
            name (str)

    """
    def __init__(self, name, user_id=None):
        """
            Args:
                user_id (int)
                name (str)
        """
        self.user_id = user_id
        self.name = name


CONDITION_TASK_RELATION_ATTR_ID = "relation_id"
CONDITION_TASK_RELATION_ATTR_STATUS = "condition_task_status"
CONDITION_TASK_RELATION_ATTR_DEFINED_ID = "defined_task_id"
CONDITION_TASK_RELATION_ATTR_TRIGGER_ID = "condition_task_id"

GROUP_ATTR_ID = "group_id"
GROUP_ATTR_CREATOR_ID = "user_creator_id"
GROUP_ATTR_TITLE = "title"
GROUP_ATTR_TYPE = "group_type"

NOTIFICATION_ATTR_ID = "notification_id"
NOTIFICATION_ATTR_DAYS = "days_to_date"
NOTIFICATION_ATTR_DATE_ID = "date_id"
NOTIFICATION_ATTR_TIME = "time"
NOTIFICATION_ATTR_USED = "used"

PLAN_ATTR_ID = "plan_id"
PLAN_ATTR_TASK_ID = "task_id"
PLAN_ATTR_DATE = "create_task_date"
PLAN_ATTR_TYPE = "repeat_type"
PLAN_ATTR_INTERVAL = "plan_interval"
PLAN_ATTR_END_TYPE = "end_type"
PLAN_ATTR_END_DATE = "end_date"
PLAN_ATTR_ACTIVE = "active"
PLAN_ATTR_REPETITIONS_NUMBER = "repetitions_number"
PLAN_ATTR_REPETITIONS_COUNT = "repetitions_count"

REMINDER_ATTR_ID = "reminder_id"
REMINDER_ATTR_TASK = "task_id"
REMINDER_ATTR_DATE = "raise_date"
REMINDER_ATTR_TIME = "raise_time"
REMINDER_ATTR_INTERVAL = "days_interval"
REMINDER_ATTR_ACTIVE = "active"

SUBTASK_RELATION_ATTR_ID = "relation_id"
SUBTASK_RELATION_ATTR_PARENT_ID = "parent_task_id"
SUBTASK_RELATION_ATTR_CHILD_ID = "sub_task_id"
SUBTASK_RELATION_ATTR_TYPE = "relation_type"

DATE_ATTR_ID = "date_id"
DATE_ATTR_TASK_ID = "task_id"
DATE_ATTR_TYPE = "date_type"
DATE_ATTR_DATE = "date"

GROUP_RELATION_ATTR_ID = "relation_id"
GROUP_RELATION_ATTR_TASK_ID = "task_id"
GROUP_RELATION_ATTR_GROUP_ID = "group_id"

MESSAGE_ATTR_ID = "task_message_id"
MESSAGE_ATTR_USER_ID = "user_id"
MESSAGE_ATTR_TEXT = "message_text"
MESSAGE_ATTR_TITLE = "message_title"
MESSAGE_ATTR_SENDER_ID = "sender_id"
MESSAGE_ATTR_SEEN = "seen"
MESSAGE_ATTR_DATE = "date"
MESSAGE_ATTR_TIME = "time"

TASK_ATTR_ID = "task_id"
TASK_ATTR_TYPE = "task_type"
TASK_ATTR_USER = "user_creator"
TASK_ATTR_TITLE = "title"
TASK_ATTR_DESCRIPTION = "description"
TASK_ATTR_CREATION_DATE = "creation_date"
TASK_ATTR_PRIORITY = "priority"
TASK_ATTR_STATUS = "status"
TASK_ATTR_DEPENDENT = "last_subtask_dependency"
TASK_ATTR_OVERDUE_PRIORITY = "priority_on_overdue"
TASK_ATTR_OVERDUE_TWO = "priority_on_overdue"

EXECUTOR_RELATION_ATTR_ID = "relation_id"
EXECUTOR_RELATION_ATTR_EXECUTOR_ID = "user_executor_id"
EXECUTOR_RELATION_ATTR_TASK_ID = "task_id"
EXECUTOR_RELATION_ATTR_CONFIRMED = "relation_confirmed"

USER_ATTR_ID = "user_id"
USER_ATTR_USERNAME = "name"


def get_representative_model(model, model_attributes_transformer):
    """
        Replaces (if needed) attributes of given model
        into representative form applying function
        from model_attributes_transformer dictionary

        Args:
            model: model to be handled
            model_attributes_transformer (dict): attributes of object to be represented

        Returns:
            Representative model

    """
    for key, value in vars(model).items():
        if key in model_attributes_transformer:
            setattr(model, key, model_attributes_transformer[key](value))
        elif value is None:
            setattr(model, key, "-")
    return model


def get_representative_models(models, model_attributes_transformer):
    """
        Replaces (if needed) attributes of given models
        into representative form applying function
        from model_attributes_transformer dictionary

        Args:
            models: models to be handled
            model_attributes_transformer (dict): attributes of object to be represented

        Returns:
            Representative model

    """
    return [get_representative_model(model, model_attributes_transformer) for model in models]


CONDITION_TASK_RELATION_ENUM_TRANSFORMER = {
    CONDITION_TASK_RELATION_ATTR_STATUS: lambda x: me.TASK_STATUS_ENUM_TO_STRING[x]
}

NOTIFICATION_ENUM_TRANSFORMER = {
    NOTIFICATION_ATTR_USED: lambda x: me.USED_ENUM_TO_STRING[x]
}

REMINDER_ENUM_TRANSFORMER = {
    REMINDER_ATTR_ACTIVE: lambda x: me.ACTIVE_ENUM_TO_STRING[x]
}

PLAN_ENUM_TRANSFORMER = {
    PLAN_ATTR_TYPE: lambda x: me.REPETITION_TYPE_ENUM_TO_STRING[x],
    PLAN_ATTR_END_TYPE: lambda x: me.PLAN_COMPLETION_CRITERIA_ENUM_TO_STRING[x],
    PLAN_ATTR_ACTIVE: lambda x: me.ACTIVE_ENUM_TO_STRING[x]
}

SUBTASK_RELATION_ENUM_TRANSFORMER = {
    SUBTASK_RELATION_ATTR_TYPE: lambda x: me.TASK_RELATION_ENUM_TO_STRING[x]
}

TASK_DATE_ENUM_TRANSFORMER = {
    DATE_ATTR_TYPE: lambda x: me.DATE_TYPE_ENUM_TO_STRING[x],
}

TASK_ENUM_TRANSFORMER = {
    TASK_ATTR_TYPE: lambda x: me.TASK_TYPE_ENUM_TO_STRING[x],
    TASK_ATTR_PRIORITY: lambda x: me.TASK_PRIORITY_ENUM_TO_STRING[x],
    TASK_ATTR_STATUS: lambda x: me.TASK_STATUS_ENUM_TO_STRING[x],
    TASK_ATTR_DEPENDENT: lambda x: me.SUBTASK_DEPENDENCY_ENUM_TO_STRING[x],
    #TASK_ATTR_OVERDUE_PRIORITY: lambda x: me.TASK_PRIORITY_ENUM_TO_STRING[x],
    TASK_ATTR_OVERDUE_TWO: lambda x: me.TASK_TWO_ENUM_TO_STRING[x]
}


