"""
    This module contains enriched data models for representation.
    Classes contain sets and objects connected with them

    Classes:
        TaskView - represents Task model
        GroupView - represents Group model
        PlanView - represents Plan model
        DateView - represents TaskDate model
        NotificationView - represents Notification model
        ReminderView - represents Reminder model
        TasksView - represents set of Task models
        GroupsView - represents set of Group models
        RemindersView - represents set of Reminder models
        NotificationsView - represents set of Notification models
        MessagesView - represents set of Message models
        SubtasksView - represents tree of SubtaskRelation models

"""


class TaskView:
    """
        Contains necessary information for
        task representations

        Attributes:
            task (models.Task)
            user_creator(models.User): user-creator of the task
            executors(list(models.User)): users-executors of the task
            dates(list(models.TaskDate)): dates associated with this task
            groups(list(models.Group)): groups where this task resides
            notifications(list(models.Notification)): notifications associated with task's dates
            reminders(list(models.Reminders)): reminders of the task

    """
    def __init__(self, task, user_creator, executors, dates, groups, notifications,
                 reminders, plan, condition):
        """
            Args:
                task (models.Task)
                user_creator(models.User): user-creator of the task
                executors(list(models.User)): users-executors of the task
                dates(list(models.TaskDate)): dates associated with this task
                groups(list(models.Group)): groups where this task resides
                notifications(list(models.Notification)): notifications associated with task's dates
                reminders(list(models.Reminders)): reminders of the task

        """
        self.task = task
        self.user_creator = user_creator
        self.executors = executors
        self.dates = dates
        self.groups = groups
        self.notifications = notifications
        self.reminders = reminders
        self.plan = plan
        self.condition = condition


class GroupView:
    """ Constrains necessary information for
        group representations

        Attributes:
            group (models.Group): object of group to be represented
            tasks (list(models.Task)): set of tasks associated with this group

    """
    def __init__(self, group, tasks):
        """
            Args:
            group (models.Group): object of group to be represented
            tasks (list(models.Task)): set of tasks associated with this group

        """
        self.group = group
        self.tasks = tasks


class PlanView:
    """
        Represents plan and its template task

        Attributes:
              plan (models.Plan): object of plan to be represented
              task (models.Task): associated with plan template task

    """
    def __init__(self, plan, task):
        """
            Args:
                  plan (models.Plan): object of plan to be represented
                  task (models.Task): associated with plan template task

        """
        self.plan = plan
        self.task = task


class DateView:
    """
        Contains information for date and its task
        representations

        Attributes:
            date (models.TaskDate): object of date to be represented
            task (models.Task): task associated with date

    """
    def __init__(self, date, task):
        """
            Args:
                date (models.TaskDate): object of date to be represented
                task (models.Task): task associated with date

        """
        self.date = date
        self.task = task


class NotificationView:
    """ Contains information for notification representations

        Attributes:
            notification (models.Notification): notification to be represented
            date (models.TaskDate): date associated with this notification
            task (models.Task): task associated with the date

    """
    def __init__(self, notification, date, task):
        """
            Args:
                notification (models.Notification): notification to be represented
                date (models.TaskDate): date associated with this notification
                task (models.Task): task associated with the date

        """
        self.notification = notification
        self.date = date
        self.task = task


class ReminderView:
    """ Contains information for reminder representations

        Attributes:
            reminder (models.Reminder): reminder to be represented
            task (models.Task): task associated with the reminder

    """

    def __init__(self, reminder, task):
        """
            Args:
                reminder (models.Reminder): reminder to be represented
                task (models.Task): task associated with the reminder

        """
        self.reminder = reminder
        self.task = task


class TasksView:
    """ Represents two sets of Tasks

        Attributes:
             creator_tasks (list(models.Task)): tasks where user is creator
             executor_tasks (list(models.Task)): tasks where user is executor

    """
    def __init__(self, creator_tasks, executor_tasks):
        """
            Args:
                creator_tasks (list(models.Task)): tasks where user is creator
                executor_tasks (list(models.Task)): tasks where user is executor

        """
        self.creator_tasks = creator_tasks
        self.executor_tasks = executor_tasks


class GroupsView:
    """ Represents set of groups

        Attributes:
            groups (list(models.Group)): set of groups to be represented

    """
    def __init__(self, groups):
        """
            Args:
                groups (list(models.Group)): set of groups to be represented
        """
        self.groups = groups


class RemindersView:
    """ Represents set of reminders

        Attributes:
            creator_reminders (list(models.Reminder)): reminders for tasks where user is creator
            executor_reminders (list(models.Reminder)): reminser for tasks where user is executor

    """
    def __init__(self, creator_reminders, executor_reminders):
        """
            Args:
                creator_reminders (list(models.Reminder)): reminders for tasks where user is creator
                executor_reminders (list(models.Reminder)): reminser for tasks where user is executor

        """
        self.creator_reminders = creator_reminders
        self.executor_reminders = executor_reminders


class NotificationsView:
    """ Represents two sets of notifications

        Attributes:
            creator_notifications (list(models.Notification)): notifications for tasks where user is creator
            executor_notifications (list(models.Notification)): notifications for tasks where user is executor

    """
    def __init__(self, creator_notifications, executor_notifications):
        """
            Args:
                creator_notifications (list(models.Notification)): notifications for tasks where user is creator
                executor_notifications (list(models.Notification)): notifications for tasks where user is executor

        """
        self.creator_notifications = creator_notifications
        self.executor_notifications = executor_notifications


class MessagesView:
    """ Represents set of messages

        Attributes:
              messages (list(models.Messages)): set of messages to be represented

    """
    def __init__(self, messages):
        """
            Args:
                messages (list(models.Messages)): set of messages to be represented

        """
        self.messages = messages


class SubtasksView:
    """ Provides information for subtasks tree representations

        Attributes:
              task (models.Task): task object - root for tasks from sub_task_views
              relation_type (str): relation type with parent task
              relation_id (int): id of relation connecting this task and paren task
              sub_task_views (list(SubtasksView)): collection to represent children of this task

    """
    def __init__(self, task, relation_type, relation_id):
        """
            Args:
                task (models.Task): task object - root for tasks from sub_task_views
                relation_type (str): relation type with parent task
                relation_id (str): id of relation connecting this task and paren task

        """
        self.task = task
        self.relation_type = relation_type
        self.relation_id = relation_id
        self.sub_task_views = []
