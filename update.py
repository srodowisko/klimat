from planner4u import get_service
import threading


def update_function():
    get_service().update_system_state()
    threading.Timer(1, update_function).start()


update_function()
