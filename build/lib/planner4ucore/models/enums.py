"""
    Contains enums for models, and dictionaries for
    representation of this enums

    Classes:
        TaskStatus
        TaskType
        TaskPriority
        TaskRelation
        GroupType
        DateType
        RepetitionType
        PlanCompletionCriteria
        PlanState
        RelationConfirmation
        SubtaskDependency
        Used
        Active

    Global variables:
        # string representations of enums
        STATUS_DEFINED = "defined"
        STATUS_CREATED = "created"
        STATUS_WORK = "work"
        STATUS_EXECUTED = "executed"
        STATUS_OVERDUE = "overdue"
        STATUS_CLOSED = "closed"
        STATUS_PAUSED = "paused"

        TASK_TYPE_SIMPLE = "simple"
        TASK_TYPE_CONDITION = "condition"
        TASK_TYPE_PLAN = "plan"

        PRIORITY_MINOR = "minor"
        PRIORITY_LOW = "low"
        PRIORITY_REGULAR = "regular"
        PRIORITY_HIGH = "high"
        PRIORITY_CRITICAL = "critical"

        SUBTASK_BLOCKING = "blocking"
        SUBTASK_DEPENDS = "depends"
        SUBTASK_CONNECTED = "connected"

        DATE_TYPE_START = "Data początkowa"
        DATE_TYPE_END = "end"

        NO = "no"
        YES = "yes"

        PLAN_REPETITION_DAILY = "daily"
        PLAN_REPETITION_WEEKLY = "weekly"
        PLAN_REPETITION_MONTHLY = "monthly"
        PLAN_REPETITION_ANNUALLY = "annually"

        PLAN_ACTIVE_STATE = "active"
        PLAN_INACTIVE_STATE = "inactive"

        PLAN_END_NEVER = "never"
        PLAN_END_DATE = "date"
        PLAN_END_NUMBER = "number"

        # Following dictionaries transform string to enum value
        STRING_TO_TASK_STATUS_ENUM
        STRING_TO_TASK_TYPE_ENUM
        STRING_TO_TASK_PRIORITY_ENUM
        STRING_TO_TASK_RELATION_ENUM
        STRING_TO_DATE_TYPE_ENUM
        STRING_TO_REPETITION_TYPE_ENUM
        STRING_TO_PLAN_COMPLETION_CRITERIA_ENUM
        STRING_TO_PLAN_STATE_ENUM
        STRING_TO_SUBTASK_DEPENDENCY_ENUM
        STRING_TO_USED_ENUM
        STRING_TO_ACTIVE_ENUM

        # Following dictionaries transform enum value to string
        TASK_STATUS_ENUM_TO_STRING
        TASK_TYPE_ENUM_TO_STRING
        TASK_PRIORITY_ENUM_TO_STRING
        TASK_RELATION_ENUM_TO_STRING
        DATE_TYPE_ENUM_TO_STRING
        REPETITION_TYPE_ENUM_TO_STRING
        PLAN_COMPLETION_CRITERIA_ENUM_TO_STRING
        PLAN_STATE_ENUM_TO_STRING
        SUBTASK_DEPENDENCY_ENUM_TO_STRING
        USED_ENUM_TO_STRING
        ACTIVE_ENUM_TO_STRING

"""
from enum import Enum


class TaskStatus(Enum):
    DEFINED = 0  # "BDG | Biuro Dyrektora Generalnego"
    CREATED = 1  # "BF | Biuro Finansowe"
    WORK = 2  # "BKA | Biuro Kontroli i Audytu"
    EXECUTED = 3  # "BM | Biuro Ministra"
    OVERDUE = 4  # "DB | Departament Budżetu"
    CLOSED = 5  # "DC | Departament Ciepłownictwa"
    PAUSED = 6  # "DEG | Departament Elektromobilności i Gospodarki Wodorowej"
    DEFINED_ = 7  # "DEJ | Departament Energii Jądrowej"
    CREATED_ = 8  # "DEK | Departament Edukacji i Komunikacji"
    WORK_ = 9  # "DELG | Departament Elektroenergetyki i Gazu"
    EXECUTED_ = 10  # "DFE | Departament Funduszy Europejskich"
    OVERDUE_ = 11  # "DGK | Departament Geologii i Koncesji Geologicznych"
    CLOSED_ = 12  # "DGO | Departament Gospodarki Odpadami"
    PAUSED_ = 13  # "DIŚ | Departament Instrumentów Środowiskowych"
    DEFINED_STATUS = 14  # "DLŁ | Departament Leśnictwa i Łowiectwa"
    CREATED_STATUS = 15  # "DNGS | Departament Nadzoru Geologicznego i Polityki Surowcowej"
    WORK_STATUS = 16  # "DOP | Departament Ochrony Przyrody"
    EXECUTED_STATUS = 17  # "DOZE | Departament Odnawialnych Źródeł Energii"
    OVER_STATUS = 18  # "DP | Departament Prawny"
    CLOSED_STATUS = 19  # "DPM | Departament Ochrony Powietrza i Polityki Miejskiej"
    PAUSED_STATUS = 20  # "DRP | Departament Ropy i Paliw Transportowych"
    DEFINED_STATUS_ = 21  # "DSM | Departament Spraw Międzynarodowych"
    CREATED_STATUS_ = 22  # "DSO | Departament Spraw Obronnych, Zarządzania Kryzysowego i Bezpieczeństwa"
    WORK_STATUS_ = 23  # "DSP | Departament Strategii i Planowania Transformacji Klimatycznej"
    OVERDUE_STATUS_CORRECT = 24  # "DP | Departament Prawny"
    OVERDUE_STATUS_C = 25  # "DP | Departament Prawny"
    OVERDUE_STATUS_CO = 26  # "DP | Departament Prawny"
    OVERDUE_STATUS_COR = 27  # "DP | Departament Prawny"
    OVERDUE_STATUS_CORR = 28  # "DP | Departament Prawny"
    OVERDUE_STATUS_CORRE = 29  # "DP | Departament Prawny"
    OVERDUE_STATUS_CORREC = 30  # "DP | Departament Prawny"


class TaskType(Enum):
    SIMPLE = 0
    CONDITIONAL = 1
    PLAN = 2


class TaskPriority(Enum):
    MINOR = 0  # "A | Samodzielne Stanowisko do spraw Analiz"
    LOW = 1  # "BDO | Zespół BDO"
    REGULAR = 2  # "BHP | Zespół ds. Bezpieczeństwa i Higieny Pracy oraz Ochrony Przeciwpożarowej"
    HIGH = 3  # "BiF | Samodzielne stanowisko ds. budżetu i finansów"
    CRITICAL = 4  # "BO | Zespół do Spraw Badań Oceanicznych"
    MINOR_ = 5  # "CZK | Centrum Zarządzania Kryzysowego"
    LOW_ = 6  # "F | Samodzielne Stanowisko ds. Finansowych"
    REGULAR_REGULAR = 7  # "GKM | Główny Księgowy Ministerstwa"
    HIGH_ = 8  # "GKR |	Główny Księgowy Resortu"
    CRITICAL_ = 9  # "GMO |	Zespół ds. Organizmów Genetycznie Zmodyfikowanych"
    MINOR_P = 10  # "I |	Wydział Instrumentów Ekonomicznych i Zarządzania Środowiskowego"
    LOW_P = 11  # "I1 | Wydział Energetyki Lokalnej"
    REGULAR_P = 12  # "II | Wydział Najlepszych Dostępnych Technik"
    HIGH_P = 13  # "II2 | Wydział Morskiej Energetyki Wiatrowej"
    CRITICAL_P = 14  # "III | Wydział Orzecznictwa Administracyjnego"
    MINOR_P_ = 15  # "III3 |	Wydział Regulacji i Mechanizmów Wsparcia Odnawialnych Źródeł Energii"
    LOW_P_ = 16  # "IOD | Inspektor Ochrony Danych"
    REGULAR_P_ = 17  # "IV |	Wydział Udostępniania Informacji o Środowisku"
    HIGH_P_ = 18  # "IV4 | Zespół do Spraw Międzynarodowych"
    CRITICAL_P_ = 19  # "KTMK | Kancelaria Tajna"
    MINOR_P_P = 20  # "M | Zespół ds. Międzynarodowych"
    LOW_P_P = 21  # "OK | Wydział Odpadów Komunalnych"
    REGULAR_P_P = 22  # "OP | Wydział Odpadów Poużytkowych"
    HIGH_P_P = 23  # "OPZ | Zespół Ochrony Powierzchni Ziemi"
    CRITICAL_P_P = 24  # "OZ | Zespół ds. Ochrony Złóż"
    MINOR_P_P_ = 25  # "P | Wydział Protokolarny"
    LOW_P_P_ = 26  # "PO | Wydział Przetwarzania Odpadów"
    REGULAR_P_P_ = 27  # "POIiŚ | Wydział Kontroli POIiŚ"
    HIGH_P_P_ = 28  # "POIN | Pełnomocnik do spraw Ochrony Informacji Niejawnych"
    CRITICAL_P_P_ = 29  # "RI | Wydział Kontroli Resortowej I"
    MINOR_P_P_P = 30  # "RII | Wydział Kontroli Resortowej II"
    LOW_P_P_P = 31  # "SOK |	Samodzielne Stanowisko ds. Organizacyjnych i Komunikacji"
    REGULAR_P_P_P = 32  # "SP | Wydział Strategii i Planowania"
    HIGH_P_P_P = 33  # "SSOP | Samodzielne Stanowisko ds. Obsługi Prawnej"
    CRITICAL_P_P_P = 34  # "SSP | Samodzielne stanowisko pracy ds. nadzoru nad Prezesem Państwowej Agencji Atomistyki"
    MINOR_P_P_P_ = 35  # "TK | Samodzielne Stanowisko ds. Działań Służących Transformacji Klimatycznej"
    LOW_P_P_P_ = 36  # "UE |	Wydział Unii Europejskiej"
    REGULAR_P_P_P_ = 37  # "V | Zespół ds. Oddziaływań Fizycznych"
    HIGH_P_P_P_ = 38  # "WAF | Wydział Administracyjno-Finansowy"
    CRITICAL_P_P_P_ = 39  # "WB | Wydział Budżetu"
    MINOR_P_P_P_P = 40  # "WBRI | Wydział Budżetu Resortu I"
    LOW_P_P_P_P = 41  # "WBRII | Wydział Budżetu Resortu II"
    REGULAR_P_P_P_P = 42  # "WCB | Wydział ds. Cyberbezpieczeństwa"
    HIGH_P_P_P_P = 43  # "WD | Wydział Współpracy Dwustronnej"
    CRITICAL_P_P_P_P = 44  # "WE | Wydział Elektromobilności"
    MINOR_P_P_P_P_ = 45  # "WE1 | Wydział ds. Ekoinnowacji"
    LOW_P_P_P_P_ = 46  # "WEE | Wydział Efektywności Energetycznej"
    REGULAR_P_P_P_P_ = 47  # "WEG | Wydział Gospodarki Wodorowej i Innowacji"
    HIGH_P_P_P_P_ = 48  # "WEP | Wydział Edukacji Ekologicznej i Promocji"
    CRITICAL_P_P_P_P_ = 49  # "WEP1 | Wydział ds. Emisji do Powietrza"
    MINOR_P_P_P_P_P = 50  # "WESI | Wydział Eksploatacji Systemów Informatycznych"
    LOW_P_P_P_P_P = 51  # "WFN | Wydział Funduszy Norweskich"
    REGULAR_P_P_P_P_P = 52  # "WG | Wydział Gazu Ziemnego"
    HIGH_P_P_P_P_P = 53  # "WGL | Wydział Gospodarki Leśnej"
    CRITICAL_P_P_P_P_P = 54  # "WHG | Wydział Hydrogeologii, Geologii Inżynierskiej i Geotermii"
    MINOR_P_P_P_P_P_ = 55  # "WIG | Wydział Informacji Geologicznej"
    LOW_P_P_P_P_P_ = 56  # "WIJ | Wydział Infrastruktury Jądrowej, Jądrowego Cyklu Paliwowego oraz Rozwoju Zaplecza Naukowo-Badawczego"
    REGULAR_P_P_P_P_P_ = 57  # "WIP | Wydział Informacji i Promocji"
    HIGH_P_P_P_P_P_ = 58  # "WIS | Wydział Informacji Społecznej oraz Rozwoju Kadr na Potrzeby Energetyki Jądrowej"
    CRITICAL_P_P_P_P_P_ = 59  # "WISM | Wydział Infrastruktury i Spraw Międzynarodowych"
    MINOR_P_P_P_P_P_P = 60  # "WIZ | Wydział Instrumentów Zarządzania i Ochrony Warstwy Ozonowej"
    LOW_P_P_P_P_P_P = 61  # "WJP	| Wydział ds. Jakości Powietrza"
    REGULAR_P_P_P_P_P_P = 62  # "WJPT | Wydział Jakości Paliw Transportowych i Bezpieczeństwa Technicznego"
    HIGH_P_P_P_P_P_P = 63  # "WJRE | Wydział Jednolitego Rynku Energii"
    CRITICAL_P_P_P_P_P_P = 64  # "WK	| Wydział Kadr"
    MINOR_P_P_P_P_P_P_ = 65  # "WK1 | Wydział ds. Konwencji"
    LOW_P_P_P_P_P_P_ = 66  # "WK2 | Wydział Księgowości"
    REGULAR_P_P_P_P_P_P_ = 67  # "WKF | Wydział Analiz i Koordynacji Finansowania"
    HIGH_P_P_P_P_P_P_ = 68  # "WKiC | Wydział Kogeneracji i Ciepłownictwa"
    CRITICAL_P_P_P_P_P_P_ = 69  # "WKM | Wydział Komunikacji Medialnej"
    MINOR_P_P_P_P_P_P_P = 70  # "WKO	| Wydział Komunikacyjno-Organizacyjny"
    LOW_P_P_P_P_P_P_P = 71  # "WKS | Wydział Kopalin Stałych"
    REGULAR_P_P_P_P_P_P_P = 72  # "WKSN | Wydział Koordynacji Systemu i Nadzoru"
    HIGH_P_P_P_P_P_P_P = 73  # "WKW	| Wydział Koordynacji Wdrażania"
    CRITICAL_P_P_P_P_P_P_P = 74  # "WL | Wydział Logistyki"
    MINOR_P_P_P_P_P_P_P_ = 75  # "WŁ	| Wydział Łowiectwa"
    LOW_P_P_P_P_P_P_P_ = 76  # "WM | Wydział Monitorowania"
    REGULAR_P_P_P_P_P_P_P_ = 77  # "WMA | Wydział ds. Miast i Adaptacji do Zmian Klimatu"
    HIGH_P_P_P_P_P_P_P_ = 78  # "WMM	| Wydział Współpracy Międzynarodowej"
    CRITICAL_P_P_P_P_P_P_P_ = 79  # "WN | Wydział Nieprawidłowości"
    MINOR_P_P_P_P_P_P_P_P = 80  # "WNL | Wydział Nieruchomości Leśnych"
    LOW_P_P_P_P_P_P_P_P = 81  # "WNM	| Wydział Negocjacji Międzynarodowych"
    REGULAR_P_P_P_P_P_P_P_P = 82  # "WNPP | Wydział Naboru i Promocji Pracodawcy"
    HIGH_P_P_P_P_P_P_P_P = 83  # "WNSR | Wydział Naruszeń i Systemów Raportowania"
    CRITICAL_P_P_P_P_P_P_P_P = 84  # "WOA | Wydział Organizacji i Archiwum"
    MINOR_P_P_P_P_P_P_P_P_ = 85  # "WOK | Wydział Obsługi Kancelaryjnej"
    LOW_P_P_P_P_P_P_P_P_ = 86  # "WOP | Wydział Obsługi Prawnej i Legislacji"
    REGULAR_P_P_P_P_P_P_P_P_ = 87  # "WOP1 | Wydział Obsługi Prawnej I"
    HIGH_P_P_P_P_P_P_P_P_ = 88  # "WOP2 | Wydział Obsługi Prawnej II"
    CRITICAL_P_P_P_P_P_P_P_P_ = 89  # "WOPIII | Wydział Obsługi Prawnej III"
    MINOR_P_P_P_P_P_P_P_P_P = 90  # "WOPIV | Wydział Obsługi Prawnej IV"
    LOW_P_P_P_P_P_P_P_P_P = 91  # "WOPL | Wydział Obsługi Prawno-Legislacyjnej"
    REGULAR_P_P_P_P_P_P_P_P_P = 92  # "WOPV | Wydział Obsługi Prawnej V"
    HIGH_P_P_P_P_P_P_P_P_P = 93  # "WOPVI | Wydział Obsługi Prawnej VI"
    CRITICAL_P_P_P_P_P_P_P_P_P = 94  # "WOŚ | Wydział ds. Ochrony Środowiska"
    MINOR_P_P_P_P_P_P_P_P_P_ = 95  # "WP	| Wydział Płac"
    LOW_P_P_P_P_P_P_P_P_P_ = 96  # "WPE | Wydział Programowania i Ewaluacji"
    REGULAR_P_P_P_P_P_P_P_P_P_ = 97  # "WPE1 | Wydział Polityki Energetycznej"
    HIGH_P_P_P_P_P_P_P_P_P_ = 98  # "WPN	| Wydział Programowania Prac Państwowej Służby Geologicznej i Nadzoru"
    CRITICAL_P_P_P_P_P_P_P_P_P_ = 99  # "WPN1 | Wydział ds. Parków Narodowych"
    MINOR_P_P_P_P_P_P_P_P_P_P = 100  # "WPR | Wydział Przepływów i Rachunkowości"
    LOW_P_P_P_P_P_P_P_P_P_P = 101  # "WPRPT | Wydział Planowania, Rozliczania i Pomocy Technicznej"
    REGULAR_P_P_P_P_P_P_P_P_P_P = 102  # "WPS	| Wydział Polityki Surowcowej"
    HIGH_P_P_P_P_P_P_P_P_P_P = 103  # "WR	| Wydział Regulacji"
    CRITICAL_P_P_P_P_P_P_P_P_P_P = 104  # "WRD | Wydział Rynku Detalicznego i Sieci Inteligentnych"
    MINOR_P_P_P_P_P_P_P_P_P_P_ = 105  # "WRH | Wydział Rynku Hurtowego, Bezpieczeństwa i Infrastruktury"
    LOW_P_P_P_P_P_P_P_P_P_P_ = 106  # "WRP | Wydział Realizacji Płatności"
    REGULAR_P_P_P_P_P_P_P_P_P_P_ = 107  # "WRRN | Wydział Rynku Ropy Naftowej, Paliw i Biopaliw Ciekłych"
    HIGH_P_P_P_P_P_P_P_P_P_P_ = 108  # "WRS | Wydział Rezerw Strategicznych"
    CRITICAL_P_P_P_P_P_P_P_P_P_P_ = 109  # "WRSI | Wydział Rozwoju Systemów Informatycznych"
    MINOR_P_P_P_P_P_P_P_P_P_P_P = 110  # "WRZ	| Wydział Rozwoju Zawodowego"
    LOW_P_P_P_P_P_P_P_P_P_P_P = 111  # "WSA | Wydział Sprawozdawczości i Analiz"
    REGULAR_P_P_P_P_P_P_P_P_P_P_P = 112  # "WSL | Wydział Szkolnictwa Leśnego"
    HIGH_P_P_P_P_P_P_P_P_P_P_P = 113  # "WSO | Wydział do Spraw Obronnych"
    CRITICAL_P_P_P_P_P_P_P_P_P_P_P = 114  # "WSR | Wydział Strategii i Regulacji"
    MINOR_P_P_P_P_P_P_P_P_P_P_P_ = 115  # "WTE | Wydział Transformacji Sektora Elektroenergetycznego i Spraw Międzynarodowych"
    LOW_P_P_P_P_P_P_P_P_P_P_P_ = 116  # "WW | Wydział Wdrażania POIiŚ"
    REGULAR_P_P_P_P_P_P_P_P_P_P_P_ = 117  # "WW1 | Wydział Współpracy Wielostronnej"
    HIGH_P_P_P_P_P_P_P_P_P_P_P_ = 118  # "WW2	| Wydział Węglowodorów"
    CRITICAL_P_P_P_P_P_P_P_P_P_P_P_ = 119  # "WZ | Wydział Zarządzania POIiŚ"
    MINOR_P_P_P_P_P_P_P_P_P_P_P_P = 120  # "WZE | Wydział Zarządzania Emisjami Gazów Cieplarnianych"
    LOW_P_P_P_P_P_P_P_P_P_P_P_P = 121  # "WZF	| Wydział Zarządzania Finansowego"
    REGULAR_P_P_P_P_P_P_P_P_P_P_P_P = 122  # "WZK | Wydział Zarządzania Kryzysowego"
    HIGH_P_P_P_P_P_P_P_P_P_P_P_P = 123  # "WZN | Wydział Zarządzania Nieruchomością"
    CRITICAL_P_P_P_P_P_P_P_P_P_P_P_P = 124  # "WZP | Wydział Zamówień Publicznych"
    MINOR_P_P_P_P_P_P_P_P_P_P_P_P_ = 125  # "ZA | Zespół ds. Audytu Wewnętrznego"
    LOW_P_P_P_P_P_P_P_P_P_P_P_P_ = 126  # "ZA1 | Zespół Analiz"
    REGULAR_P_P_P_P_P_P_P_P_P_P_P_P_ = 127  # "ZB | Zespół do spraw Bezpieczeństwa"
    HIGH_P_P_P_P_P_P_P_P_P_P_P_P_ = 128  # "ZDK | Zespół Dokumentowania Złóż Kopalin i Kwalifikacji Geologicznych"
    CRITICAL_P_P_P_P_P_P_P_P_P_P_P_P_ = 129  # "ZF | Zespół ds. Finansowych"
    MINOR_P_P_P_P_P_P_P_P_P_P_P_P_P = 130  # "ZKLZ | Zespół Koordynacji Legislacji Zewnętrznej"
    LOW_P_P_P_P_P_P_P_P_P_P_P_P_P = 131  # "ZOE | Zespół Opłat Eksploatacyjnych"
    REGULAR_P_P_P_P_P_P_P_P_P_P_P_P_P = 132  # "ZOL |	Zespół Orzecznictwa, Legislacji i Analiz"
    HIGH_P_P_P_P_P_P_P_P_P_P_P_P_P = 133  # "ZOPP | Zespół ds. Obsługi Prawnej i Procesowej"
    CRITICAL_P_P_P_P_P_P_P_P_P_P_P_P_P = 134  # "ZP |	Zespół ds. Prawnych"
    MINOR_P_P_P_P_P_P_P_P_P_P_P_P_P_ = 135  # "ZP1 | Zespół ds. Parlamentarnych"
    LOW_P_P_P_P_P_P_P_P_P_P_P_P_P_ = 136  # "ZP2 | Zespół ds. Prawnych"
    REGULAR_P_P_P_P_P_P_P_P_P_P_P_P_P_ = 137  # "ZPE | Zespół Polityki Ekologicznej"
    HIGH_P_P_P_P_P_P_P_P_P_P_P_P_P_ = 138  # "ZSWP | Zespół ds. Skarg, Wniosków i Petycji"
    CRITICAL_P_P_P_P_P_P_P_P_P_P_P_P_P_ = 139  # "ZUIT | Zespół ds. Utrzymania Infrastruktury Teleinformatycznej Ministerstwa"
    MINOR_CORRECT = 140


class TaskTwo(Enum):
    PLANNED = 0
    COMPLETE = 1


class ActionOnExecutedTask(Enum):
    DELETE = 1
    CLOSE = 2
    MARK_AS_EXECUTED = 4


class ActionOnOverdueTask(Enum):
    MARK_AS_OVERDUE = 0
    DELETE = 1
    CLOSE = 2


class TaskRelation(Enum):
    BLOCKING = 0
    DEPENDS_ON = 1
    CONNECTED = 2


class GroupType(Enum):
    SYSTEM = 0
    CUSTOM = 1


class DateType(Enum):
    START = 0
    END = 1
    EVENT = 2


class RepetitionType(Enum):
    ANNUALLY = 0
    MONTHLY = 1
    WEEKLY = 2
    DAILY = 3


class PlanCompletionCriteria(Enum):
    NEVER = 0
    AFTER_NUMBER_OF_TIMES = 1
    AFTER_DATE = 2


class PlanState(Enum):
    ACTIVE = 0
    NOT_ACTIVE = 1


class RelationConfirmation(Enum):
    NOT_CONFIRMED = 0
    CONFIRMED = 1


class SubtaskDependency(Enum):
    NOT_DEPENDENT = 0
    DEPENDENT = 1


class Used(Enum):
    NO = 0
    YES = 1


class Active(Enum):
    NO = 0
    YES = 1


STATUS_DEFINED = "BDG | Biuro Dyrektora Generalnego"
STATUS_CREATED = "BF | Biuro Finansowe"
STATUS_WORK = "BKA | Biuro Kontroli i Audytu"
STATUS_EXECUTED = "BM | Biuro Ministra"
STATUS_OVERDUE = "DB | Departament Budżetu"
STATUS_CLOSED = "DC | Departament Ciepłownictwa"
STATUS_PAUSED = "DEG | Departament Elektromobilności i Gospodarki Wodorowej"
STATUS_DEFINED_ = "DEJ | Departament Energii Jądrowej"
STATUS_CREATED_ = "DEK | Departament Edukacji i Komunikacji"
STATUS_WORK_ = "DELG | Departament Elektroenergetyki i Gazu"
STATUS_EXECUTED_ = "DFE | Departament Funduszy Europejskich"
STATUS_OVERDUE_ = "DGK | Departament Geologii i Koncesji Geologicznych"
STATUS_CLOSED_ = "DGO | Departament Gospodarki Odpadami"
STATUS_PAUSED_ = "DIŚ | Departament Instrumentów Środowiskowych"
STATUS_DEFINED_STATUS = "DLŁ | Departament Leśnictwa i Łowiectwa"
STATUS_CREATED_STATUS = "DNGS | Departament Nadzoru Geologicznego i Polityki Surowcowej"
STATUS_WORK_STATUS = "DOP | Departament Ochrony Przyrody"
STATUS_EXECUTED_STATUS = "DOZE | Departament Odnawialnych Źródeł Energii"
STATUS_OVER_STATUS = "DP | Departament Prawny"
STATUS_CLOSED_STATUS = "DPM | Departament Ochrony Powietrza i Polityki Miejskiej"
STATUS_PAUSED_STATUS = "DRP | Departament Ropy i Paliw Transportowych"
STATUS_DEFINED_STATUS_ = "DSM | Departament Spraw Międzynarodowych"
STATUS_CREATED_STATUS_ = "DSO | Departament Spraw Obronnych, Zarządzania Kryzysowego i Bezpieczeństwa"
STATUS_WORK_STATUS_ = "DSP | Departament Strategii i Planowania Transformacji Klimatycznej"
STATUS_OVERDUE_STATUS_CORRECT = "DRP | Departament Ropy i Paliw Transportowych"
STATUS_OVERDUE_STATUS_C = "DP | Departament Prawny"
STATUS_OVERDUE_STATUS_CO = "DP | Departament Prawny"
STATUS_OVERDUE_STATUS_COR = "DP | Departament Prawny"
STATUS_OVERDUE_STATUS_CORR = "DP | Departament Prawny"
STATUS_OVERDUE_STATUS_CORRE = "DP | Departament Prawny"
STATUS_OVERDUE_STATUS_CORREC = "DP | Departament Prawny"


TASK_TYPE_SIMPLE = "simple"
TASK_TYPE_CONDITION = "condition"
TASK_TYPE_PLAN = "plan"

PRIORITY_MINOR = "A | Samodzielne Stanowisko do spraw Analiz"
PRIORITY_LOW = "BDO | Zespół BDO"
PRIORITY_REGULAR = "BHP | Zespół ds. Bezpieczeństwa i Higieny Pracy oraz Ochrony Przeciwpożarowej"
PRIORITY_HIGH = "BiF | Samodzielne stanowisko ds. budżetu i finansów"
PRIORITY_CRITICAL = "BO | Zespół do Spraw Badań Oceanicznych"
PRIORITY_MINOR_ = "CZK | Centrum Zarządzania Kryzysowego"
PRIORITY_LOW_ = "F | Samodzielne Stanowisko ds. Finansowych"
PRIORITY_REGULAR_REGULAR = "GKM | Główny Księgowy Ministerstwa"
PRIORITY_HIGH_ = "GKR |	Główny Księgowy Resortu"
PRIORITY_CRITICAL_ = "GMO |	Zespół ds. Organizmów Genetycznie Zmodyfikowanych"
PRIORITY_MINOR_P = "I |	Wydział Instrumentów Ekonomicznych i Zarządzania Środowiskowego"
PRIORITY_LOW_P = "I1 | Wydział Energetyki Lokalnej"
PRIORITY_REGULAR_P = "II | Wydział Najlepszych Dostępnych Technik"
PRIORITY_HIGH_P = "II2 | Wydział Morskiej Energetyki Wiatrowej"
PRIORITY_CRITICAL_P = "III | Wydział Orzecznictwa Administracyjnego"
PRIORITY_MINOR_P_ = "III3 |	Wydział Regulacji i Mechanizmów Wsparcia Odnawialnych Źródeł Energii"
PRIORITY_LOW_P_ = "IOD | Inspektor Ochrony Danych"
PRIORITY_REGULAR_P_ = "IV |	Wydział Udostępniania Informacji o Środowisku"
PRIORITY_HIGH_P_ = "IV4 | Zespół do Spraw Międzynarodowych"
PRIORITY_CRITICAL_P_ = "KTMK | Kancelaria Tajna"
PRIORITY_MINOR_P_P = "M | Zespół ds. Międzynarodowych"
PRIORITY_LOW_P_P = "OK | Wydział Odpadów Komunalnych"
PRIORITY_REGULAR_P_P = "OP | Wydział Odpadów Poużytkowych"
PRIORITY_HIGH_P_P = "OPZ | Zespół Ochrony Powierzchni Ziemi"
PRIORITY_CRITICAL_P_P = "OZ | Zespół ds. Ochrony Złóż"
PRIORITY_MINOR_P_P_ = "P | Wydział Protokolarny"
PRIORITY_LOW_P_P_ = "PO | Wydział Przetwarzania Odpadów"
PRIORITY_REGULAR_P_P_ = "POIiŚ | Wydział Kontroli POIiŚ"
PRIORITY_HIGH_P_P_ = "POIN | Pełnomocnik do spraw Ochrony Informacji Niejawnych"
PRIORITY_CRITICAL_P_P_ = "RI | Wydział Kontroli Resortowej I"
PRIORITY_MINOR_P_P_P = "RII | Wydział Kontroli Resortowej II"
PRIORITY_LOW_P_P_P = "SOK |	Samodzielne Stanowisko ds. Organizacyjnych i Komunikacji"
PRIORITY_REGULAR_P_P_P = "SP | Wydział Strategii i Planowania"
PRIORITY_HIGH_P_P_P = "SSOP | Samodzielne Stanowisko ds. Obsługi Prawnej"
PRIORITY_CRITICAL_P_P_P = "SSP | Samodzielne stanowisko pracy ds. nadzoru nad Prezesem Państwowej Agencji Atomistyki"
PRIORITY_MINOR_P_P_P_ = "TK | Samodzielne Stanowisko ds. Działań Służących Transformacji Klimatycznej"
PRIORITY_LOW_P_P_P_ = "UE |	Wydział Unii Europejskiej"
PRIORITY_REGULAR_P_P_P_ = "V | Zespół ds. Oddziaływań Fizycznych"
PRIORITY_HIGH_P_P_P_ = "WAF | Wydział Administracyjno-Finansowy"
PRIORITY_CRITICAL_P_P_P_ = "WB | Wydział Budżetu"
PRIORITY_MINOR_P_P_P_P = "WBRI | Wydział Budżetu Resortu I"
PRIORITY_LOW_P_P_P_P = "WBRII | Wydział Budżetu Resortu II"
PRIORITY_REGULAR_P_P_P_P = "WCB | Wydział ds. Cyberbezpieczeństwa"
PRIORITY_HIGH_P_P_P_P = "WD | Wydział Współpracy Dwustronnej"
PRIORITY_CRITICAL_P_P_P_P = "WE | Wydział Elektromobilności"
PRIORITY_MINOR_P_P_P_P_ = "WE1 | Wydział ds. Ekoinnowacji"
PRIORITY_LOW_P_P_P_P_ = "WEE | Wydział Efektywności Energetycznej"
PRIORITY_REGULAR_P_P_P_P_ = "WEG | Wydział Gospodarki Wodorowej i Innowacji"
PRIORITY_HIGH_P_P_P_P_ = "WEP | Wydział Edukacji Ekologicznej i Promocji"
PRIORITY_CRITICAL_P_P_P_P_ = "WEP1 | Wydział ds. Emisji do Powietrza"
PRIORITY_MINOR_P_P_P_P_P = "WESI | Wydział Eksploatacji Systemów Informatycznych"
PRIORITY_LOW_P_P_P_P_P = "WFN | Wydział Funduszy Norweskich"
PRIORITY_REGULAR_P_P_P_P_P = "WG | Wydział Gazu Ziemnego"
PRIORITY_HIGH_P_P_P_P_P = "WGL | Wydział Gospodarki Leśnej"
PRIORITY_CRITICAL_P_P_P_P_P = "WHG | Wydział Hydrogeologii, Geologii Inżynierskiej i Geotermii"
PRIORITY_MINOR_P_P_P_P_P_ = "WIG | Wydział Informacji Geologicznej"
PRIORITY_LOW_P_P_P_P_P_ = "WIJ | Wydział Infrastruktury Jądrowej, Jądrowego Cyklu Paliwowego oraz Rozwoju Zaplecza Naukowo-Badawczego"
PRIORITY_REGULAR_P_P_P_P_P_ = "WIP | Wydział Informacji i Promocji"
PRIORITY_HIGH_P_P_P_P_P_ = "WIS | Wydział Informacji Społecznej oraz Rozwoju Kadr na Potrzeby Energetyki Jądrowej"
PRIORITY_CRITICAL_P_P_P_P_P_ = "WISM | Wydział Infrastruktury i Spraw Międzynarodowych"
PRIORITY_MINOR_P_P_P_P_P_P = "WIZ | Wydział Instrumentów Zarządzania i Ochrony Warstwy Ozonowej"
PRIORITY_LOW_P_P_P_P_P_P = "WJP	| Wydział ds. Jakości Powietrza"
PRIORITY_REGULAR_P_P_P_P_P_P = "WJPT | Wydział Jakości Paliw Transportowych i Bezpieczeństwa Technicznego"
PRIORITY_HIGH_P_P_P_P_P_P = "WJRE | Wydział Jednolitego Rynku Energii"
PRIORITY_CRITICAL_P_P_P_P_P_P = "WK	| Wydział Kadr"
PRIORITY_MINOR_P_P_P_P_P_P_ = "WK1 | Wydział ds. Konwencji"
PRIORITY_LOW_P_P_P_P_P_P_ = "WK2 | Wydział Księgowości"
PRIORITY_REGULAR_P_P_P_P_P_P_ = "WKF | Wydział Analiz i Koordynacji Finansowania"
PRIORITY_HIGH_P_P_P_P_P_P_ = "WKiC | Wydział Kogeneracji i Ciepłownictwa"
PRIORITY_CRITICAL_P_P_P_P_P_P_ = "WKM | Wydział Komunikacji Medialnej"
PRIORITY_MINOR_P_P_P_P_P_P_P = "WKO	| Wydział Komunikacyjno-Organizacyjny"
PRIORITY_LOW_P_P_P_P_P_P_P = "WKS | Wydział Kopalin Stałych"
PRIORITY_REGULAR_P_P_P_P_P_P_P = "WKSN | Wydział Koordynacji Systemu i Nadzoru"
PRIORITY_HIGH_P_P_P_P_P_P_P = "WKW	| Wydział Koordynacji Wdrażania"
PRIORITY_CRITICAL_P_P_P_P_P_P_P = "WL | Wydział Logistyki"
PRIORITY_MINOR_P_P_P_P_P_P_P_ = "WŁ	| Wydział Łowiectwa"
PRIORITY_LOW_P_P_P_P_P_P_P_ = "WM | Wydział Monitorowania"
PRIORITY_REGULAR_P_P_P_P_P_P_P_ = "WMA | Wydział ds. Miast i Adaptacji do Zmian Klimatu"
PRIORITY_HIGH_P_P_P_P_P_P_P_ = "WMM	| Wydział Współpracy Międzynarodowej"
PRIORITY_CRITICAL_P_P_P_P_P_P_P_ = "WN | Wydział Nieprawidłowości"
PRIORITY_MINOR_P_P_P_P_P_P_P_P = "WNL | Wydział Nieruchomości Leśnych"
PRIORITY_LOW_P_P_P_P_P_P_P_P = "WNM	| Wydział Negocjacji Międzynarodowych"
PRIORITY_REGULAR_P_P_P_P_P_P_P_P = "WNPP | Wydział Naboru i Promocji Pracodawcy"
PRIORITY_HIGH_P_P_P_P_P_P_P_P = "WNSR | Wydział Naruszeń i Systemów Raportowania"
PRIORITY_CRITICAL_P_P_P_P_P_P_P_P = "WOA | Wydział Organizacji i Archiwum"
PRIORITY_MINOR_P_P_P_P_P_P_P_P_ = "WOK | Wydział Obsługi Kancelaryjnej"
PRIORITY_LOW_P_P_P_P_P_P_P_P_ = "WOP | Wydział Obsługi Prawnej i Legislacji"
PRIORITY_REGULAR_P_P_P_P_P_P_P_P_ = "WOP1 | Wydział Obsługi Prawnej I"
PRIORITY_HIGH_P_P_P_P_P_P_P_P_ = "WOP2 | Wydział Obsługi Prawnej II"
PRIORITY_CRITICAL_P_P_P_P_P_P_P_P_ = "WOPIII | Wydział Obsługi Prawnej III"
PRIORITY_MINOR_P_P_P_P_P_P_P_P_P = "WOPIV | Wydział Obsługi Prawnej IV"
PRIORITY_LOW_P_P_P_P_P_P_P_P_P = "WOPL | Wydział Obsługi Prawno-Legislacyjnej"
PRIORITY_REGULAR_P_P_P_P_P_P_P_P_P = "WOPV | Wydział Obsługi Prawnej V"
PRIORITY_HIGH_P_P_P_P_P_P_P_P_P = "WOPVI | Wydział Obsługi Prawnej VI"
PRIORITY_CRITICAL_P_P_P_P_P_P_P_P_P = "WOŚ | Wydział ds. Ochrony Środowiska"
PRIORITY_MINOR_P_P_P_P_P_P_P_P_P_ = "WP	| Wydział Płac"
PRIORITY_LOW_P_P_P_P_P_P_P_P_P_ = "WPE | Wydział Programowania i Ewaluacji"
PRIORITY_REGULAR_P_P_P_P_P_P_P_P_P_ = "WPE1 | Wydział Polityki Energetycznej"
PRIORITY_HIGH_P_P_P_P_P_P_P_P_P_ = "WPN	| Wydział Programowania Prac Państwowej Służby Geologicznej i Nadzoru"
PRIORITY_CRITICAL_P_P_P_P_P_P_P_P_P_ = "WPN1 | Wydział ds. Parków Narodowych"
PRIORITY_MINOR_P_P_P_P_P_P_P_P_P_P = "WPR | Wydział Przepływów i Rachunkowości"
PRIORITY_LOW_P_P_P_P_P_P_P_P_P_P = "WPRPT | Wydział Planowania, Rozliczania i Pomocy Technicznej"
PRIORITY_REGULAR_P_P_P_P_P_P_P_P_P_P = "WPS	| Wydział Polityki Surowcowej"
PRIORITY_HIGH_P_P_P_P_P_P_P_P_P_P = "WR	| Wydział Regulacji"
PRIORITY_CRITICAL_P_P_P_P_P_P_P_P_P_P = "WRD | Wydział Rynku Detalicznego i Sieci Inteligentnych"
PRIORITY_MINOR_P_P_P_P_P_P_P_P_P_P_ = "WRH | Wydział Rynku Hurtowego, Bezpieczeństwa i Infrastruktury"
PRIORITY_LOW_P_P_P_P_P_P_P_P_P_P_ = "WRP | Wydział Realizacji Płatności"
PRIORITY_REGULAR_P_P_P_P_P_P_P_P_P_P_ = "WRRN | Wydział Rynku Ropy Naftowej, Paliw i Biopaliw Ciekłych"
PRIORITY_HIGH_P_P_P_P_P_P_P_P_P_P_ = "WRS | Wydział Rezerw Strategicznych"
PRIORITY_CRITICAL_P_P_P_P_P_P_P_P_P_P_ = "WRSI | Wydział Rozwoju Systemów Informatycznych"
PRIORITY_MINOR_P_P_P_P_P_P_P_P_P_P_P = "WRZ	| Wydział Rozwoju Zawodowego"
PRIORITY_LOW_P_P_P_P_P_P_P_P_P_P_P = "WSA | Wydział Sprawozdawczości i Analiz"
PRIORITY_REGULAR_P_P_P_P_P_P_P_P_P_P_P = "WSL | Wydział Szkolnictwa Leśnego"
PRIORITY_HIGH_P_P_P_P_P_P_P_P_P_P_P = "WSO | Wydział do Spraw Obronnych"
PRIORITY_CRITICAL_P_P_P_P_P_P_P_P_P_P_P = "WSR | Wydział Strategii i Regulacji"
PRIORITY_MINOR_P_P_P_P_P_P_P_P_P_P_P_ = "WTE | Wydział Transformacji Sektora Elektroenergetycznego i Spraw Międzynarodowych"
PRIORITY_LOW_P_P_P_P_P_P_P_P_P_P_P_ = "WW | Wydział Wdrażania POIiŚ"
PRIORITY_REGULAR_P_P_P_P_P_P_P_P_P_P_P_ = "WW1 | Wydział Współpracy Wielostronnej"
PRIORITY_HIGH_P_P_P_P_P_P_P_P_P_P_P_ = "WW2	| Wydział Węglowodorów"
PRIORITY_CRITICAL_P_P_P_P_P_P_P_P_P_P_P_ = "WZ | Wydział Zarządzania POIiŚ"
PRIORITY_MINOR_P_P_P_P_P_P_P_P_P_P_P_P = "WZE | Wydział Zarządzania Emisjami Gazów Cieplarnianych"
PRIORITY_LOW_P_P_P_P_P_P_P_P_P_P_P_P = "WZF	| Wydział Zarządzania Finansowego"
PRIORITY_REGULAR_P_P_P_P_P_P_P_P_P_P_P_P = "WZK | Wydział Zarządzania Kryzysowego"
PRIORITY_HIGH_P_P_P_P_P_P_P_P_P_P_P_P = "WZN | Wydział Zarządzania Nieruchomością"
PRIORITY_CRITICAL_P_P_P_P_P_P_P_P_P_P_P_P = "WZP | Wydział Zamówień Publicznych"
PRIORITY_MINOR_P_P_P_P_P_P_P_P_P_P_P_P_ = "ZA | Zespół ds. Audytu Wewnętrznego"
PRIORITY_LOW_P_P_P_P_P_P_P_P_P_P_P_P_ = "ZA1 | Zespół Analiz"
PRIORITY_REGULAR_P_P_P_P_P_P_P_P_P_P_P_P_ = "ZB | Zespół do spraw Bezpieczeństwa"
PRIORITY_HIGH_P_P_P_P_P_P_P_P_P_P_P_P_ = "ZDK | Zespół Dokumentowania Złóż Kopalin i Kwalifikacji Geologicznych"
PRIORITY_CRITICAL_P_P_P_P_P_P_P_P_P_P_P_P_ = "ZF | Zespół ds. Finansowych"
PRIORITY_MINOR_P_P_P_P_P_P_P_P_P_P_P_P_P = "ZKLZ | Zespół Koordynacji Legislacji Zewnętrznej"
PRIORITY_LOW_P_P_P_P_P_P_P_P_P_P_P_P_P = "ZOE | Zespół Opłat Eksploatacyjnych"
PRIORITY_REGULAR_P_P_P_P_P_P_P_P_P_P_P_P_P = "ZOL |	Zespół Orzecznictwa, Legislacji i Analiz"
PRIORITY_HIGH_P_P_P_P_P_P_P_P_P_P_P_P_P = "ZOPP | Zespół ds. Obsługi Prawnej i Procesowej"
PRIORITY_CRITICAL_P_P_P_P_P_P_P_P_P_P_P_P_P = "ZP |	Zespół ds. Prawnych"
PRIORITY_MINOR_P_P_P_P_P_P_P_P_P_P_P_P_P_ = "ZP1 | Zespół ds. Parlamentarnych"
PRIORITY_LOW_P_P_P_P_P_P_P_P_P_P_P_P_P_ = "ZP2 | Zespół ds. Prawnych"
PRIORITY_REGULAR_P_P_P_P_P_P_P_P_P_P_P_P_P_ = "ZPE | Zespół Polityki Ekologicznej"
PRIORITY_HIGH_P_P_P_P_P_P_P_P_P_P_P_P_P_ = "ZSWP | Zespół ds. Skarg, Wniosków i Petycji"
PRIORITY_CRITICAL_P_P_P_P_P_P_P_P_P_P_P_P_P_ = "ZUIT | Zespół ds. Utrzymania Infrastruktury Teleinformatycznej Ministerstwa"
PRIORITY_MINOR_CORRECT = "GKM | Główny Księgowy Ministerstwa"


COMPLETE = "Planowane"
PLANNED = "Zrealizowane"

SUBTASK_BLOCKING = "blocking"
SUBTASK_DEPENDS = "depends"
SUBTASK_CONNECTED = "connected"

DATE_TYPE_START = "Data początkowa"
DATE_TYPE_END = "Data końcowa"
DATE_TYPE_EVENT = "Wydarzenie"


NO = "no"
YES = "yes"

PLAN_REPETITION_DAILY = "daily"
PLAN_REPETITION_WEEKLY = "weekly"
PLAN_REPETITION_MONTHLY = "monthly"
PLAN_REPETITION_ANNUALLY = "annually"

PLAN_ACTIVE_STATE = "active"
PLAN_INACTIVE_STATE = "inactive"

PLAN_END_NEVER = "never"
PLAN_END_DATE = "date"
PLAN_END_NUMBER = "number"

RESTRICTED_UPDATE_TASK_STATUSES = [STATUS_DEFINED, STATUS_CREATED]

STRING_TO_TASK_STATUS_ENUM = {
     STATUS_DEFINED:   TaskStatus.DEFINED.value,
     STATUS_CREATED:   TaskStatus.CREATED.value,
     STATUS_WORK:   TaskStatus.WORK.value,
     STATUS_EXECUTED:   TaskStatus.EXECUTED.value,
     STATUS_OVERDUE:   TaskStatus.OVERDUE.value,
     STATUS_CLOSED:   TaskStatus.CLOSED.value,
     STATUS_PAUSED:   TaskStatus.PAUSED.value,
     STATUS_DEFINED_: TaskStatus.DEFINED_.value,
     STATUS_CREATED_: TaskStatus.CREATED_.value,
     STATUS_WORK_: TaskStatus.WORK_.value,
     STATUS_EXECUTED_: TaskStatus.EXECUTED_.value,
     STATUS_OVERDUE_: TaskStatus.OVERDUE_.value,
     STATUS_CLOSED_: TaskStatus.CLOSED_.value,
     STATUS_PAUSED_: TaskStatus.PAUSED_.value,
     STATUS_DEFINED_STATUS: TaskStatus.DEFINED_STATUS.value,
     STATUS_CREATED_STATUS: TaskStatus.CREATED_STATUS.value,
     STATUS_WORK_STATUS: TaskStatus.WORK_STATUS.value,
     STATUS_EXECUTED_STATUS: TaskStatus.EXECUTED_STATUS.value,
     STATUS_OVER_STATUS: TaskStatus.OVER_STATUS.value,
     STATUS_CLOSED_STATUS: TaskStatus.CLOSED_STATUS.value,
     STATUS_PAUSED_STATUS: TaskStatus.PAUSED_STATUS.value,
     STATUS_DEFINED_STATUS_: TaskStatus.DEFINED_STATUS_.value,
     STATUS_CREATED_STATUS_: TaskStatus.CREATED_STATUS_.value,
     STATUS_WORK_STATUS_: TaskStatus.WORK_STATUS_.value,
     STATUS_OVERDUE_STATUS_CORRECT: TaskStatus.OVERDUE_STATUS_CORRECT.value,
     STATUS_OVERDUE_STATUS_C: TaskStatus.OVERDUE_STATUS_C.value,
     STATUS_OVERDUE_STATUS_CO: TaskStatus.OVERDUE_STATUS_CO.value,
     STATUS_OVERDUE_STATUS_COR: TaskStatus.OVERDUE_STATUS_COR.value,
     STATUS_OVERDUE_STATUS_CORR: TaskStatus.OVERDUE_STATUS_CORR.value,
     STATUS_OVERDUE_STATUS_CORRE: TaskStatus.OVERDUE_STATUS_CORRE.value,
     STATUS_OVERDUE_STATUS_CORREC: TaskStatus.OVERDUE_STATUS_CORREC.value
}


STRING_TO_UPDATABLE_TASK_STATUSES = {key: value for key, value in STRING_TO_TASK_STATUS_ENUM.items()
                                     if key not in RESTRICTED_UPDATE_TASK_STATUSES}
STRING_TO_TASK_TYPE_ENUM = {
     TASK_TYPE_SIMPLE:   TaskType.SIMPLE.value,
     TASK_TYPE_CONDITION:   TaskType.CONDITIONAL.value,
     TASK_TYPE_PLAN:   TaskType.PLAN.value
}

STRING_TO_TASK_PRIORITY_ENUM = {
     PRIORITY_MINOR:   TaskPriority.MINOR.value,
     PRIORITY_LOW:   TaskPriority.LOW.value,
     PRIORITY_REGULAR:   TaskPriority.REGULAR.value,
     PRIORITY_HIGH:   TaskPriority.HIGH.value,
     PRIORITY_CRITICAL:   TaskPriority.CRITICAL.value,
     PRIORITY_MINOR_: TaskPriority.MINOR_.value,
     PRIORITY_LOW_: TaskPriority.LOW_.value,
     PRIORITY_REGULAR_REGULAR: TaskPriority.REGULAR_REGULAR.value,
     PRIORITY_HIGH_: TaskPriority.HIGH_.value,
     PRIORITY_CRITICAL_: TaskPriority.CRITICAL_.value,
     PRIORITY_MINOR_P: TaskPriority.MINOR_P.value,
     PRIORITY_LOW_P: TaskPriority.LOW_P.value,
     PRIORITY_REGULAR_P: TaskPriority.REGULAR_P.value,
     PRIORITY_HIGH_P: TaskPriority.HIGH_P.value,
     PRIORITY_CRITICAL_P: TaskPriority.CRITICAL_P.value,
     PRIORITY_MINOR_P_: TaskPriority.MINOR_P_.value,
     PRIORITY_LOW_P_: TaskPriority.LOW_P_.value,
     PRIORITY_REGULAR_P_: TaskPriority.REGULAR_P_.value,
     PRIORITY_HIGH_P_: TaskPriority.HIGH_P_.value,
     PRIORITY_CRITICAL_P_: TaskPriority.CRITICAL_P_.value,
     PRIORITY_MINOR_P_P: TaskPriority.MINOR_P_P.value,
     PRIORITY_LOW_P_P: TaskPriority.LOW_P_P.value,
     PRIORITY_REGULAR_P_P: TaskPriority.REGULAR_P_P.value,
     PRIORITY_HIGH_P_P: TaskPriority.HIGH_P_P.value,
     PRIORITY_CRITICAL_P_P: TaskPriority.CRITICAL_P_P.value,
     PRIORITY_MINOR_P_P_: TaskPriority.MINOR_P_P_.value,
     PRIORITY_LOW_P_P_: TaskPriority.LOW_P_P_.value,
     PRIORITY_REGULAR_P_P_: TaskPriority.REGULAR_P_P_.value,
     PRIORITY_HIGH_P_P_: TaskPriority.HIGH_P_P_.value,
     PRIORITY_CRITICAL_P_P_: TaskPriority.CRITICAL_P_P_.value,
     PRIORITY_MINOR_P_P_P: TaskPriority.MINOR_P_P_P.value,
     PRIORITY_LOW_P_P_P: TaskPriority.LOW_P_P_P.value,
     PRIORITY_REGULAR_P_P_P: TaskPriority.REGULAR_P_P_P.value,
     PRIORITY_HIGH_P_P_P: TaskPriority.HIGH_P_P_P.value,
     PRIORITY_CRITICAL_P_P_P: TaskPriority.CRITICAL_P_P_P.value,
     PRIORITY_MINOR_P_P_P_: TaskPriority.MINOR_P_P_P_.value,
     PRIORITY_LOW_P_P_P_: TaskPriority.LOW_P_P_P_.value,
     PRIORITY_REGULAR_P_P_P_: TaskPriority.REGULAR_P_P_P_.value,
     PRIORITY_HIGH_P_P_P_: TaskPriority.HIGH_P_P_P_.value,
     PRIORITY_CRITICAL_P_P_P_: TaskPriority.CRITICAL_P_P_P_.value,
     PRIORITY_MINOR_P_P_P_P: TaskPriority.MINOR_P_P_P_P.value,
     PRIORITY_LOW_P_P_P_P: TaskPriority.LOW_P_P_P_P.value,
     PRIORITY_REGULAR_P_P_P_P: TaskPriority.REGULAR_P_P_P_P.value,
     PRIORITY_HIGH_P_P_P_P: TaskPriority.HIGH_P_P_P_P.value,
     PRIORITY_CRITICAL_P_P_P_P: TaskPriority.CRITICAL_P_P_P_P.value,
     PRIORITY_MINOR_P_P_P_P_: TaskPriority.MINOR_P_P_P_P_.value,
     PRIORITY_LOW_P_P_P_P_: TaskPriority.LOW_P_P_P_P_.value,
     PRIORITY_REGULAR_P_P_P_P_: TaskPriority.REGULAR_P_P_P_P_.value,
     PRIORITY_HIGH_P_P_P_P_: TaskPriority.HIGH_P_P_P_P_.value,
     PRIORITY_CRITICAL_P_P_P_P_: TaskPriority.CRITICAL_P_P_P_P_.value,
     PRIORITY_MINOR_P_P_P_P_P: TaskPriority.MINOR_P_P_P_P_P.value,
     PRIORITY_LOW_P_P_P_P_P: TaskPriority.LOW_P_P_P_P_P.value,
     PRIORITY_REGULAR_P_P_P_P_P: TaskPriority.REGULAR_P_P_P_P_P.value,
     PRIORITY_HIGH_P_P_P_P_P: TaskPriority.HIGH_P_P_P_P_P.value,
     PRIORITY_CRITICAL_P_P_P_P_P: TaskPriority.CRITICAL_P_P_P_P_P.value,
     PRIORITY_MINOR_P_P_P_P_P_: TaskPriority.MINOR_P_P_P_P_P_.value,
     PRIORITY_LOW_P_P_P_P_P_: TaskPriority.LOW_P_P_P_P_P_.value,
     PRIORITY_REGULAR_P_P_P_P_P_: TaskPriority.REGULAR_P_P_P_P_P_.value,
     PRIORITY_HIGH_P_P_P_P_P_: TaskPriority.HIGH_P_P_P_P_P_.value,
     PRIORITY_CRITICAL_P_P_P_P_P_: TaskPriority.CRITICAL_P_P_P_P_P_.value,
     PRIORITY_MINOR_P_P_P_P_P_P: TaskPriority.MINOR_P_P_P_P_P_P.value,
     PRIORITY_LOW_P_P_P_P_P_P: TaskPriority.LOW_P_P_P_P_P_P.value,
     PRIORITY_REGULAR_P_P_P_P_P_P: TaskPriority.REGULAR_P_P_P_P_P_P.value,
     PRIORITY_HIGH_P_P_P_P_P_P: TaskPriority.HIGH_P_P_P_P_P_P.value,
     PRIORITY_CRITICAL_P_P_P_P_P_P: TaskPriority.CRITICAL_P_P_P_P_P_P.value,
     PRIORITY_MINOR_P_P_P_P_P_P_: TaskPriority.MINOR_P_P_P_P_P_P_.value,
     PRIORITY_LOW_P_P_P_P_P_P_: TaskPriority.LOW_P_P_P_P_P_P_.value,
     PRIORITY_REGULAR_P_P_P_P_P_P_: TaskPriority.REGULAR_P_P_P_P_P_P_.value,
     PRIORITY_HIGH_P_P_P_P_P_P_: TaskPriority.HIGH_P_P_P_P_P_P_.value,
     PRIORITY_CRITICAL_P_P_P_P_P_P_: TaskPriority.CRITICAL_P_P_P_P_P_P_.value,
     PRIORITY_MINOR_P_P_P_P_P_P_P: TaskPriority.MINOR_P_P_P_P_P_P_P.value,
     PRIORITY_LOW_P_P_P_P_P_P_P: TaskPriority.LOW_P_P_P_P_P_P_P.value,
     PRIORITY_REGULAR_P_P_P_P_P_P_P: TaskPriority.REGULAR_P_P_P_P_P_P_P.value,
     PRIORITY_HIGH_P_P_P_P_P_P_P: TaskPriority.HIGH_P_P_P_P_P_P_P.value,
     PRIORITY_CRITICAL_P_P_P_P_P_P_P: TaskPriority.CRITICAL_P_P_P_P_P_P_P.value,
     PRIORITY_MINOR_P_P_P_P_P_P_P_: TaskPriority.MINOR_P_P_P_P_P_P_P_.value,
     PRIORITY_LOW_P_P_P_P_P_P_P_: TaskPriority.LOW_P_P_P_P_P_P_P_.value,
     PRIORITY_REGULAR_P_P_P_P_P_P_P_: TaskPriority.REGULAR_P_P_P_P_P_P_P_.value,
     PRIORITY_HIGH_P_P_P_P_P_P_P_: TaskPriority.HIGH_P_P_P_P_P_P_P_.value,
     PRIORITY_CRITICAL_P_P_P_P_P_P_P_: TaskPriority.CRITICAL_P_P_P_P_P_P_P_.value,
     PRIORITY_MINOR_P_P_P_P_P_P_P_P: TaskPriority.MINOR_P_P_P_P_P_P_P_P.value,
     PRIORITY_LOW_P_P_P_P_P_P_P_P: TaskPriority.LOW_P_P_P_P_P_P_P_P.value,
     PRIORITY_REGULAR_P_P_P_P_P_P_P_P: TaskPriority.REGULAR_P_P_P_P_P_P_P_P.value,
     PRIORITY_HIGH_P_P_P_P_P_P_P_P: TaskPriority.HIGH_P_P_P_P_P_P_P_P.value,
     PRIORITY_CRITICAL_P_P_P_P_P_P_P_P: TaskPriority.CRITICAL_P_P_P_P_P_P_P_P.value,
     PRIORITY_MINOR_P_P_P_P_P_P_P_P_: TaskPriority.MINOR_P_P_P_P_P_P_P_P_.value,
     PRIORITY_LOW_P_P_P_P_P_P_P_P_: TaskPriority.LOW_P_P_P_P_P_P_P_P_.value,
     PRIORITY_REGULAR_P_P_P_P_P_P_P_P_: TaskPriority.REGULAR_P_P_P_P_P_P_P_P_.value,
     PRIORITY_HIGH_P_P_P_P_P_P_P_P_: TaskPriority.HIGH_P_P_P_P_P_P_P_P_.value,
     PRIORITY_CRITICAL_P_P_P_P_P_P_P_P_: TaskPriority.CRITICAL_P_P_P_P_P_P_P_P_.value,
     PRIORITY_MINOR_P_P_P_P_P_P_P_P_P: TaskPriority.MINOR_P_P_P_P_P_P_P_P_P.value,
     PRIORITY_LOW_P_P_P_P_P_P_P_P_P: TaskPriority.LOW_P_P_P_P_P_P_P_P_P.value,
     PRIORITY_REGULAR_P_P_P_P_P_P_P_P_P: TaskPriority.REGULAR_P_P_P_P_P_P_P_P_P.value,
     PRIORITY_HIGH_P_P_P_P_P_P_P_P_P: TaskPriority.HIGH_P_P_P_P_P_P_P_P_P.value,
     PRIORITY_CRITICAL_P_P_P_P_P_P_P_P_P: TaskPriority.CRITICAL_P_P_P_P_P_P_P_P_P.value,
     PRIORITY_MINOR_P_P_P_P_P_P_P_P_P_: TaskPriority.MINOR_P_P_P_P_P_P_P_P_P_.value,
     PRIORITY_LOW_P_P_P_P_P_P_P_P_P_: TaskPriority.LOW_P_P_P_P_P_P_P_P_P_.value,
     PRIORITY_REGULAR_P_P_P_P_P_P_P_P_P_: TaskPriority.REGULAR_P_P_P_P_P_P_P_P_P_.value,
     PRIORITY_HIGH_P_P_P_P_P_P_P_P_P_: TaskPriority.HIGH_P_P_P_P_P_P_P_P_P_.value,
     PRIORITY_CRITICAL_P_P_P_P_P_P_P_P_P_: TaskPriority.CRITICAL_P_P_P_P_P_P_P_P_P_.value,
     PRIORITY_MINOR_P_P_P_P_P_P_P_P_P_P: TaskPriority.MINOR_P_P_P_P_P_P_P_P_P_P.value,
     PRIORITY_LOW_P_P_P_P_P_P_P_P_P_P: TaskPriority.LOW_P_P_P_P_P_P_P_P_P_P.value,
     PRIORITY_REGULAR_P_P_P_P_P_P_P_P_P_P: TaskPriority.REGULAR_P_P_P_P_P_P_P_P_P_P.value,
     PRIORITY_HIGH_P_P_P_P_P_P_P_P_P_P: TaskPriority.HIGH_P_P_P_P_P_P_P_P_P_P.value,
     PRIORITY_CRITICAL_P_P_P_P_P_P_P_P_P_P: TaskPriority.CRITICAL_P_P_P_P_P_P_P_P_P_P.value,
     PRIORITY_MINOR_P_P_P_P_P_P_P_P_P_P_: TaskPriority.MINOR_P_P_P_P_P_P_P_P_P_P_.value,
     PRIORITY_LOW_P_P_P_P_P_P_P_P_P_P_: TaskPriority.LOW_P_P_P_P_P_P_P_P_P_P_.value,
     PRIORITY_REGULAR_P_P_P_P_P_P_P_P_P_P_: TaskPriority.REGULAR_P_P_P_P_P_P_P_P_P_P_.value,
     PRIORITY_HIGH_P_P_P_P_P_P_P_P_P_P_: TaskPriority.HIGH_P_P_P_P_P_P_P_P_P_P_.value,
     PRIORITY_CRITICAL_P_P_P_P_P_P_P_P_P_P_: TaskPriority.CRITICAL_P_P_P_P_P_P_P_P_P_P_.value,
     PRIORITY_MINOR_P_P_P_P_P_P_P_P_P_P_P: TaskPriority.MINOR_P_P_P_P_P_P_P_P_P_P_P.value,
     PRIORITY_LOW_P_P_P_P_P_P_P_P_P_P_P: TaskPriority.LOW_P_P_P_P_P_P_P_P_P_P_P.value,
     PRIORITY_REGULAR_P_P_P_P_P_P_P_P_P_P_P: TaskPriority.REGULAR_P_P_P_P_P_P_P_P_P_P_P.value,
     PRIORITY_HIGH_P_P_P_P_P_P_P_P_P_P_P: TaskPriority.HIGH_P_P_P_P_P_P_P_P_P_P_P.value,
     PRIORITY_CRITICAL_P_P_P_P_P_P_P_P_P_P_P: TaskPriority.CRITICAL_P_P_P_P_P_P_P_P_P_P_P.value,
     PRIORITY_MINOR_P_P_P_P_P_P_P_P_P_P_P_: TaskPriority.MINOR_P_P_P_P_P_P_P_P_P_P_P_.value,
     PRIORITY_LOW_P_P_P_P_P_P_P_P_P_P_P_: TaskPriority.LOW_P_P_P_P_P_P_P_P_P_P_P_.value,
     PRIORITY_REGULAR_P_P_P_P_P_P_P_P_P_P_P_: TaskPriority.REGULAR_P_P_P_P_P_P_P_P_P_P_P_.value,
     PRIORITY_HIGH_P_P_P_P_P_P_P_P_P_P_P_: TaskPriority.HIGH_P_P_P_P_P_P_P_P_P_P_P_.value,
     PRIORITY_CRITICAL_P_P_P_P_P_P_P_P_P_P_P_: TaskPriority.CRITICAL_P_P_P_P_P_P_P_P_P_P_P_.value,
     PRIORITY_MINOR_P_P_P_P_P_P_P_P_P_P_P_P: TaskPriority.MINOR_P_P_P_P_P_P_P_P_P_P_P_P.value,
     PRIORITY_LOW_P_P_P_P_P_P_P_P_P_P_P_P: TaskPriority.LOW_P_P_P_P_P_P_P_P_P_P_P_P.value,
     PRIORITY_REGULAR_P_P_P_P_P_P_P_P_P_P_P_P: TaskPriority.REGULAR_P_P_P_P_P_P_P_P_P_P_P_P.value,
     PRIORITY_HIGH_P_P_P_P_P_P_P_P_P_P_P_P: TaskPriority.HIGH_P_P_P_P_P_P_P_P_P_P_P_P.value,
     PRIORITY_CRITICAL_P_P_P_P_P_P_P_P_P_P_P_P: TaskPriority.CRITICAL_P_P_P_P_P_P_P_P_P_P_P_P.value,
     PRIORITY_MINOR_P_P_P_P_P_P_P_P_P_P_P_P_: TaskPriority.MINOR_P_P_P_P_P_P_P_P_P_P_P_P_.value,
     PRIORITY_LOW_P_P_P_P_P_P_P_P_P_P_P_P_: TaskPriority.LOW_P_P_P_P_P_P_P_P_P_P_P_P_.value,
     PRIORITY_REGULAR_P_P_P_P_P_P_P_P_P_P_P_P_: TaskPriority.REGULAR_P_P_P_P_P_P_P_P_P_P_P_P_.value,
     PRIORITY_HIGH_P_P_P_P_P_P_P_P_P_P_P_P_: TaskPriority.HIGH_P_P_P_P_P_P_P_P_P_P_P_P_.value,
     PRIORITY_CRITICAL_P_P_P_P_P_P_P_P_P_P_P_P_: TaskPriority.CRITICAL_P_P_P_P_P_P_P_P_P_P_P_P_.value,
     PRIORITY_MINOR_P_P_P_P_P_P_P_P_P_P_P_P_P: TaskPriority.MINOR_P_P_P_P_P_P_P_P_P_P_P_P_P.value,
     PRIORITY_LOW_P_P_P_P_P_P_P_P_P_P_P_P_P: TaskPriority.LOW_P_P_P_P_P_P_P_P_P_P_P_P_P.value,
     PRIORITY_REGULAR_P_P_P_P_P_P_P_P_P_P_P_P_P: TaskPriority.REGULAR_P_P_P_P_P_P_P_P_P_P_P_P_P.value,
     PRIORITY_HIGH_P_P_P_P_P_P_P_P_P_P_P_P_P: TaskPriority.HIGH_P_P_P_P_P_P_P_P_P_P_P_P_P.value,
     PRIORITY_CRITICAL_P_P_P_P_P_P_P_P_P_P_P_P_P: TaskPriority.CRITICAL_P_P_P_P_P_P_P_P_P_P_P_P_P.value,
     PRIORITY_MINOR_P_P_P_P_P_P_P_P_P_P_P_P_P_: TaskPriority.MINOR_P_P_P_P_P_P_P_P_P_P_P_P_P_.value,
     PRIORITY_LOW_P_P_P_P_P_P_P_P_P_P_P_P_P_: TaskPriority.LOW_P_P_P_P_P_P_P_P_P_P_P_P_P_.value,
     PRIORITY_REGULAR_P_P_P_P_P_P_P_P_P_P_P_P_P_: TaskPriority.REGULAR_P_P_P_P_P_P_P_P_P_P_P_P_P_.value,
     PRIORITY_HIGH_P_P_P_P_P_P_P_P_P_P_P_P_P_: TaskPriority.HIGH_P_P_P_P_P_P_P_P_P_P_P_P_P_.value,
     PRIORITY_CRITICAL_P_P_P_P_P_P_P_P_P_P_P_P_P_: TaskPriority.CRITICAL_P_P_P_P_P_P_P_P_P_P_P_P_P_.value,
     PRIORITY_MINOR_CORRECT: TaskPriority.MINOR_CORRECT.value
}

STRING_TO_TASK_TWO_ENUM = {
    PLANNED: TaskTwo.PLANNED.value,
    COMPLETE:    TaskTwo.COMPLETE.value
}

STRING_TO_TASK_RELATION_ENUM = {
     SUBTASK_BLOCKING:   TaskRelation.BLOCKING.value,
     SUBTASK_DEPENDS:   TaskRelation.DEPENDS_ON.value,
     SUBTASK_CONNECTED:   TaskRelation.CONNECTED.value
}

STRING_TO_DATE_TYPE_ENUM = {
     DATE_TYPE_START:   DateType.START.value,
     DATE_TYPE_END:   DateType.END.value,
     DATE_TYPE_EVENT: DateType.EVENT.value
}

STRING_TO_REPETITION_TYPE_ENUM = {
     PLAN_REPETITION_ANNUALLY:   RepetitionType.ANNUALLY.value,
     PLAN_REPETITION_MONTHLY:   RepetitionType.MONTHLY.value,
     PLAN_REPETITION_WEEKLY:   RepetitionType.WEEKLY.value,
     PLAN_REPETITION_DAILY:   RepetitionType.DAILY.value
}

STRING_TO_PLAN_COMPLETION_CRITERIA_ENUM = {
     PLAN_END_NEVER:   PlanCompletionCriteria.NEVER.value,
     PLAN_END_DATE:   PlanCompletionCriteria.AFTER_DATE.value,
     PLAN_END_NUMBER:   PlanCompletionCriteria.AFTER_NUMBER_OF_TIMES.value
}

STRING_TO_PLAN_STATE_ENUM = {
     PLAN_ACTIVE_STATE:   PlanState.ACTIVE.value,
     PLAN_INACTIVE_STATE:   PlanState.NOT_ACTIVE.value
}

STRING_TO_SUBTASK_DEPENDENCY_ENUM = {
     NO:   SubtaskDependency.NOT_DEPENDENT.value,
     YES:   SubtaskDependency.DEPENDENT.value
}

STRING_TO_USED_ENUM = {
     NO:   Used.NO.value,
     YES:   Used.YES.value
}

STRING_TO_ACTIVE_ENUM = {
     NO:   Active.NO.value,
     YES:   Active.YES.value
}


def reverse_dictionary(dictionary):
    return {v: k for k, v in dictionary.items()}


TASK_STATUS_ENUM_TO_STRING = reverse_dictionary(STRING_TO_TASK_STATUS_ENUM)

TASK_TYPE_ENUM_TO_STRING = reverse_dictionary(STRING_TO_TASK_TYPE_ENUM)

TASK_PRIORITY_ENUM_TO_STRING = reverse_dictionary(STRING_TO_TASK_PRIORITY_ENUM)

TASK_TWO_ENUM_TO_STRING = reverse_dictionary(STRING_TO_TASK_TWO_ENUM)

TASK_RELATION_ENUM_TO_STRING = reverse_dictionary(STRING_TO_TASK_RELATION_ENUM)

DATE_TYPE_ENUM_TO_STRING = reverse_dictionary(STRING_TO_DATE_TYPE_ENUM)

REPETITION_TYPE_ENUM_TO_STRING = reverse_dictionary(STRING_TO_REPETITION_TYPE_ENUM)

PLAN_COMPLETION_CRITERIA_ENUM_TO_STRING = reverse_dictionary(STRING_TO_PLAN_COMPLETION_CRITERIA_ENUM)

PLAN_STATE_ENUM_TO_STRING = reverse_dictionary(STRING_TO_PLAN_STATE_ENUM)

SUBTASK_DEPENDENCY_ENUM_TO_STRING = reverse_dictionary(STRING_TO_SUBTASK_DEPENDENCY_ENUM)

USED_ENUM_TO_STRING = reverse_dictionary(STRING_TO_USED_ENUM)

ACTIVE_ENUM_TO_STRING = reverse_dictionary(STRING_TO_ACTIVE_ENUM)
