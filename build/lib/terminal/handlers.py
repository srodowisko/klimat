"""
    This module provides class for handling
    terminal arguments

    Classes:
        TerminalHandler - class for terminal arguments handling

    Functions:
        error_catcher - decorator to catch exceptions

"""
from sqlalchemy.exc import SQLAlchemyError

from planner4ucore.models import enums as me
from planner4ucore.models import models as mo
from planner4ucore.services import exceptions as ex

from terminal import initializations
from terminal.representations import printers
from terminal.representations import attributes_transformers as attrs_transf
from terminal.representations import arguments_to_attributes


def error_catcher(func):
    """
        Decorator which "wraps" function in
        try-except statement

        Handles exceptions by printing specified message
        Doesn't re-raise exceptions

    """
    def wrapper(*args, **kwargs):
        try:
            func(*args, **kwargs)
        except SQLAlchemyError:
            print("Internal data storage error occurred while adding information to database, see log for more details")
        except ex.Planner4UError as e:
            print(str(e))
        except Exception:
            print("Unhandled exception, see log file for more details")
    return wrapper


class TerminalHandler:
    """
        Contains all functions to handle input terminal arguments
        and make corresponding service requests

        Attributes:
            service (Services): object that provides methods
                                for data manipulations

            object_handlers - mapping to handle command for each object
            task_actions - mapping from action to its function for task
            plan_actions - mapping from action to its function for plan
            condition_actions - mapping from action to its function for condition
            group_actions - mapping from action to its function for group
            date_actions - mapping from action to its function for date
            reminder_actions - mapping from action to its function for reminder
            notfication_actions - mapping from action to its function for notification
            relation_handlers - mapping from relation object to its handler
            message_actions - mapping from action to its function for messaged
            subtask_relation_actions - mapping from action to its function for subtask relation
            group_relation_actions - mapping from action to its function for task group relation
            executor_relation_actions - mapping from action to its function for executor relation

        Functions:
            handle - entry function
            handle_command - executes function from given dict

            handle_create_task_command
            handle_create_plan_command
            handle_create_condition_command
            handle_create_group_command
            handle_create_date_command
            handle_create_reminder_command
            handle_create_notification_command
            handle_create_relation_command
            handle_create_task_group_relation
            handle_create_user_executor_relation_command
            handle_create_subtask_relation_command

            handle_show_task_command
            handle_show_plan_command
            handle_show_tasks_command
            handle_show_subtasks_command
            handle_show_relation_command
            handle_show_relation_condition_command
            handle_show_subtask_relation_command
            handle_show_date_command
            handle_show_group_command
            handle_show_groups_command
            handle_show_notifications_command
            handle_show_reminders_command
            handle_show_reminder_command
            handle_show_notification_command
            handle_show_message_command
            handle_show_messaegs_command

            handle_delete_task_command
            handle_delete_plan_command
            handle_delete_relation_command
            handle_delete_relation_condition_command
            handle_delete_subtask_relation_command
            handle_delete_date_command
            handle_delete_group_command
            handle_delete_reminder_command
            handle_delete_notification_command
            handle_delete_task_group_relation_command
            handle_delete_user_executor_relation_command

            handle_update_task_command
            handle_update_task_status_command
            handle_update_relation_command
            handle_update_relation_condition_command
            handle_update_subtask_relation_command
            handle_update_date_command
            handle_update_group_command
            handle_update_reminder_command
            handle_update_notification_command

    """

    def __init__(self, service):
        self.serv = service

        self.objects_handlers = {
            initializations.TASK_COMMAND: self.handle_task_command,
            initializations.PLAN_COMMAND: self.handle_plan_command,
            initializations.CONDITION_COMMAND: self.handle_condition_command,
            initializations.GROUP_COMMAND: self.handle_group_command,
            initializations.DATE_COMMAND: self.handle_date_command,
            initializations.REMINDER_COMMAND: self.handle_reminder_command,
            initializations.NOTIFICATION_COMMAND: self.handle_notification_command,
            initializations.RELATION_COMMAND: self.handle_relation_command,
            initializations.MESSAGE_COMMAND: self.handle_message_command,
            initializations.MESSAGES_COMMAND: self.handle_show_messages_command,
            initializations.SUBTASKS_COMMAND: self.handle_show_subtasks_command,
            initializations.TASKS_COMMAND: self.handle_show_tasks_command,
            initializations.GROUPS_COMMAND: self.handle_show_groups_command,
            initializations.USERS_COMMAND: self.handle_show_users_command,
            initializations.INIT_COMMAND: self.handle_init_command
        }

        self.task_actions = {
            initializations.ADD_COMMAND: self.handle_create_task_command,
            initializations.UPDATE_COMMAND: self.handle_update_task_command,
            initializations.SHOW_COMMAND: self.handle_show_task_command,
            initializations.DELETE_COMMAND: self.handle_delete_task_command,
        }

        self.plan_actions = {
            initializations.ADD_COMMAND: self.handle_create_plan_command,
            initializations.SHOW_COMMAND: self.handle_show_plan_command,
            initializations.DELETE_COMMAND: self.handle_delete_plan_command,
            initializations.UPDATE_COMMAND: self.handle_update_plan_command
        }

        self.condition_actions = {
            initializations.ADD_COMMAND: self.handle_create_condition_command,
            initializations.SHOW_COMMAND: self.handle_show_relation_condition_command,
            initializations.UPDATE_COMMAND: self.handle_update_relation_condition_command,
            initializations.DELETE_COMMAND: self.handle_delete_relation_condition_command
        }

        self.group_actions = {
            initializations.ADD_COMMAND: self.handle_create_group_command,
            initializations.SHOW_COMMAND: self.handle_show_group_command,
            initializations.UPDATE_COMMAND: self.handle_update_group_command,
            initializations.DELETE_COMMAND: self.handle_delete_group_command
        }

        self.date_actions = {
            initializations.ADD_COMMAND: self.handle_create_date_command,
            initializations.SHOW_COMMAND: self.handle_show_date_command,
            initializations.UPDATE_COMMAND: self.handle_update_date_command,
            initializations.DELETE_COMMAND: self.handle_delete_date_command,
        }

        self.reminder_actions = {
            initializations.ADD_COMMAND: self.handle_create_reminder_command,
            initializations.SHOW_COMMAND: self.handle_show_reminder_command,
            initializations.UPDATE_COMMAND: self.handle_update_reminder_command,
            initializations.DELETE_COMMAND: self.handle_delete_reminder_command
        }

        self.notification_actions = {
            initializations.ADD_COMMAND: self.handle_create_notification_command,
            initializations.SHOW_COMMAND: self.handle_show_notification_command,
            initializations.DELETE_COMMAND: self.handle_delete_notification_command,
            initializations.UPDATE_COMMAND: self.handle_update_notification_command
        }

        self.relation_handlers = {
            initializations.SUBTASK_COMMAND: self.handle_subtask_relation_command,
            initializations.GROUP_COMMAND: self.handle_task_group_relation_command,
            initializations.EXECUTOR_COMMAND: self.handle_executor_relation_command
        }

        self.message_actions = {
            initializations.SHOW_COMMAND: self.handle_show_message_command,
            initializations.DELETE_COMMAND: self.handle_delete_message_command
        }

        self.subtask_relation_actions = {
            initializations.ADD_COMMAND: self.handle_create_subtask_relation_command,
            initializations.SHOW_COMMAND: self.handle_show_subtask_relation_command,
            initializations.DELETE_COMMAND: self.handle_delete_subtask_relation_command,
            initializations.UPDATE_COMMAND: self.handle_update_subtask_relation_command,
        }

        self.group_relation_actions = {
            initializations.ADD_COMMAND: self.handle_create_task_group_relation_command,
            initializations.DELETE_COMMAND: self.handle_delete_task_group_relation_command
        }

        self.executor_relation_actions = {
            initializations.ADD_COMMAND: self.handle_create_user_executor_relation_command,
            initializations.DELETE_COMMAND: self.handle_delete_user_executor_relation_command
        }

    @staticmethod
    @error_catcher
    def handle_command(commands, command, *args, **kwargs):
        if command in commands:
            commands[command](*args, **kwargs)
        else:
            raise KeyError("Could not find handler for {} command".format(command))

    @error_catcher
    def handle(self, namespace):
        self.handle_command(self.objects_handlers, namespace.first_command, namespace=namespace)

    # region Object commands
    def handle_task_command(self, namespace, *args, **kwargs):
        self.handle_command(self.task_actions, namespace.second_command, namespace=namespace, *args, **kwargs)

    def handle_plan_command(self, namespace, *args, **kwargs):
        self.handle_command(self.plan_actions, namespace.second_command, namespace=namespace, *args, **kwargs)

    def handle_condition_command(self, namespace, *args, **kwargs):
        self.handle_command(self.condition_actions, namespace.second_command, namespace=namespace, *args, **kwargs)

    def handle_group_command(self, namespace, *args, **kwargs):
        self.handle_command(self.group_actions, namespace.second_command,
                            namespace=namespace, *args, **kwargs)

    def handle_date_command(self, namespace, *args, **kwargs):
        self.handle_command(self.date_actions, namespace.second_command, namespace=namespace, *args, **kwargs)

    def handle_reminder_command(self, namespace, *args, **kwargs):
        self.handle_command(self.reminder_actions, namespace.second_command, namespace=namespace, *args, **kwargs)

    def handle_notification_command(self, namespace, *args, **kwargs):
        self.handle_command(self.notification_actions, namespace.second_command, namespace=namespace, *args, **kwargs)

    def handle_relation_command(self, namespace, *args, **kwargs):
        self.handle_command(self.relation_handlers, namespace.second_command, namespace=namespace, *args, **kwargs)

    def handle_message_command(self, namespace, *args, **kwargs):
        self.handle_command(self.message_actions, namespace.second_command, namespace=namespace, *args, **kwargs)

    def handle_subtask_relation_command(self, namespace, *args, **kwargs):
        self.handle_command(self.subtask_relation_actions, namespace.third_command,
                            namespace=namespace, *args, **kwargs)

    def handle_task_group_relation_command(self, namespace, *args, **kwargs):
        self.handle_command(self.group_relation_actions, namespace.third_command,
                            namespace=namespace, *args, **kwargs)

    def handle_executor_relation_command(self, namespace, *args, **kwargs):
        self.handle_command(self.executor_relation_actions, namespace.third_command,
                            namespace=namespace, *args, **kwargs)

    @staticmethod
    def handle_init_command(service_class, data_storage_class, namespace):
        connection_string = namespace.connection_string
        service_class.set_up_database(data_storage_class, connection_string)
        print("Database was set up")
        if namespace.username:
            data_storage = data_storage_class(connection_string=namespace.connection_string)
            service = service_class(data_storage)
            user = service.add_user(mo.User(namespace.username))
            print("User was added: {} (id {})".format(user.name, user.user_id))
    # endregion

    # region Create commands
    def handle_create_task_command(self, namespace):
        user_creator = namespace.user_id
        args = attrs_transf.get_attributes(arguments_to_attributes.TASK_ATTRIBUTES_TRANSLATIONS,
                                           attrs_transf.TASK_ATTRIBUTES_TRANSFORMER, vars(namespace))

        task = mo.Task(task_type=me.TaskType.SIMPLE.value, user_creator=user_creator,
                       status=me.TaskStatus.CREATED.value, **args)

        task = self.serv.add_task(task)
        printers.show_task(task, header="------ Task was created: ")

        # Checking dates
        incompatible_date_selection = namespace.date and (namespace.date_start or namespace.date_end)
        if incompatible_date_selection:
            print("You have specified incompatible dates, they weren't added")
        elif namespace.date_end or namespace.date_start or namespace.date:
            one_date_creation = ((namespace.date_end and not namespace.date_start) or
                                 (namespace.date_start and not namespace.date_end) or
                                 namespace.date)
            if one_date_creation:
                if namespace.date:
                    date = mo.TaskDate(task_id=task.task_id, date_type=me.DateType.EVENT.value, date=namespace.date)
                else:
                    if namespace.date_end and not namespace.date_start:
                        date = mo.TaskDate(task_id=task.task_id, date_type=me.DateType.END.value,
                                           date=namespace.date_end)
                    else:
                        date = mo.TaskDate(task_id=task.task_id, date_type=me.DateType.START.value,
                                           date=namespace.date_start)
                date = self.serv.add_date(user_creator, date)
                printers.show_date(date, header="------ Date was was created: ")
            else:
                if namespace.date_end < namespace.date_start:
                    print("Start date can't be later than end date, dates weren't added")
                else:
                    start_date = mo.TaskDate(task_id=task.task_id, date_type=me.DateType.START.value,
                                             date=namespace.date_start)
                    end_date = mo.TaskDate(task_id=task.task_id, date_type=me.DateType.END.value,
                                           date=namespace.date_end)
                    start_date = self.serv.add_date(user_creator, start_date)
                    end_date = self.serv.add_date(user_creator, end_date)
                    printers.show_date(start_date, header="------ Start date was was created: ")
                    printers.show_date(end_date, header="------ End date was created")

    def handle_create_plan_command(self, namespace):
        user_creator = namespace.user_id
        if namespace.repetitions_number and namespace.end_date:
            print("Please, choose one completion criteria")
            return

        if namespace.repetitions_number:
            namespace.end_type = me.PLAN_END_NUMBER
        elif namespace.end_date:
            namespace.end_type = me.PLAN_END_DATE
        else:
            namespace.end_type = me.PLAN_END_NEVER

        task_args = attrs_transf.get_attributes(arguments_to_attributes.TASK_ATTRIBUTES_TRANSLATIONS,
                                                attrs_transf.TASK_ATTRIBUTES_TRANSFORMER, vars(namespace))
        plan_args = attrs_transf.get_attributes(arguments_to_attributes.PLAN_ATTRIBUTES_TRANSLATIONS,
                                                attrs_transf.PLAN_ATTRIBUTES_TRANSFORMER, vars(namespace))

        task = mo.Task(task_type=me.TaskType.PLAN.value, user_creator=user_creator,
                       status=me.TaskStatus.DEFINED.value, **task_args)

        plan = mo.Plan(**plan_args)

        task, plan = self.serv.add_plan(task, plan)

        printers.show_plan(plan, header="------ Plan was created:")
        printers.show_task(task, header="------ Task was created:")

    def handle_create_condition_command(self, namespace):
        user_creator = namespace.user_id

        # Getting task object and saving it
        task_args = attrs_transf.get_attributes(arguments_to_attributes.TASK_ATTRIBUTES_TRANSLATIONS,
                                                attrs_transf.TASK_ATTRIBUTES_TRANSFORMER, vars(namespace))
        task = mo.Task(task_type=me.TaskType.CONDITIONAL.value, user_creator=user_creator,
                       status=me.TaskStatus.DEFINED.value, **task_args)
        task = self.serv.add_task(task)

        # Getting condition relation object and saving it
        translation = arguments_to_attributes.CONDTION_TASK_RELATIONS_ATTRIBUTES_TRANSLATION
        condition_args = attrs_transf.get_attributes(translation,
                                                     attrs_transf.CONDITION_TASK_RELATIONS_ATTRIBUTES_TRANSFORMER,
                                                     vars(namespace))
        condition_relation = mo.ConditionTaskRelation(defined_task_id=task.task_id, **condition_args)
        condition_relation = self.serv.add_condition_relation(user_creator, condition_relation)

        printers.show_task(task, header="------ Task was created:")
        printers.show_condition_relation(condition_relation, header="------ Condition was created:")

    def handle_create_group_command(self, namespace):
        user_creator = namespace.user_id

        group_args = attrs_transf.get_attributes(arguments_to_attributes.GROUP_ATTRIBUTES_TRANSLATIONS,
                                                 attrs_transf.GROUP_ATTRIBUTES_TRANSFORMER, vars(namespace))
        group = mo.Group(user_creator_id=user_creator, **group_args)
        group = self.serv.add_group(group)
        printers.show_group(group, header="------ Group was created:")

    def handle_create_date_command(self, namespace):
        args = attrs_transf.get_attributes(arguments_to_attributes.TASK_DATE_ATTRIBUTES_TRANSLATIONS,
                                           attrs_transf.TASK_DATE_ATTRIBUTES_TRANSFORMER, vars(namespace))
        user_id = namespace.user_id
        date = mo.TaskDate(**args)
        date = self.serv.add_date(user_id, date)
        printers.show_date(date, header="------ Date was created:")

    def handle_create_reminder_command(self, namespace):
        args = attrs_transf.get_attributes(arguments_to_attributes.REMINDER_ATTRIBUTES_TRANSLATIONS,
                                           attrs_transf.REMINDER_ATTRIBUTES_TRANSFORMER, vars(namespace))
        user_id = namespace.user_id
        reminder = mo.Reminder(**args)
        self.serv.add_reminder(user_id, reminder)
        printers.show_reminder(reminder, header="------ Reminder was created:")

    def handle_create_notification_command(self, namespace):
        args = attrs_transf.get_attributes(arguments_to_attributes.NOTIFICATION_ATTRIBUTES_TRANSLATIONS,
                                           attrs_transf.NOTIFICATION_ATTRIBUTES_TRANSFORMER, vars(namespace))
        user_id = namespace.user_id
        notification = mo.Notification(**args)
        notification = self.serv.add_notification(user_id, notification)
        printers.show_notification(notification, header="------ Notification was created:")

    def handle_create_task_group_relation_command(self, namespace):
        args = attrs_transf.get_attributes(arguments_to_attributes.TASK_GROUP_ATTRIBUTES_TRANSLATIONS,
                                           attrs_transf.TASK_GROUP_ATTRIBUTES_TRANSFORMER, vars(namespace))

        user_id = namespace.user_id
        relation = mo.TaskGroupRelation(**args)
        relation = self.serv.add_task_group_relation(user_id, relation)
        printers.show_task_group_relation(relation, header="------ Task Group Relation was created:")

    def handle_create_user_executor_relation_command(self, namespace):
        user_id = namespace.user_id
        args = attrs_transf.get_attributes(arguments_to_attributes.USER_EXECUTOR_RELATION_ATTRIBUTES_TRANSLATIONS,
                                           attrs_transf.USER_EXECUTOR_RELATION_ATTRIBUTES_TRANSFORMER, vars(namespace))
        relation = mo.UserExecutorRelation(**args)
        relation = self.serv.add_user_executor_relation(user_id, relation)
        printers.show_user_executor_relation(relation, header="------ User Executor Relation was created:")

    def handle_create_subtask_relation_command(self, namespace):
        user_id = namespace.user_id

        args = attrs_transf.get_attributes(arguments_to_attributes.SUBTASK_RELATION_ATTRIBUTES_TRANSLATIONS,
                                           attrs_transf.SUBTASK_RELATION_ATTRIBUTES_TRANSFORMER, vars(namespace))
        relation = mo.SubtaskRelation(**args)
        relation = self.serv.add_subtask_relation(user_id, relation)
        printers.show_subtask_relation(relation, header="------ Subtask relation was created: ")

    # endregion

    # region Show commands
    def handle_show_task_command(self, namespace):
        user_id = namespace.user_id
        task_id = namespace.task_id
        task_view = self.serv.get_task_view(user_id, task_id)
        print("------ Task")
        printers.show_task(task_view.task)
        printers.show_plan(task_view.plan, header="------ Plan Schedule: ")
        printers.show_condition_relation(task_view.condition, header="------ Condition Relation: ")
        printers.show_user(task_view.user_creator, header="------ User Creator:")

        if task_view.dates:
            print("------ Dates: ")
            for date in task_view.dates:
                printers.show_date(date, oneline=True)
        if task_view.groups:
            print("------ Groups: ")
            for group in task_view.groups:
                printers.show_group(group, oneline=True)
        if task_view.executors:
            print("------ Executors: ")
            for executor in task_view.executors:
                printers.show_user(executor)
        if task_view.notifications:
            print("------ Notifications: ")
            for notification in task_view.notifications:
                printers.show_notification(notification, oneline=True)
        if task_view.reminders:
            print("------ Reminders:: ")
            for reminder in task_view.reminders:
                printers.show_reminder(reminder, oneline=True)

    def handle_show_plan_command(self, namespace):
        user_id = namespace.user_id
        plan_id = namespace.plan_id

        plan_view = self.serv.get_plan_view(user_id, plan_id)
        printers.show_plan(plan_view.plan, header="------ Plan: ")
        printers.show_task(plan_view.task, header="------ Template Task: ")

    def handle_show_tasks_command(self, namespace):
        user_id = namespace.user_id

        details = namespace.details
        tasks_view = self.serv.get_tasks_view(user_id)
        if tasks_view.creator_tasks:
            print("------ Creator: ")
            for task in tasks_view.creator_tasks:
                if details:
                    printers.show_task(task)
                    print("   ---")
                else:
                    printers.show_task(task, oneline=True)

        if tasks_view.executor_tasks:
            print("------ Executor: ")
            for task in tasks_view.executor_tasks:
                if details:
                    printers.show_task(task)
                    print("   ---")
                else:
                    printers.show_task(task, oneline=True)

        if not tasks_view.executor_tasks and not tasks_view.creator_tasks:
            print(" User doesn't have any tasks")

    def handle_show_subtasks_command(self, namespace):
        user_id = namespace.user_id
        task_id = namespace.task_id
        subtasks_view = self.serv.get_subtasks_view(user_id, task_id)

        def show_sub_tasks(sub_tasks_view, level):
            task = sub_tasks_view.task
            relation_type = me.TASK_RELATION_ENUM_TO_STRING.get(sub_tasks_view.relation_type, "p")
            relation_id = sub_tasks_view.relation_id
            print(("{tabs}|{r_id} {type}| {id} - {title} |{status}|"
                   .format(tabs="".join(["\t " for _ in range(0, level)]), type=relation_type, id=task.task_id,
                           title=task.title, r_id=relation_id,
                           status=me.TASK_STATUS_ENUM_TO_STRING[task.status])))
            for sub_task_view in sub_tasks_view.sub_task_views:
                show_sub_tasks(sub_task_view, level + 1)

        show_sub_tasks(subtasks_view, 0)

    def handle_show_relation_condition_command(self, namespace):
        relation_id = namespace.relation_id
        user_id = namespace.user_id
        relation = self.serv.get_condition_task_relation(user_id, relation_id)
        printers.show_condition_relation(relation, header="------ Relation Condition:")

    def handle_show_subtask_relation_command(self, namespace):
        user_id = namespace.user_id
        relation_id = namespace.relation_id
        relation = self.serv.get_subtask_relation(user_id, relation_id)
        printers.show_subtask_relation(relation, header="------ Subtask relation:")

    def handle_show_date_command(self, namespace):
        date_id = namespace.date_id
        user_id = namespace.user_id
        date_view = self.serv.get_date_view(user_id, date_id)
        printers.show_date(date_view.date, header="------ Date: ")
        printers.show_task(date_view.task, header="------ Date Task: ")

    def handle_show_group_command(self, namespace):
        group_id = namespace.group_id
        user_id = namespace.user_id
        group_view = self.serv.get_group_view(user_id, group_id)
        printers.show_group(group_view.group, header="------ Group: ")
        if group_view.tasks:
            print("------ Tasks: ")
            for task in group_view.tasks:
                printers.show_task(task, oneline=True)
        else:
            print("Group doesn't have any tasks")

    def handle_show_groups_command(self, namespace):
        user_id = namespace.user_id

        groups_view = self.serv.get_groups_view(user_id)
        if groups_view.groups:
            print("------ User Groups: ")
            for group in groups_view.groups:
                printers.show_group(group, oneline=True)
        else:
            print(" User doesn't have any groups")

    def handle_show_users_command(self, namespace):
        users = self.serv.get_users()
        for user in users:
            printers.show_user(user)

    def handle_show_notifications_command(self, namespace):
        user_id = namespace.user_id
        notifications_view = self.serv.get_notifications_view(user_id)

        if notifications_view.creator_notifications:
            print("------ Own Notifications: ")
            for notification in notifications_view.creator_notifications:
                printers.show_notification(notification, oneline=True)

        if notifications_view.executor_notifications:
            print("------ Executor Notifications: ")
            for notification in notifications_view.executor_notifications:
                printers.show_notification(notification, oneline=True)

        if not notifications_view.executor_notifications and not notifications_view.creator_notifications:
            print("User doesn't have any notifications")

    def handle_show_reminders_command(self, namespace):
        user_id = namespace.user_id
        reminders_view = self.serv.get_reminders_view(user_id)

        if reminders_view.creator_reminders:
            print("------ Own Reminders: ")
            for reminder in reminders_view.creator_reminders:
                printers.show_reminder(reminder, oneline=True)

        if reminders_view.executor_reminders:
            print("------ Executor Reminders: ")
            for reminder in reminders_view.executor_reminders:
                printers.show_reminder(reminder, oneline=True)

        if not reminders_view.creator_reminders and not reminders_view.executor_reminders:
            print("User doesn't have any reminders")

    def handle_show_reminder_command(self, namespace):
        user_id = namespace.user_id
        reminder_id = namespace.reminder_id

        reminder_view = self.serv.get_reminder_view(user_id, reminder_id)
        printers.show_reminder(reminder_view.reminder, header="------ Reminder: ")
        printers.show_task(reminder_view.task, header="------ Reminder Task: ")

    def handle_show_notification_command(self, namespace):
        user_id = namespace.user_id
        notification_id = namespace.notification_id
        notification_view = self.serv.get_notification_view(user_id, notification_id)
        printers.show_notification(notification_view.notification, header="------ Notification: ")
        printers.show_date(notification_view.date, header="------ Notification Date: ")
        printers.show_task(notification_view.task, header="------ Notification Task: ")

    def handle_show_message_command(self, namespace):
        user_id = namespace.user_id
        message_id = namespace.message_id
        message = self.serv.get_message(user_id, message_id)
        printers.show_message(message, header="------ Message: ")

    def handle_show_messages_command(self, namespace):
        user_id = namespace.user_id
        messages_view = self.serv.get_messages_view(user_id)
        if messages_view.messages:
            print("------ Messages: ")
            for message in messages_view.messages:
                printers.show_message(message, oneline=True)
        else:
            print("User doesn't have any messages")

    # endregion

    # region Delete commands
    def handle_delete_task_command(self, namespace):
        task_id = namespace.task_id
        user_id = namespace.user_id
        self.acknowledge_deletion(self.serv.delete_task(user_id, task_id))

    def handle_delete_plan_command(self, namespace):
        user_id = namespace.user_id
        plan_id = namespace.plan_id

        plan = self.serv.get_plan(user_id, plan_id)
        self.acknowledge_deletion(self.serv.delete_task(user_id, plan.task_id))

    def handle_delete_relation_condition_command(self, namespace):
        user_id = namespace.user_id
        relation_id = namespace.relation_id
        self.acknowledge_deletion(self.serv.delete_condition_relation(user_id, relation_id))

    def handle_delete_subtask_relation_command(self, namespace):
        user_id = namespace.user_id

        parent_task_id = namespace.parent_id
        sub_task_id = namespace.child_id

        self.acknowledge_deletion(self.serv.delete_subtask_relation(user_id, parent_task_id, sub_task_id))

    def handle_delete_date_command(self, namespace):
        user_id = namespace.user_id
        date_id = namespace.date_id
        self.acknowledge_deletion(self.serv.delete_date(user_id, date_id))

    def handle_delete_group_command(self, namespace):
        user_id = namespace.user_id
        group_id = namespace.group_id
        self.acknowledge_deletion(self.serv.delete_group(user_id, group_id))

    def handle_delete_reminder_command(self, namespace):
        user_id = namespace.user_id
        reminder_id = namespace.reminder_id
        self.acknowledge_deletion(self.serv.delete_reminder(user_id, reminder_id))

    def handle_delete_notification_command(self, namespace):
        user_id = namespace.user_id
        notification_id = namespace.notification_id
        self.acknowledge_deletion(self.serv.delete_notification(user_id, notification_id))

    def handle_delete_task_group_relation_command(self, namespace):
        user_id = namespace.user_id
        task_id = namespace.task_id
        group_id = namespace.group_id
        self.acknowledge_deletion(self.serv.delete_task_group_relation(user_id, task_id, group_id))

    def handle_delete_user_executor_relation_command(self, namespace):
        user_id = namespace.user_id
        task_id = namespace.task_id
        executor_id = namespace.executor_id
        self.acknowledge_deletion(self.serv.delete_user_executor_relation(user_id, task_id, executor_id))

    def handle_delete_message_command(self, namespace):
        user_id = namespace.user_id
        message_id = namespace.message_id
        self.acknowledge_deletion(self.serv.delete_message(user_id, message_id))
    # endregion

    # region Update commands
    def handle_update_task_command(self, namespace):
        args = attrs_transf.get_attributes(arguments_to_attributes.TASK_ATTRIBUTES_TRANSLATIONS,
                                           attrs_transf.TASK_ATTRIBUTES_TRANSFORMER, vars(namespace), filter_none=True)

        dates_wasnt_updated = namespace.date is None and namespace.date_end is None and namespace.date_start is None
        if not args and dates_wasnt_updated:
            print("Nothing to update")
            return

        task_id = namespace.task_id
        user_id = namespace.user_id

        if args:
            task = self.serv.update_task(user_id, task_id, **args)
            printers.show_task(task, header="------ Task was updated:")

        # Updating dates
        date_end = "date_end"
        date_start = "date_start"
        date = "date"

        dates = {
            date: namespace.date,
            date_start: namespace.date_start,
            date_end: namespace.date_end
        }
        incompatible_date_selection = (namespace.date and (namespace.date_end or namespace.date_start))
        if incompatible_date_selection:
            print("You have specified incompatible dates, they weren't updated")
        elif any(dates.values()):
            task_dates = self.serv.get_task_dates(user_id, task_id)
            if task_dates:
                if len(task_dates) == 1:
                    task_date = task_dates[0]
                    if task_date.date_type == me.DateType.EVENT.value and dates[date] is not None:
                        task_date = self.serv.update_date(user_id, task_date.date_id, date=dates[date])
                    elif task_date.date_type == me.DateType.START.value and dates[date_start] is not None:
                        task_date = self.serv.update_date(user_id, task_date.date_id,date=dates[date_start])
                    elif task_date.date_type == me.DateType.END.value and dates[date_end] is not None:
                        task_date = self.serv.update_date(user_id, task_date.date_id, date=dates[date_end])
                    else:
                        print("You are trying to update date which task doesnt have")
                        return
                    printers.show_date(task_date, header="------ Date was updated: ")
                else:
                    start_date = task_dates[0] if task_dates[0].date_type == me.DateType.START.value else task_dates[1]
                    end_date = task_dates[1] if task_dates[1].date_type == me.DateType.END.value else task_dates[0]

                    if dates[date_start]:
                        start_date = self.serv.update_date(user_id, start_date.date_id, date=dates[date_start])
                        printers.show_date(start_date, header="------ Start date was updated: ")
                    if dates[date_end]:
                        end_date = self.serv.update_date(user_id, end_date.date_id, date=dates[date_end])
                        printers.show_date(end_date, header="------ End date was updated")

            else:
                print("Task doesn't have dates")

    def handle_update_task_status_command(self, namespace):
        user_id = namespace.user_id
        task_id = namespace.task_id
        args = attrs_transf.get_attributes(arguments_to_attributes.TASK_ATTRIBUTES_TRANSLATIONS,
                                           attrs_transf.TASK_ATTRIBUTES_TRANSFORMER, vars(namespace), filter_none=True)
        if not args:
            print("Nothing to update")
            return

        task = self.serv.update_task_status(user_id, task_id, status=args[mo.TASK_ATTR_STATUS])
        printers.show_task(task, header="------ Task was updated: ")

    def handle_execute_task_command(self, namespace):
        user_id = namespace.user_id
        task_id = namespace.task_id
        task = self.serv.update_task_status(user_id, task_id, status=me.TaskStatus.EXECUTED.value)
        printers.show_task(task, header="------ Task was executed:")

    def handle_close_task_command(self, namespace):
        user_id = namespace.user_id
        task_id = namespace.task_id
        task = self.serv.update_task_status(user_id, task_id, status=me.TaskStatus.CLOSED.value)
        printers.show_task(task, header="------ Task was closed:")

    def handle_update_plan_command(self, namespace):
        user_id = namespace.user_id
        plan_id = namespace.plan_id
        plan_args = attrs_transf.get_attributes(arguments_to_attributes.PLAN_ATTRIBUTES_TRANSLATIONS,
                                                attrs_transf.PLAN_ATTRIBUTES_TRANSFORMER,
                                                vars(namespace), filter_none=True)

        if not plan_args:
            print("Nothing to update")
            return

        plan = self.serv.update_plan(user_id, plan_id, **plan_args)
        printers.show_plan(plan, header="----- Plan was updated: ")

    def handle_update_relation_condition_command(self, namespace):
        # Getting condition relation object and saving it
        translation = arguments_to_attributes.CONDTION_TASK_RELATIONS_ATTRIBUTES_TRANSLATION
        condition_args = attrs_transf.get_attributes(translation,
                                                     attrs_transf.CONDITION_TASK_RELATIONS_ATTRIBUTES_TRANSFORMER,
                                                     vars(namespace))
        if not condition_args:
            print("Nothing to update")
            return

        user_id = namespace.user_id
        relation_id = namespace.relation_id
        condition = self.serv.update_condition_relation(user_id, relation_id, **condition_args)
        printers.show_condition_relation(condition, header="------ Condition Relation was updated:")

    def handle_update_subtask_relation_command(self, namespace):
        user_id = namespace.user_id
        args = attrs_transf.get_attributes(arguments_to_attributes.SUBTASK_RELATION_ATTRIBUTES_TRANSLATIONS,
                                           attrs_transf.SUBTASK_RELATION_ATTRIBUTES_TRANSFORMER, vars(namespace),
                                           filter_none=True)
        if not args:
            print("Nothing to update")
            return

        relation_id = namespace.relation_id
        relation = self.serv.update_subtask_relation(user_id, relation_id, **args)
        printers.show_subtask_relation(relation, header="------ Condtion Relation was updated:")

    def handle_update_date_command(self, namespace):
        args = attrs_transf.get_attributes(arguments_to_attributes.TASK_DATE_ATTRIBUTES_TRANSLATIONS,
                                           attrs_transf.TASK_DATE_ATTRIBUTES_TRANSFORMER, vars(namespace),
                                           filter_none=True)
        if not args:
            print("Nothing to update")
            return

        date_id = namespace.date_id
        user_id = namespace.user_id
        date = self.serv.update_date(user_id, date_id, **args)
        printers.show_date(date, header="------ Date was updated:")

    def handle_update_group_command(self, namespace):
        args = attrs_transf.get_attributes(arguments_to_attributes.GROUP_ATTRIBUTES_TRANSLATIONS,
                                           attrs_transf.GROUP_ATTRIBUTES_TRANSFORMER, vars(namespace), filter_none=True)
        if not args:
            print("Nothing to update")
            return

        group_id = namespace.group_id
        user_id = namespace.user_id
        group = self.serv.update_group(user_id, group_id, **args)
        printers.show_group(group, header="------ Group was updated:")

    def handle_update_reminder_command(self, namespace):
        args = attrs_transf.get_attributes(arguments_to_attributes.REMINDER_ATTRIBUTES_TRANSLATIONS,
                                           attrs_transf.REMINDER_ATTRIBUTES_TRANSFORMER, vars(namespace),
                                           filter_none=True)
        if not args:
            print("Nothing to update")
            return

        user_id = namespace.user_id
        reminder_id = namespace.reminder_id
        reminder = self.serv.update_reminder(user_id, reminder_id, **args)
        printers.show_reminder(reminder, header="------ Reminder was updated:")

    def handle_update_notification_command(self, namespace):
        args = attrs_transf.get_attributes(arguments_to_attributes.NOTIFICATION_ATTRIBUTES_TRANSLATIONS,
                                           attrs_transf.NOTIFICATION_ATTRIBUTES_TRANSFORMER, vars(namespace),
                                           filter_none=True)
        if not args:
            print("Nothing to update")
            return

        user_id = namespace.user_id
        notification_id = namespace.notification_id
        notification = self.serv.update_notification(user_id, notification_id, **args)
        printers.show_notification(notification, header="------ Notification was updated: ")

    @staticmethod
    def acknowledge_deletion(deletion_result):
        if deletion_result:
            print("Deleted successfully")
        else:
            print("Object wasn't deleted")

    # endregion
