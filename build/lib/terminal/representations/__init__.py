"""
    Contains modules for object representation

    Modules:
        arguments_to_attributes - mappings from terminal args names to models attributes names
        arguments_transformers - mappings from terminal args names to functions of how they are handled
        arguments_to_enums - mappings from enums representations strings to their values
        enums_transformers -  mappings from enums values to their string representations
        printers - provides functions for printing models

"""