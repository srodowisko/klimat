"""
    This package provides models classes for
    planner4u library

    Modules:
        models.py - contains bare data models and attributes constants
        enums.py - contains enums for models and enums representations
        view_models - contains enriched data modles for representation

"""
