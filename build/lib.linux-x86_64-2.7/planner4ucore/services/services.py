"""
    This module provides Service class with all
    necessary functions that can be used to access
    data layer

"""
from datetime import datetime
from dateutil.relativedelta import relativedelta
from functools import wraps

from planner4ucore.models import enums as me
from planner4ucore.models import view_models as vmo
from planner4ucore.models import models as mo

from planner4ucore.services.constraints import DuplicationConstraint, AuthorizationConstraint, LogicConstraint
from planner4ucore.services.log_functions import write_enter_function_log
from planner4ucore.services.log_functions import write_exception_log
from planner4ucore.services.exceptions import ObjectIsNotFoundError

from planner4ucore.storage.sqlalchemy_storage import session_scope


def user_id_injector(func):
    @wraps(func)
    def wrapper(service, *args, **kwargs):
        if mo.USER_ATTR_ID in kwargs:
            if isinstance(kwargs[mo.USER_ATTR_ID], str):
                user = service.get_user_by_username(kwargs[mo.USER_ATTR_ID])
                if user is None:
                    raise ObjectIsNotFoundError(kwargs[mo.USER_ATTR_ID], "user")
                kwargs[mo.USER_ATTR_ID] = user.user_id
        return func(service, *args, **kwargs)
    return wrapper


class Service:
    """
        This class includes add, get, update, delete
        and system functions to work with data

        Args:
            data_storage: object implementing data-storage interface

        Attributes:
            data_storage: object implementing data-storage interface
            logic (LogicConstraint): object responsible for checking logic constraints
            auth (AuthorizationConstraint): object responsible for checking user permissions
            dupl (DuplicationConstraint): object responsible for checking entities duplication

        Functions:
            add_task - adds new task to storage
            add_plan - adds new plan to storage
            add_group - adds new group to storage
            add_date - adds new date to storage
            add_reminder - add new reminder to storage
            add_condition_relation - add new condition relation to storage
            add_notification - add new notification to storage
            add_task_group_relation - add new task group relation to storage
            add_user_executor_relation - add new user executor relation to storage
            add_subtask_relation - add new subtask relation to storage

            update_task - updates specified task attributes
            update_task_status - updates task status
            update_condition_relation - updates relation
            update_date - updates specified date attributes
            update_group - updates specified group attributes
            update_notification - updates specified notification attributes
            update_reminder - updates specified reminder attributes
            update_user_executor_relation - updates specified user executor relation
            update_subtask_relation - updates subtask realtion

            get_task - returns required task
            get_plan - returns required paln
            get_user_tasks - returns all tasks where user is creator
            get_user_executable_tasks - returns all tasks where user is executor
            get_condition_task_relation - returns condition relation by its id
            get_condition_task_relation_by_defined - returns relation by defined task id
            get_date - returns date by id
            get_group - returns group by id
            get_reminder - returns reminder by id
            get_notification - returns notification by id
            get_user_executor_relation - returns relation by id
            get_subtask_relation - returns subtask relation by id
            get_message - returns message by id

            delete_task - delete entity specifying its id
            delete_condition_relation - - delete entity specifying its id
            delete_date - delete entity specifying its id
            delete_group - delete entity specifying its id
            delete_reminder - delete entity specifying its id
            delete_notification - delete entity specifying its id
            delete_task_group_relation - delete entity specifying its id
            delete_user_executor_relation -- delete entity specifying its id
            delete_subtask_relation - delete entity specifying its id

            get_task_view - returns task model for representations
            get_group_view - returns group model for representations
            get_plan_view - returns plan model for representations
            get_date_view - returns date model for representations
            get_notification_view - returns notification model for representations
            get_reminder_view - returns reminder model for representations
            get_tasks_view - returns tasks model for representations
            get_groups_view - returns groups model for representations
            get_notifications_view - returns notifications model for representations
            get_reminders_view - returns reminders model for representations
            get_subtasks_view - returns subtasks model for representations
            get_messages_view - returns messages model for representations

            handle_execute_task_event - implements algorithm of how task should be executed
            notify_users_on_tasks_status_changed - sends all connected users messages about task status
            notfiy_user_of_task - sends all connected users specified messages
            execute_sub_tasks - tries to executed subtasks of given task
            execute_parent_tasks - tries to execute parent tasks of given task
            update_condition tasks - creates condition tasks if status of given is valid

            update_system_state - invokes another functions listed below
            update_ripe_notifications - updates "up to date" notifications
            update_ripe_reminders - updates "up to date" reminders
            update_ripe_plans - updates "up to date" plans
            update_overdue_tasks - update tasks which date was overdue

    """
    def __init__(self, data_storage):
        self.ds = data_storage

        self.logic = LogicConstraint(data_storage)
        self.auth = AuthorizationConstraint(data_storage)
        self.dupl = DuplicationConstraint(data_storage)

    # region Add services
    @write_exception_log
    @write_enter_function_log()
    def add_task(self, task):
        """ Adds task to data storage

            Args:
                task (models.Task): task which will be added

            Returns:
                Added task (models.Task)

        """
        return self.ds.add_task(task)

    @write_exception_log
    @write_enter_function_log()
    def add_plan(self, template_task, plan):
        """ Adds plan to data storage

            Args:
                template_task (models.Task): template task for plan
                plan (models.Plan): plan which will be added

            Returns:
                Added task, plan tuple

        """
        template_task = self.ds.add_task(template_task)
        plan.task_id = template_task.task_id

        try:
            self.logic.check_add_plan_restriction(plan)
            plan = self.ds.add_plan(plan)
        except Exception:
            self.ds.delete_task(template_task.task_id)
            raise

        return template_task, plan

    @write_exception_log
    @write_enter_function_log()
    def add_group(self, group):
        """ Adds group to data storage

            Args:
                 (models.Group): group which will be added

            Returns:
                Added group with set id(models.Group)

        """
        return self.ds.add_group(group)

    @user_id_injector
    @write_exception_log
    @write_enter_function_log()
    def add_date(self, user_id, date):
        """ Adds date to data storage

            Args:
                user_id (int): id of user how is performing operation
                date (models.date):  which will be added

            Returns:
                Added group with set id(models.Group)

        """
        self.auth.authorize_for_task(user_id, date.task_id)

        # Check logical restrictions
        self.logic.check_plan_restriction(date.task_id)
        self.logic.check_condition_restriction(date.task_id)
        self.logic.check_add_date_restriction(date)
        return self.ds.add_date(date)

    @user_id_injector
    @write_exception_log
    @write_enter_function_log()
    def add_reminder(self, user_id, reminder):
        """ Adds reminder to data storage

            Args:
                user_id (int): id of user how is performing operation
                reminder (models.Reminder): instance of reminder which will be added

            Returns:
                Added reminder with set id(models.Reminder)

        """
        self.auth.authorize_for_task(user_id, reminder.task_id)
        self.logic.check_plan_restriction(reminder.task_id)
        self.logic.check_condition_restriction(reminder.task_id)

        return self.ds.add_reminder(reminder)

    @user_id_injector
    @write_exception_log
    @write_enter_function_log()
    def add_condition_relation(self, user_id, condition_relation):
        """ Adds condition relation to data storage

            Args:
                user_id (int): id of user how is performing operation
                condition_relation (models.ConditionRelation): instance of  which will be added

            Returns:
                Added condition relation with set id(models.ConditionRelation)

        """
        self.auth.authorize_for_task(user_id, condition_relation.condition_task_id)
        try:
            self.logic.check_plan_restriction(condition_relation.condition_task_id)
        except Exception:
            defined_task_id = condition_relation.defined_task_id
            self.ds.delete_task(defined_task_id)
            raise
        return self.ds.add_condition_task_relation(condition_relation)

    @user_id_injector
    @write_exception_log
    @write_enter_function_log()
    def add_notification(self, user_id, notification):
        """ Adds notification to data storage

            Args:
                user_id (int): id of user how is performing operation
                notification (models.Notification): instance of  which will be added

            Returns:
                Added notification with set id(models.Notification)

        """
        self.auth.authorize_for_date(user_id, notification.date_id)
        return self.ds.add_notification(notification)

    @user_id_injector
    @write_exception_log
    @write_enter_function_log()
    def add_task_group_relation(self, user_id, task_group_relation):
        """ Adds task group relation to data storage

            Args:
                user_id (int): id of user how is performing operation
                task_group_relation (models.TaskGroupRelation): instance of  which will be added

            Returns:
                Added task group relation with set id(models.TaskGroupRelation)

        """
        self.auth.authorize_for_task_group_relation(user_id, task_group_relation.group_id, task_group_relation.task_id)
        self.dupl.check_task_group_relation_duplication(task_group_relation)
        return self.ds.add_task_group_relation(task_group_relation)

    @user_id_injector
    @write_exception_log
    @write_enter_function_log()
    def add_user_executor_relation(self, user_id, user_executor_relation):
        """ Adds user executor relation to data storage

            Args:
                user_id (int): id of user how is performing operation
                user_executor_relation (models.UserExecutorRelation): instance of  which will be added

            Returns:
                Added user executor relation with set id(models.UserExecutorRelation)

        """
        self.auth.authorize_for_task(user_id, user_executor_relation.task_id, executor=False)
        self.dupl.check_user_executor_duplication(user_executor_relation)
        self.logic.check_plan_restriction(user_executor_relation.task_id)
        return self.ds.add_user_executor_relation(user_executor_relation)

    @user_id_injector
    @write_exception_log
    @write_enter_function_log()
    def add_subtask_relation(self, user_id, subtask_relation):
        """ Adds subtask relation to data storage

            Args:
                user_id (int): id of user how is performing operation
                subtask_relation (models.SubtaskRelation): instance of  which will be added

            Returns:
                Added subtask relation with set id(models.SubtaskRelation)

        """
        self.auth.authorize_for_subtask_relation(user_id, subtask_relation.parent_task_id, subtask_relation.sub_task_id,
                                                 executor=False)

        # Check plan, condition restrictions for both tasks
        self.logic.check_plan_restriction(subtask_relation.sub_task_id)
        self.logic.check_plan_restriction(subtask_relation.parent_task_id)
        self.logic.check_condition_restriction(subtask_relation.parent_task_id)

        # Check duplication case
        self.dupl.check_subtask_relation_duplication(subtask_relation)
        # Check loop dependency
        self.logic.check_subtask_loop_dependency(subtask_relation.sub_task_id, subtask_relation.parent_task_id)

        return self.ds.add_subtask_relation(subtask_relation)

    def add_user(self, user):
        return self.ds.add_user(user)
    # endregion

    # region Update services
    @user_id_injector
    @write_exception_log
    @write_enter_function_log()
    def update_task(self, user_id, task_id, title=None, status=None, task_type=None, priority=None, priority_bdg=None,
                    description=None, last_subtask_dependency=None, priority_on_overdue=None):
        """ Updates specified task

            Args:
                user_id (int): id of user how is performing operation
                task_id (int): id of entity in storage which will be updated
                title (str) = None: title of the task
                status (int) = None: status of the task
                task_type (int) = None: task type
                priority (int) = None: priority of the task
                description (str) = None: description of the task
                last_subtask_dependency (int) = None: whether task dependent on the last subtask
                priority_on_overdue (int) = None: priority which will be set to the task if it is overdue

            Returns:
                Updated task object (models.Task)

        """
        self.auth.authorize_for_task(user_id, task_id)

        args = {}
        if title is not None:
            args[mo.TASK_ATTR_TITLE] = title
        if status is not None:
            self.update_task_status(user_id, task_id, status=status)
            args[mo.TASK_ATTR_STATUS] = status
        if task_type is not None:
            args[mo.TASK_ATTR_TYPE] = task_type
        if priority is not None:
            args[mo.TASK_ATTR_PRIORITY] = priority
        if priority_bdg is not None:
            args[mo.TASK_ATTR_PRIORITY_BDG] = priority_bdg
        if description is not None:
            args[mo.TASK_ATTR_DESCRIPTION] = description
        if last_subtask_dependency is not None:
            args[mo.TASK_ATTR_DEPENDENT] = last_subtask_dependency
        if priority_on_overdue is not None:
            args[mo.TASK_ATTR_OVERDUE_PRIORITY] = priority_on_overdue

        return self.ds.update_task(task_id, args)

    @user_id_injector
    @write_exception_log
    @write_enter_function_log()
    def update_plan(self, user_id, plan_id, create_task_date=None, repeat_type=None, plan_interval=None,
                    end_type=None, end_date=None, repetitions_number=None, active=None):

        self.auth.authorize_for_plan(user_id, plan_id)
        args = {}
        if create_task_date is not None:
            args[mo.PLAN_ATTR_DATE] = create_task_date
        if repeat_type is not None:
            args[mo.PLAN_ATTR_TYPE] = repeat_type
        if plan_interval is not None:
            args[mo.PLAN_ATTR_INTERVAL] = plan_interval
        if end_type is not None:
            args[mo.PLAN_ATTR_END_TYPE] = end_type
        if end_date is not None:
            args[mo.PLAN_ATTR_END_DATE] = end_date
        if repetitions_number is not None:
            args[mo.PLAN_ATTR_REPETITIONS_NUMBER] = repetitions_number
        if active is not None:
            args[mo.PLAN_ATTR_ACTIVE] = active

        self.logic.check_update_plan_restriction(args)
        return self.ds.update_plan(plan_id, args)

    @user_id_injector
    @write_exception_log
    @write_enter_function_log()
    def update_task_status(self, user_id, task_id, status):
        """ Updates status of specified task

            Args:
                user_id (int): id of user how is performing operation
                task_id (int): id of entity in storage which will be updated
                status(int): new status of the task

            Returns:
                Updated task object (models.Task)

        """
        self.auth.authorize_for_task(user_id, task_id)

        self.logic.check_condition_restriction(task_id)
        self.logic.check_plan_restriction(task_id)

        task = self.ds.get_task(task_id)
        # Apply special algorithm on task executed status
        if status == me.TaskStatus.EXECUTED.value:
            self.handle_execute_task_event(task)
        else:
            task = self.ds.update_task(task_id, {mo.TASK_ATTR_STATUS: status})
            self.notify_users_on_task_status_changed(task)
            self.update_condition_tasks()

        return self.ds.get_task(task_id)

    @user_id_injector
    @write_exception_log
    @write_enter_function_log()
    def update_condition_relation(self, user_id, relation_id, condition_task_status):
        """ Updates specified condition relation

            Args:
                user_id (int): id of user how is performing operation
                relation_id (int): id of entity in storage which will be updated
                condition_task_status(int): status of trigger task
            Returns:
                Updated relation object (models.ConditionRelation)

        """
        args = {mo.CONDITION_TASK_RELATION_ATTR_STATUS: condition_task_status}
        self.auth.authorize_for_condition_relation(user_id, relation_id)
        return self.ds.update_condition_task_relation(relation_id, args)

    @user_id_injector
    @write_exception_log
    @write_enter_function_log()
    def update_date(self, user_id, date_id, date_type=None, date=None):
        """ Updates specified date

            Args:
                user_id (int): id of user how is performing operation
                date_id (int): id of entity in storage which will be updated
                date_type (int): type of date
                date (datetime.date): date

            Returns:
                Updated date object (models.Date)

        """
        self.auth.authorize_for_date(user_id, date_id)

        args = {}
        if date_type is not None:
            args[mo.DATE_ATTR_TYPE] = date_type
        if date is not None:
            args[mo.DATE_ATTR_DATE] = date

        if mo.DATE_ATTR_DATE in args:
            self.logic.check_update_date_restriction(date_id, args[mo.DATE_ATTR_DATE])
        return self.ds.update_date(date_id, args)

    @user_id_injector
    @write_exception_log
    @write_enter_function_log()
    def update_group(self, user_id, group_id, title):
        """ Updates specified group

            Args:
                user_id (int): id of user how is performing operation
                group_id (int): id of entity in storage which will be updated
                title (str): title of the group

            Returns:
                Updated group object (models.Group)

        """
        self.auth.authorize_for_group(user_id, group_id)
        args = {mo.GROUP_ATTR_TITLE: title}
        return self.ds.update_group(group_id, args)

    @user_id_injector
    @write_exception_log
    @write_enter_function_log()
    def update_reminder(self, user_id, reminder_id, raise_date=None, raise_time=None, days_interval=None,
                        active=None):
        """ Updates specified reminder

            Args:
                user_id (int): id of user who is performing operation
                reminder_id (int): id of entity in storage which will be updated
                raise_date (datetime.date): date when reminder will be raised
                raise_time (datetime.time): time when reminder will be raised
                days_interval (int): interval of days when reminder will be raised
                active (int): whether reminder acitve or not

            Returns:
                Updated reminder object (models.Reminder)

        """
        self.auth.authorize_for_reminder(user_id, reminder_id)

        args = {}
        if raise_date is not None:
            args[mo.REMINDER_ATTR_DATE] = raise_date
        if raise_time is not None:
            args[mo.REMINDER_ATTR_TIME] = raise_time
        if days_interval is not None:
            args[mo.REMINDER_ATTR_INTERVAL] = days_interval
        if active is not None:
            args[mo.REMINDER_ATTR_ACTIVE] = active

        return self.ds.update_reminder(reminder_id, args)

    @user_id_injector
    @write_exception_log
    @write_enter_function_log()
    def update_notification(self, user_id, notification_id, days_to_date=None, time=None, used=None):
        """ Updates specified notification

            Args:
                user_id (int): id of user how is performing operation
                notification_id (int): id of entity in storage which will be updated
                days_to_date (int): days to date of notification
                time (datetime.time): time when notification will be raised
                used(int): whether notifiction was used

            Returns:
                Updated notification object (models.Notification)

        """
        self.auth.authorize_for_notification(user_id, notification_id)

        args = {}
        if days_to_date is not None:
            args[mo.NOTIFICATION_ATTR_DAYS] = days_to_date
        if time is not None:
            args[mo.NOTIFICATION_ATTR_TIME] = time
        if used is not None:
            args[mo.NOTIFICATION_ATTR_USED] = used

        return self.ds.update_notification(notification_id, args)

    @user_id_injector
    @write_exception_log
    @write_enter_function_log()
    def update_user_executor_relation(self, user_id, relation_id, args):
        """ Updates specified user executor relation

            Args:
                user_id (int): id of user how is performing operation
                relation_id (int): id of entity in storage which will be updated
                args (dict): key-value pairs to update

            Returns:
                Updated relation object (models.UserExecutorRelation)

        """
        self.auth.authorize_for_user_executor_relation(user_id, relation_id)
        return self.ds.update_user_executor_relation(relation_id, args)

    @user_id_injector
    @write_exception_log
    @write_enter_function_log()
    def update_subtask_relation(self, user_id, relation_id, relation_type):
        """ Updates specified subtask relation

            Args:
                user_id (int): id of user how is performing operation
                relation_id (int): id of entity in storage which will be updated
                relation_type (int): type of relation between task and subtask

            Returns:
                Updated relation object (models.SubtaskRelation)

        """
        relation = self.ds.get_subtask_relation_by_id(relation_id)
        self.auth.authorize_for_subtask_relation(user_id, relation.parent_task_id, relation.sub_task_id)
        args = {mo.SUBTASK_RELATION_ATTR_TYPE: relation_type}
        return self.ds.update_subtask_relation(relation_id, args)

    # endregion

    # region Get services
    @user_id_injector
    @write_exception_log
    @write_enter_function_log()
    def get_task(self, user_id, task_id):
        """ Returns required task

            Args:
                user_id (int): id of user who is performing operation
                task_id (int): id of entity in storage to return

            Returns:
                Required (models.)

        """
        self.auth.authorize_for_task(user_id, task_id)
        return self.ds.get_task(task_id)

    @user_id_injector
    @write_exception_log
    @write_enter_function_log()
    def get_plan(self, user_id, plan_id):
        """ Returns required plan

            Args:
                user_id (int): id of user who is performing operation
                plan_id (int): id of entity in storage to return

            Returns:
                Required plan (models.Plan)

        """
        self.auth.authorize_for_plan(user_id, plan_id)
        return self.ds.get_plan(plan_id)

    @user_id_injector
    @write_exception_log
    @write_enter_function_log()
    def get_user_tasks(self, user_id):
        """ Returns user tasks where he is creator

            Args:
                user_id (int): id of user who is performing operation

            Returns:
               List of tasks (models.Task)

        """
        return self.ds.get_user_tasks(user_id)

    @user_id_injector
    @write_exception_log
    @write_enter_function_log()
    def get_task_dates(self, user_id, task_id):
        self.auth.authorize_for_task(user_id, task_id)
        return self.ds.get_task_dates(task_id)

    @user_id_injector
    @write_exception_log
    @write_enter_function_log()
    def get_task_datess(self, user_id=None, task_id=None):
        self.auth.authorize_for_task(user_id, task_id)
        return self.ds.get_task_dates(task_id)

    @user_id_injector
    @write_exception_log
    @write_enter_function_log()
    def get_user_messages(self, user_id):
        return self.ds.get_user_task_messages(user_id)

    @user_id_injector
    @write_exception_log
    @write_enter_function_log()
    def get_user_executable_tasks(self, user_id):
        """ Returns tasks where user is executor

            Args:
                user_id (int): id of user who is performing operation

            Returns:
                List of tasks (list(models.Task))

        """
        return self.ds.get_user_executable_tasks(user_id)

    @user_id_injector
    @write_exception_log
    @write_enter_function_log()
    def get_condition_task_relation(self, user_id, relation_id):
        """ Returns required condition task relation

            Args:
                user_id (int): id of user who is performing operation
                relation_id (int): id of entity in storage to return

            Returns:
                Required condition task relation (models.ConditionTaskRelation)

        """
        self.auth.authorize_for_condition_relation(user_id, relation_id)
        return self.ds.get_condition_task_relation(relation_id)

    @user_id_injector
    @write_exception_log
    @write_enter_function_log()
    def get_condition_task_relation_by_defined(self, user_id, defined_task_id):
        """ Returns required condition by defined task id

            Args:
                user_id (int): id of user who is performing operation
                defined_task_id (int): id of defined task for this relation

            Returns:
                Required task condition (models.ConditionTaskRelation)

        """
        relation = self.ds.get_condition_task_relation_by_defined(defined_task_id)
        self.auth.authorize_for_condition_relation(user_id, relation.relation_id)
        return relation

    @user_id_injector
    @write_exception_log
    @write_enter_function_log()
    def get_date(self, user_id, date_id):
        """ Returns required date

            Args:
                user_id (int): id of user who is performing operation
                date_id (int): id of entity in storage to return

            Returns:
                Required date (models.Date)

        """
        self.auth.authorize_for_date(user_id, date_id)
        return self.ds.get_date(date_id)

    @user_id_injector
    @write_exception_log
    @write_enter_function_log()
    def get_group(self, user_id, group_id):
        """ Returns required group

            Args:
                user_id (int): id of user who is performing operation
                group_id (int): id of entity in storage to return

            Returns:
                Required group (models.Group)

        """
        self.auth.authorize_for_group(user_id, group_id)
        return self.ds.get_group(group_id)

    @user_id_injector
    @write_exception_log
    @write_enter_function_log()
    def get_reminder(self, user_id, reminder_id):
        """ Returns required reminder

            Args:
                user_id (int): id of user who is performing operation
                reminder_id (int): id of entity in storage to return

            Returns:
                Required reminder (models.Reminder)

        """
        self.auth.authorize_for_reminder(user_id, reminder_id)
        return self.ds.get_reminder(reminder_id)

    @user_id_injector
    @write_exception_log
    @write_enter_function_log()
    def get_notification(self, user_id, notification_id):
        """ Returns notification

            Args:
                user_id (int): id of user who is performing operation
                notification_id (int): id of entity in storage to return

            Returns:
                Required notification (models.Notification)

        """
        self.auth.authorize_for_notification(user_id, notification_id)
        return self.ds.get_notification(notification_id)

    @user_id_injector
    @write_exception_log
    @write_enter_function_log()
    def get_user_executor_relation(self, user_id, relation_id):
        """ Returns required user executor relation

            Args:
                user_id (int): id of user who is performing operation
                relation_id (int): id of entity in storage to return

            Returns:
                Required relation (models.UserExecutorRelation)

        """
        self.auth.authorize_for_user_executor_relation(user_id, relation_id)
        self.ds.get_user_executor_relation(relation_id)

    @user_id_injector
    @write_exception_log
    @write_enter_function_log()
    def get_subtask_relation(self, user_id, relation_id):
        """ Returns required subtask relation

            Args:
                user_id (int): id of user who is performing operation
                relation_id (int): id of entity in storage to return

            Returns:
                Required subtask relation (models.SubtaskRelation)

        """
        self.auth.authorize_for_subtask_relation_by_id(user_id, relation_id)
        return self.ds.get_subtask_relation_by_id(relation_id)

    @user_id_injector
    @write_exception_log
    @write_enter_function_log()
    def get_message(self, user_id, message_id):
        """ Returns required message

            Args:
                user_id (int): id of user who is performing operation
                message_id (int): id of entity in storage to return

            Returns:
                Required message (models.Message)

        """
        self.auth.authorize_for_message(user_id, message_id)
        return self.ds.get_task_message(message_id)

    @user_id_injector
    @write_exception_log
    @write_enter_function_log()
    def get_users_executors(self, user_id, task_id):
        self.auth.authorize_for_task(user_id, task_id)
        return self.ds.get_users_executors(task_id)

    @write_exception_log
    @write_enter_function_log()
    def get_users(self):
        return self.ds.get_users()

    @user_id_injector
    @write_exception_log
    @write_enter_function_log()
    def get_subtasks(self, user_id, task_id):
        self.auth.authorize_for_task(user_id, task_id)
        return self.ds.get_sub_tasks(task_id)

    @user_id_injector
    @write_exception_log
    @write_enter_function_log()
    def get_parent_tasks(self, user_id, task_id):
        self.auth.authorize_for_task(user_id, task_id)
        return self.ds.get_parent_tasks(task_id)

    @write_exception_log
    @write_enter_function_log()
    def get_user_by_username(self, username):
        """ Returns user by its id"""
        return self.ds.get_user_by_username(username)

    @user_id_injector
    @write_exception_log
    @write_enter_function_log()
    def get_user(self, user_id):
        """ Returns user by name"""
        return self.ds.get_user(user_id)

    @write_exception_log
    @write_enter_function_log()
    def get_filtered_dates(self, date_id=None, task_id=None, date_type=None, user_creator_id=None, date=None, creation_date=None,
                           from_date=None, to_date=None, user_executor_id=None, title_pattern=None, start_from=None, status=None,
                           end_by=None):

        """ Returns filtered dates by specified parameters """

        filter_dict = {}
        if date_id:
            filter_dict[mo.DATE_ATTR_ID] = date_id
        if task_id:
            filter_dict[mo.DATE_ATTR_TASK_ID] = task_id
        if date_type:
            filter_dict[mo.DATE_ATTR_TYPE] = date_type
        if user_creator_id:
            if isinstance(user_creator_id, str):
                user_creator = self.ds.get_user_by_username(user_creator_id)
                if user_creator is None:
                    raise ObjectIsNotFoundError(user_creator_id, "user creator")
                user_creator_id = user_creator.user_id
            filter_dict[mo.TASK_ATTR_USER] = user_creator_id
        if user_executor_id:
            if isinstance(user_executor_id, str):
                user_executor = self.ds.get_user_by_username(user_executor_id)
                if user_executor is None:
                    raise ObjectIsNotFoundError(user_executor_id, "user creator")
                user_executor_id = user_executor.user_id
        if date:
            filter_dict[mo.DATE_ATTR_DATE] = date
        if status:
            filter_dict[mo.TASK_ATTR_STATUS] = status

        return self.ds.get_filtered_dates(filter_dict=filter_dict, from_date=from_date, to_date=to_date,
                                          date_type=date_type, user_executor_id=user_executor_id, start_from=start_from,
                                          end_by=end_by, title_pattern=title_pattern)

    @write_exception_log
    @write_enter_function_log()
    def get_filtered_tasks(self, task_id=None, task_type=None, user_creator_id=None, priority=None, priority_bdg=None, status=None,
                           creation_date=None, from_date=None, to_date=None, date_type=None, user_executor_id=None,
                           title_pattern=None, start_from=None, end_by=None):
        """ Returns filtered tasks by specified parameters """

        filter_dict = {}
        if task_id:
            filter_dict[mo.TASK_ATTR_ID] = task_id
        if task_type:
            filter_dict[mo.TASK_ATTR_TYPE] = task_type
        if date_type:
            filter_dict[mo.DATE_ATTR_TYPE] = date_type
        if user_creator_id:
            if isinstance(user_creator_id, str):
                user_creator = self.ds.get_user_by_username(user_creator_id)
                if user_creator is None:
                    raise ObjectIsNotFoundError(user_creator_id, "user creator")
                user_creator_id = user_creator.user_id
            filter_dict[mo.TASK_ATTR_USER] = user_creator_id
        if priority:
            filter_dict[mo.TASK_ATTR_PRIORITY] = priority
        if priority_bdg:
            filter_dict[mo.TASK_ATTR_PRIORITY_BDG] = priority_bdg
        if status:
            filter_dict[mo.TASK_ATTR_STATUS] = status
        if creation_date:
            filter_dict[mo.TASK_ATTR_CREATION_DATE] = creation_date

        if user_executor_id:
            if isinstance(user_executor_id, str):
                user_executor = self.ds.get_user_by_username(user_executor_id)
                if user_executor is None:
                    raise ObjectIsNotFoundError(user_executor_id, "user creator")
                user_executor_id = user_executor.user_id

        return self.ds.get_filtered_tasks(filter_dict=filter_dict, from_date=from_date, to_date=to_date,
                                          date_type=date_type, user_executor_id=user_executor_id, start_from=start_from,
                                          end_by=end_by, title_pattern=title_pattern)

    @write_exception_log
    @write_enter_function_log()
    def get_filtered_notifications(self, notification_id=None, date_id=None, days_to_date=None, time=None, used=None,
                                   task_id=None, user_id=None, from_date=None, to_date=None, date_type=None,
                                   start_from=None, end_by=None):
        """ Retruns filtered notifications"""
        filter_dict = {}
        if notification_id is not None:
            filter_dict[mo.NOTIFICATION_ATTR_ID] = notification_id
        if date_id is not None:
            filter_dict[mo.NOTIFICATION_ATTR_DATE_ID] = date_id
        if days_to_date is not None:
            filter_dict[mo.NOTIFICATION_ATTR_DAYS] = days_to_date
        if time is not None:
            filter_dict[mo.NOTIFICATION_ATTR_TIME] = time
        if used is not None:
            filter_dict[mo.NOTIFICATION_ATTR_USED] = used

        if isinstance(user_id, str):
            user = self.ds.get_user_by_username(user_id)
            if user is None:
                raise ObjectIsNotFoundError(user_id, "user")
            user_id = user.user_id

        return self.ds.get_filtered_notifications(filter_dict=filter_dict, task_id=task_id, user_id=user_id,
                                                  from_date=from_date, to_date=to_date, date_type=date_type,
                                                  start_from=start_from, end_by=end_by)

    @write_exception_log
    @write_enter_function_log()
    def get_filtered_reminders(self, reminder_id=None, task_id=None, raise_date=None, raise_time=None,
                               days_interval=None, active=None, user_id=None, from_raise_date=None, to_raise_date=None,
                               start_from=None, end_by=None):
        """ Returns filtered reminders """
        filter_dict = {}
        if reminder_id is not None:
            filter_dict[mo.REMINDER_ATTR_ID] = reminder_id
        if task_id is not None:
            filter_dict[mo.REMINDER_ATTR_TASK] = task_id
        if raise_date is not None:
            filter_dict[mo.REMINDER_ATTR_DATE] = raise_date
        if raise_time is not None:
            filter_dict[mo.REMINDER_ATTR_TIME] = raise_time
        if days_interval is not None:
            filter_dict[mo.REMINDER_ATTR_INTERVAL] = days_interval
        if active is not None:
            filter_dict[mo.REMINDER_ATTR_ACTIVE] = active

        if isinstance(user_id, str):
            user = self.ds.get_user_by_username(user_id)
            if user is None:
                raise ObjectIsNotFoundError(user_id, "user")
            user_id = user.user_id

        return self.ds.get_filtered_reminders(filter_dict=filter_dict, user_id=user_id, from_raise_date=from_raise_date,
                                              to_raise_date=to_raise_date, start_from=start_from, end_by=end_by)

    @write_exception_log
    @write_enter_function_log()
    def get_filtered_groups(self, group_id=None, user_creator_id=None, title=None, task_id=None,
                            start_from=None, end_by=None, title_pattern=None):
        """ Returns filtered groups """
        filter_dict = {}
        if group_id is not None:
            filter_dict[mo.GROUP_ATTR_ID] = group_id
        if user_creator_id is not None:
            if isinstance(user_creator_id, str):
                user_creator = self.ds.get_user_by_username(user_creator_id)
                if user_creator is None:
                    raise ObjectIsNotFoundError(user_creator_id, "user creator")
                user_creator_id = user_creator.user_id

            filter_dict[mo.GROUP_ATTR_CREATOR_ID] = user_creator_id
        if title:
            filter_dict[mo.GROUP_ATTR_TITLE] = title

        return self.ds.get_filtered_groups(filter_dict=filter_dict, task_id=task_id, start_from=start_from,
                                           end_by=end_by, title_pattern=title_pattern)

    @write_exception_log
    @write_enter_function_log()
    def get_filtered_plans(self, plan_id=None, task_id=None, create_task_date=None, repeat_type=None,
                           plan_interval=None, user_id=None, start_from=None, end_by=None):
        """ Returns filtered plans"""
        filter_dict = {}
        if plan_id is not None:
            filter_dict[mo.PLAN_ATTR_ID] = plan_id
        if task_id is not None:
            filter_dict[mo.PLAN_ATTR_TASK_ID] = task_id
        if create_task_date is not None:
            filter_dict[mo.PLAN_ATTR_DATE] = create_task_date
        if repeat_type is not None:
            filter_dict[mo.PLAN_ATTR_TYPE] = repeat_type
        if plan_interval is not None:
            filter_dict[mo.PLAN_ATTR_INTERVAL] = plan_interval

        if isinstance(user_id, str):
            user = self.ds.get_user_by_username(user_id)
            if user is None:
                raise ObjectIsNotFoundError(user_id, "user")
            user_id = user.user_id
        return self.ds.get_filtered_plans(filter_dict=filter_dict, user_id=user_id, start_from=start_from,
                                          end_by=end_by)

    @write_exception_log
    @write_enter_function_log()
    def get_filtered_users(self, username_pattern, start_from=None, end_by=None):
        """ Returns users matching pattern"""
        return self.ds.get_filtered_users(username_pattern=username_pattern,
                                          start_from=start_from, end_by=end_by)

    @write_exception_log
    @write_enter_function_log()
    def get_filtered_messages(self, user_id=None, seen=None, date=None,
                              title_pattern=None, start_from=None, end_by=None):
        filter_dict = {}
        if user_id is not None:
            if isinstance(user_id, str):
                user = self.ds.get_user_by_username(user_id)
                if user is None:
                    raise ObjectIsNotFoundError(user_id, "user")
                user_id = user.user_id
            filter_dict[mo.MESSAGE_ATTR_USER_ID] = user_id
        if seen is not None:
            filter_dict[mo.MESSAGE_ATTR_SEEN] = seen
        if date is not None:
            filter_dict[mo.MESSAGE_ATTR_DATE] = date

        return self.ds.get_filtered_messages(filter_dict=filter_dict, title_pattern=title_pattern,
                                             start_from=start_from, end_by=end_by)

    @user_id_injector
    @write_exception_log
    @write_enter_function_log()
    def get_group_tasks(self, user_id, group_id):
        """ Returns tasks of group"""
        self.auth.authorize_for_group(user_id, group_id)
        return self.ds.get_group_tasks(group_id)

    @user_id_injector
    @write_exception_log
    @write_enter_function_log()
    def get_plan_by_task(self, user_id, task_id):
        """ Returns plan connected with task"""
        self.auth.authorize_for_task(user_id, task_id)
        return self.get_filtered_plans(task_id=task_id)[0]

    @user_id_injector
    @write_exception_log
    @write_enter_function_log()
    def get_task_groups(self, user_id, task_id):
        """ Returns groups connected with task"""
        self.auth.authorize_for_task(user_id, task_id)
        return self.ds.get_filtered_groups(user_creator_id=user_id, task_id=task_id)

    @user_id_injector
    @write_exception_log
    @write_enter_function_log()
    def get_subtask_relations(self, user_id, task_id):
        """ Returns array of tuples: (relation, subtask)"""
        self.auth.authorize_for_task(user_id, task_id)
        relations = self.ds.get_task_subtask_relations(task_id)
        tasks = [self.ds.get_task(r.sub_task_id) for r in relations]
        return zip(relations, tasks)

    # endregion

    # region Delete services
    @user_id_injector
    @write_exception_log
    @write_enter_function_log()
    def delete_task(self, user_id, task_id):
        """ Delete specified task by its id

            Args:
                user_id (int): id of user who is performing operation
                task_id (int): id of entity in storage to delete

        """
        self.auth.authorize_for_task(user_id, task_id, executor=False)

        # If task is trigger for condition(s) - delete this condition(s)
        condition_relations = self.ds.get_task_condition_relations(task_id)
        for relation in condition_relations:
            self.ds.delete_task(relation.defined_task_id)

        return self.ds.delete_task(task_id)

    @user_id_injector
    @write_exception_log
    @write_enter_function_log()
    def delete_condition_relation(self, user_id, relation_id):
        """ Delete specified condition relation by its id

            Args:
                user_id (int): id of user who is performing operation
                relation_id (int): id of entity in storage to delete

        """
        self.auth.authorize_for_condition_relation(user_id, relation_id)
        with session_scope() as session:
            relation = self.ds.get_condition_task_relation(relation_id, session=session)
            return self.ds.delete_task(relation.defined_task_id, session=session)

    @user_id_injector
    @write_exception_log
    @write_enter_function_log()
    def delete_date(self, user_id, date_id):
        """ Delete specified date by its id

            Args:
                user_id (int): id of user who is performing operation
                date_id (int): id of entity in storage to delete

        """
        self.auth.authorize_for_date(user_id, date_id)
        return self.ds.delete_date(date_id)

    @user_id_injector
    @write_exception_log
    @write_enter_function_log()
    def delete_group(self, user_id, group_id):
        """ Delete specified group by its id

            Args:
                user_id (int): id of user who is performing operation
                group_id (int): id of entity in storage to delete

        """
        self.auth.authorize_for_group(user_id, group_id)
        return self.ds.delete_group(group_id)

    @user_id_injector
    @write_exception_log
    @write_enter_function_log()
    def delete_reminder(self, user_id, reminder_id):
        """ Delete specified reminder by its id

            Args:
                user_id (int): id of user who is performing operation
                reminder_id (int): id of entity in storage to delete

        """
        self.auth.authorize_for_reminder(user_id, reminder_id)
        return self.ds.delete_reminder(reminder_id)

    @user_id_injector
    @write_exception_log
    @write_enter_function_log()
    def delete_notification(self, user_id, notification_id):
        """ Delete specified notification by its id

            Args:
                user_id (int): id of user who is performing operation
                notification_id (int): id of entity in storage to delete

        """
        self.auth.authorize_for_notification(user_id, notification_id)
        return self.ds.delete_notification(notification_id)

    @user_id_injector
    @write_exception_log
    @write_enter_function_log()
    def delete_task_group_relation(self, user_id, task_id, group_id):
        """ Delete specified task group relation  by task and group id

            Args:
                user_id (int): id of user who is performing operation
                task_id(int): id of task which belongs to group
                group_id(int): id of group where task is registered

        """
        self.auth.authorize_for_group(user_id, group_id)
        return self.ds.delete_task_group_relation(task_id=task_id, group_id=group_id)

    @user_id_injector
    @write_exception_log
    @write_enter_function_log()
    def delete_user_executor_relation(self, user_id, task_id, executor_id):
        """ Delete specified user executor relation by user and task id

            Args:
                user_id (int): id of user who is performing operation
                executor_id (int): id of executor
                task_id (int): id of task

        """
        self.auth.authorize_for_task(user_id, task_id)
        task = self.ds.get_task(task_id)
        if task.user_creator != user_id:
            self.ds.delete_task_from_user_groups(user_id, task_id)
        return self.ds.delete_user_executor_relation(user_id=executor_id, task_id=task_id)

    @user_id_injector
    @write_exception_log
    @write_enter_function_log()
    def delete_subtask_relation(self, user_id, parent_task_id, sub_task_id):
        """ Delete specified relation by its parent task id and sub task id

            Args:
                user_id (int): id of user who is performing operation
                parent_task_id (int): id of parent task
                sub_task_id (int): id of sub task

        """
        self.auth.authorize_for_subtask_relation(user_id, parent_task_id, sub_task_id)
        return self.ds.delete_subtask_relation(parent_task_id, sub_task_id)

    @user_id_injector
    @write_exception_log
    @write_enter_function_log()
    def delete_user(self, user_id):
        return self.ds.delete_user(user_id)

    @user_id_injector
    @write_exception_log
    @write_enter_function_log()
    def delete_message(self, user_id, message_id):
        self.auth.authorize_for_message(user_id, message_id)
        return self.ds.delete_task_message(message_id)
    # endregion

    # region Get ModelViews
    @user_id_injector
    @write_exception_log
    @write_enter_function_log()
    def get_task_view(self, user_id, task_id):
        """ Return task model for representations """
        with session_scope() as session:
            task = self.get_task(user_id, task_id)

            # Filling view model with information
            executors = self.ds.get_users_executors(task_id, session=session)
            dates = self.ds.get_task_dates(task_id, session=session)
            dates = self.ds.get_task_datess(task_id, session=session)
            groups = self.ds.get_task_groups(user_id, task_id, session=session)
            reminders = self.ds.get_task_reminders(task_id, session=session)
            notifications = self.ds.get_task_notifications(task_id, session=session)
            creator = self.ds.get_user(task.user_creator, session=session)

            plan = self.ds.get_task_plan(task_id, session=session)
            condition = self.ds.get_condition_task_relation_by_defined(task_id, session=session)

            task_view = vmo.TaskView(task, creator, executors, dates, groups,
                                     notifications, reminders, plan, condition)
            return task_view

    @user_id_injector
    @write_exception_log
    @write_enter_function_log()
    def get_group_view(self, user_id, group_id):
        """ Returns group model for representations"""
        with session_scope() as session:
            group = self.get_group(user_id, group_id)
            tasks = self.ds.get_group_tasks(group_id, session=session)
            return vmo.GroupView(group, tasks)

    @user_id_injector
    @write_exception_log
    @write_enter_function_log()
    def get_plan_view(self, user_id, plan_id):
        """ Returns plan view for representations"""
        plan = self.get_plan(user_id, plan_id)
        task = self.ds.get_task(plan.task_id)
        return vmo.PlanView(plan, task)

    @user_id_injector
    @write_exception_log
    @write_enter_function_log()
    def get_date_view(self, user_id, date_id):
        """ Returns date for view representations"""
        date = self.get_date(user_id, date_id)
        task = self.ds.get_date_task(date_id)
        return vmo.DateView(date, task)

    @user_id_injector
    @write_exception_log
    @write_enter_function_log()
    def get_notification_view(self, user_id, notification_id):
        """ Returns notification model for representations"""
        notification = self.get_notification(user_id, notification_id)
        date = self.ds.get_date(notification.date_id)
        task = self.ds.get_task(date.task_id)
        return vmo.NotificationView(notification, date, task)

    @user_id_injector
    @write_exception_log
    @write_enter_function_log()
    def get_reminder_view(self, user_id, reminder_id):
        """ Returns reminder model for representations"""
        reminder = self.get_reminder(user_id, reminder_id)
        task = self.ds.get_task(reminder.task_id)
        return vmo.ReminderView(reminder, task)

    @user_id_injector
    @write_exception_log
    @write_enter_function_log()
    def get_tasks_view(self, user_id):
        """ Returns tasks model for representations"""
        tasks_creator = self.ds.get_user_tasks(user_id)
        tasks_executor = self.ds.get_user_executable_tasks(user_id)
        return vmo.TasksView(tasks_creator, tasks_executor)

    @user_id_injector
    @write_exception_log
    @ write_enter_function_log()
    def get_groups_view(self, user_id):
        """ Returns groups model for representations"""
        groups = self.ds.get_user_groups(user_id)
        return vmo.GroupsView(groups)

    @user_id_injector
    @write_exception_log
    @write_enter_function_log()
    def get_notifications_view(self, user_id):
        """ Returns notifications model for representations"""
        creator_notifications = self.ds.get_user_creator_notifications(user_id)
        executor_notifications = self.ds.get_user_executor_notifications(user_id)
        return vmo.NotificationsView(creator_notifications, executor_notifications)

    @user_id_injector
    @write_exception_log
    @write_enter_function_log()
    def get_reminders_view(self, user_id):
        """ Returns reminders view for representations"""
        creator_reminders = self.ds.get_user_creator_reminders(user_id)
        executor_reminders = self.ds.get_user_executor_reminders(user_id)
        return vmo.RemindersView(creator_reminders, executor_reminders)

    @user_id_injector
    @write_exception_log
    @write_enter_function_log()
    def get_subtasks_view(self, user_id, task_id):
        """ Returns subtasks model for representations """
        parent_task = self.get_task(user_id, task_id)
        sub_tasks = self.ds.get_sub_tasks(task_id)
        sub_task_view = vmo.SubtasksView(parent_task, "", "")

        # Function that generates tree of SubtasksView
        def form_subtasks_view(parent_subtask_view, task):
            # Get all subtasks of task
            tasks = self.ds.get_sub_tasks(task.task_id)
            # Get relation between parent and child
            reltaion = self.ds.get_subtask_relation(parent_subtask_view.task.task_id, task.task_id)
            # Create new SubtaskView
            child_subtask_view = vmo.SubtasksView(task, reltaion.relation_type, reltaion.relation_id)
            # Add it to parent
            parent_subtask_view.sub_task_views.append(child_subtask_view)
            for task in tasks:
                form_subtasks_view(child_subtask_view, task)

        # For form SbutasksView for each subtask
        for sub_task in sub_tasks:
            form_subtasks_view(sub_task_view, sub_task)

        return sub_task_view

    @user_id_injector
    @write_exception_log
    @write_enter_function_log()
    def get_messages_view(self, user_id):
        """ Returns all messages of user"""
        messages = self.ds.get_user_task_messages(user_id)
        return vmo.MessagesView(messages)
    # endregion

    # region Update task status functions
    @write_exception_log
    @write_enter_function_log()
    def handle_execute_task_event(self, task):
        """
            Invoked when attempt was made to change
            task status on executed

            Args:
                 task (models.Task): instance to update the state

        """
        # If satus of task was already execute, do nothing
        if task.status == me.TaskStatus.EXECUTED.value:
            return

        # Check all relations with subtasks and their statuses
        self.logic.check_task_can_be_executed(task)

        # Update task
        status_arg = {mo.TASK_ATTR_STATUS: me.TaskStatus.EXECUTED.value}
        task = self.ds.update_task(task.task_id, status_arg)

        # Try to execute parent tasks if its last task and parent
        # task has last subtask execution trigger
        self.execute_parent_tasks(task)
        # Try to execute subtasks with relation depends
        self.execute_sub_tasks(task)
        # Update condition task, on new satus
        self.update_condition_tasks()

        # notify all users that status of task was changed
        self.notify_users_on_task_status_changed(task)

    @write_exception_log
    @write_enter_function_log()
    def notify_users_on_task_status_changed(self, task):
        with session_scope() as session:
            user_creator = self.ds.get_user(task.user_creator, session=session)
            executors = self.ds.get_users_executors(task.task_id, session=session)
            executors.append(user_creator)

            for user in executors:
                message_text = ("\tHi, {name}, status of task \"{title}\" (id {id}) was changed on {status}"
                                .format(name=user.name, title=task.title, id=task.task_id,
                                        status=me.TaskStatus(task.status).name.lower()))
                message_title = "Status of task \"{title}\" was changed".format(title=task.title)
                message = mo.Message(user_id=user.user_id, message_text=message_text,
                                     message_title=message_title)
                self.ds.add_task_message(message, session=session)

    @write_exception_log
    @write_enter_function_log()
    def notify_users_of_task(self, task, message_title, message_text):
        with session_scope() as session:
            user_creator = self.ds.get_user(task.user_creator, session=session)
            executors = self.ds.get_users_executors(task.task_id, session=session)
            executors.append(user_creator)

            for user in executors:
                completed_message_text = ("\tHi, {name}, {text}".format(name=user.name, text=message_text))
                message = mo.Message(user_id=user.user_id, message_text=completed_message_text,
                                     message_title=message_title)
                self.ds.add_task_message(message, session=session)

    @write_exception_log
    @write_enter_function_log()
    def execute_sub_tasks(self, task):
        """ Executes all available subtasks of task"""
        with session_scope() as session:
            # Get all subtask relations
            relations = self.ds.get_task_subtask_relations(task.task_id, session=session)
            for relation in relations:
                # If relation type is "depends on", try to execute this subtask
                if relation.relation_type == me.TaskRelation.DEPENDS_ON.value:
                    self.handle_execute_task_event(relation.sub_task)

    @write_exception_log
    @write_enter_function_log()
    def execute_parent_tasks(self, task):
        """ Executes all avaliable parent tasks"""
        with session_scope() as session:
            # Get all parent tasks of task
            parent_tasks = self.ds.get_parent_tasks(task.task_id, session=session)
            for parent_task in parent_tasks:
                # If parent task is dependent on last child
                if parent_task.last_subtask_dependency == me.SubtaskDependency.DEPENDENT.value:
                    # Get all parent subtasks
                    parent_subtasks = self.ds.get_sub_tasks(parent_task.task_id, session=session)
                    # And if all of subtasks are executed, then try execute this task
                    if all([parent_subtask.status == me.TaskStatus.EXECUTED.value for parent_subtask in
                            parent_subtasks]):
                        self.handle_execute_task_event(parent_task)

    @write_exception_log
    @write_enter_function_log()
    def update_condition_tasks(self):
        """ Updates all available condition tasks"""
        with session_scope() as session:
            # Get condition relations
            relations = self.ds.get_condition_task_relations(session=session)
            for relation in relations:
                # If status in relation equals status of task, we need create condition task
                if relation.condition_task_status == relation.condition_task.status:
                    defined_task = relation.defined_task
                    # Just changing status and type of defined task
                    defined_task.task_type = me.TaskType.SIMPLE.value
                    defined_task.status = me.TaskStatus.CREATED.value
                    # Delete condition relation
                    self.ds.delete_condition_task_relation(relation.relation_id, session=session)
                    # And notify that task was changed
                    message_title = "Condition task \"{title}\" {id} was created!".format(title=defined_task.title,
                                                                                          id=defined_task.task_id)
                    message_text = ("condition task \"{condition_title}\" was created on {status} status of "
                                    "\"{trigger_title}\""
                                    .format(condition_title=defined_task.title,
                                            trigger_title=relation.condition_task.title,
                                            status=me.TaskStatus(relation.condition_task_status).name.lower()))

                    self.notify_users_of_task(defined_task, message_title, message_text)

    # endregion

    # region Update system state functions
    @write_exception_log
    @write_enter_function_log()
    def update_system_state(self):
        """ Performs:
                update_ripe_notifications
                update_ripe_reminders
                update_ripe_plans
                update_overdue_tasks

        """
        self.update_ripe_notifications()
        self.update_ripe_reminders()
        self.update_ripe_plans()
        self.update_overdue_tasks()
        self.update_condition_tasks()

    @write_exception_log
    @write_enter_function_log()
    def update_ripe_notifications(self):
        """
            Updates all "ready" notifications, sending
            users messages and deactivating them

        """
        with session_scope() as session:
            # Get all notifications which is "up to date"
            ripe_notifications = self.ds.get_ripe_notifications(session=session)
            for ripe_notification in ripe_notifications:
                # Get executors to notify
                users_to_notify = self.ds.get_users_executors_to_notify(ripe_notification.notification_id,
                                                                        session=session)

                # Add to them user-creator
                date = ripe_notification.date
                task = date.task
                users_to_notify.append(task.User)

                # For each user generate message
                for user in users_to_notify:
                    message_text = (("\tHi, {name}, don't forged that {days} or less days left until "
                                     "{date_type} date ({date}) of task \"{title}\" (id {id})!")
                                    .format(name=user.name, days=ripe_notification.days_to_date, date=date.date,
                                            date_type=me.DateType(date.date_type).name.lower(), title=task.title,
                                            id=task.task_id))

                    message_title = "Notification on task \"{title}\" ".format(title=task.title)
                    message = mo.Message(user_id=user.user_id, message_text=message_text,
                                         message_title=message_title)
                    self.ds.add_task_message(message, session=session)

                # Set notification to used
                ripe_notification.used = me.Used.YES.value

    @write_exception_log
    @write_enter_function_log()
    def update_ripe_reminders(self):
        """ Updates all reminders, sending users message"""
        with session_scope() as session:
            # Get all "up to date" reminders
            ripe_reminders = self.ds.get_ripe_reminders(session=session)
            for ripe_reminder in ripe_reminders:
                task = ripe_reminder.task
                user_creator = task.User
                users_to_remind = self.ds.get_users_executors_to_remind(ripe_reminder.reminder_id, session=session)
                users_to_remind.append(user_creator)

                # For each user generate letter
                for user in users_to_remind:
                    message_text = ("\tHi, {name}, don't forget about \"{title}\" task (id {id})!"
                                    .format(name=user.name, title=task.title, id=task.task_id))

                    message_title = "Reminder of \"{title}\" task ".format(title=task.title)
                    message = mo.Message(user_id=user.user_id, message_text=message_text,
                                         message_title=message_title)
                    self.ds.add_task_message(message, session=session)

                # Update next reminder date
                ripe_reminder.raise_date = ripe_reminder.raise_date + relativedelta(days=ripe_reminder.days_interval)

    @write_exception_log
    @write_enter_function_log()
    def update_ripe_plans(self):
        """ Update all plans with template task date up to now """
        with session_scope() as session:
            # Get all "up to date" plans
            ripe_plans = self.ds.get_ripe_plans(session=session)
            for ripe_plan in ripe_plans:

                # If plan should be closed by now, deactivate it
                if (ripe_plan.end_type == me.PlanCompletionCriteria.AFTER_DATE.value
                        and datetime.now().date() > ripe_plan.end_date):
                    ripe_plan.active = me.Active.NO.value
                    continue

                # Get template task
                template_task = self.ds.get_task(ripe_plan.task_id, session=session)
                # Make copy of template task
                new_task = mo.Task(task_type=me.TaskType.SIMPLE.value,
                                   user_creator=template_task.user_creator,
                                   creation_date=datetime.now().date(),
                                   title=template_task.title,
                                   description=template_task.description,
                                   priority=template_task.priority,
                                   priority_bdg=template_task.priority_bdg,
                                   status=me.TaskStatus.CREATED.value,
                                   last_subtask_dependency=template_task.last_subtask_dependency,
                                   priority_on_overdue=template_task.priority_on_overdue)

                # save new task
                new_task = self.ds.add_task(new_task)
                # notify users that task was created

                message_title = ("Template plan task \"{title}\" (id {id}) was created!"
                                 .format(id=new_task.task_id, title=new_task.title))
                message_text = "template task of plan {plan_id} was created!".format(plan_id=ripe_plan.plan_id)
                self.notify_users_of_task(new_task, message_title, message_text)

                ripe_plan.repetitions_count += 1

                # If plan was created definited amount of times, close it
                if (ripe_plan.end_type == me.PlanCompletionCriteria.AFTER_NUMBER_OF_TIMES.value
                        and ripe_plan.repetitions_count == ripe_plan.repetitions_number):
                    ripe_plan.active = me.Active.NO.value

                # Generate new date of template task creation
                time_deltas = {
                    me.RepetitionType.DAILY.value: relativedelta(days=ripe_plan.plan_interval),
                    me.RepetitionType.WEEKLY.value: relativedelta(weeks=ripe_plan.plan_interval),
                    me.RepetitionType.MONTHLY.value: relativedelta(months=ripe_plan.plan_interval),
                    me.RepetitionType.ANNUALLY.value: relativedelta(years=ripe_plan.plan_interval)
                }

                ripe_plan.create_task_date = ripe_plan.create_task_date + time_deltas[ripe_plan.repeat_type]

                # If next time task creation date is further than plan's end date, deactivate it
                if (ripe_plan.end_type == me.PlanCompletionCriteria.AFTER_DATE.value
                        and ripe_plan.create_task_date > ripe_plan.end_date):
                    ripe_plan.active = me.Active.NO.value

    @write_exception_log
    @write_enter_function_log()
    def update_overdue_tasks(self):
        """ Update status of all task which where overdue"""
        with session_scope() as session:
            overdue_dates = [date for date in self.ds.get_overdue_dates(session=session)
                             if date.date_type != me.DateType.START.value]
            for overdue_date in overdue_dates:
                # Get task
                date_task = overdue_date.task
                # And if status of task is suitable, update status
                if (date_task.status == me.TaskStatus.WORK.value or
                        date_task.status == me.TaskStatus.CREATED.value):
                    date_task.status = me.TaskStatus.OVERDUE.value
                    date_task.priority = date_task.priority_on_overdue
                    # Notify users
                    message_title = "Task \"{title}\" {id} was overdue!".format(title=date_task.title,
                                                                                id=date_task.task_id)
                    message_text = (" task \"{title}\" (id-{task_id}) was automatically marked as overdue after"
                                    " expired date ({date}, id-{date_id})"
                                    .format(title=date_task.title, task_id=date_task.task_id, date=overdue_date.date,
                                            date_id=overdue_date.date_id))
                    self.notify_users_of_task(date_task, message_title, message_text)
                    # Update condition tasks, as status was changed
        self.update_condition_tasks()

    @staticmethod
    @write_exception_log
    @write_enter_function_log()
    def set_up_database(data_storage_class, connection_string):
        """ Initializes database """
        data_storage_class.set_up_database(connection_string)

    # endregion
