"""
    This module contains classes and functions for
    checking constraints

    Classes:
        LogicConstrain - class to check illogical operations
        DuplicationConstrain - class to check duplication of object
        AuthorizationConstrain - class to check user rights

    Functions:
        check_object_existence - checks whether object is None


"""
from planner4ucore.models import enums as me
from planner4ucore.models import models as mo
from planner4ucore.storage.sqlalchemy_storage import session_scope
from planner4ucore.services import exceptions as ex
from planner4ucore.services.log_functions import write_enter_function_log


@write_enter_function_log()
def check_object_existence(obj, object_id, object_name):
    """
        Checks whether given object is None

        Args:
            obj (object): object to check to check
            object_id (int): id of object which will be listed in exception message
            object_name (string): name which will be listed in exception message

        Raises:
            ObjectIsNotFoundError: is object is None

    """
    if obj is None:
        raise ex.ObjectIsNotFoundError(object_id, object_name)


class LogicConstraint:
    """
        Provides functions to check logic mistakes
        and constraints

        Functions:
            check_plan_restriction
            check_condition_restriction
            check_add_date_restriction
            check_add_plan_restriction
            check_task_can_be_executed
            check_update_date_restriction
            check_subtask_loop_dependency

        Attributes:
            data_storage: object implementing data-storage interface

    """
    def __init__(self, data_storage):
        """
            Args:
            data_storage: object implementing data-storage interface

        """
        self.ds = data_storage

    @write_enter_function_log()
    def check_plan_restriction(self, task_id):
        """
            Checks whether task corresponding to given id is plan template

            Args:
                task_id (int): id of task you want to check for plan type

            Raises:
                RestrictionError: if checked task is plan template

        """
        task = self.ds.get_task(task_id)

        if task.task_type == me.TaskType.PLAN.value:
            raise ex.RestrictionError("This operation cannot be applied to plan (id {})".format(task_id))

    @write_enter_function_log()
    def check_condition_restriction(self, task_id):
        """
            Checks whether task with given id is condition task

            Args:
                task_id (int): id of task you want to check

            Raises:
                RestrictionError: if task is condition task

        """
        task = self.ds.get_task(task_id)

        check_object_existence(task, task_id, "condition task")

        if task.task_type == me.TaskType.CONDITIONAL.value:
            raise ex.RestrictionError("This operation cannot be applied to condition task (id {})".format(task_id))

    @write_enter_function_log()
    def check_add_date_restriction(self, date):
        """
            Checks whether date can be added to the task

            Args:
                date (models.TaskDate): date that you want to add to the task

            Raises:
                RestrictionError: if date can't be added to the task

        """
        task = self.ds.get_task(date.task_id)
        check_object_existence(task, task.task_id, "task")

        # Get dates connected with task

        task_dates = self.ds.get_task_dates(task.task_id)

        # Check restrictions if date type is event
        if date.date_type == me.DateType.EVENT.value:
            for task_date in task_dates:
                if task_date.date_type == me.DateType.END.value or task_date.date_type == me.DateType.START.value:
                    raise ex.RestrictionError(
                        "")
                if task_date.date_type == date.date_type:
                    raise ex.RestrictionError("")
        else:
            # Checking period dates restrictions
            for task_date in task_dates:
                if task_date.date_type == me.DateType.EVENT.value:
                    raise ex.RestrictionError("")
                if task_date.date_type == date.date_type:
                    raise ex.RestrictionError("")
                if (date.date_type == me.DateType.END.value and task_date.date_type == me.DateType.START.value
                        and date.date < task_date.date):
                    raise ex.RestrictionError((""))
                if (date.date_type == me.DateType.START.value and task_date.date_type == me.DateType.END.value
                        and date.date > task_date.date):
                    raise ex.RestrictionError((""))

    @classmethod
    @write_enter_function_log()
    def check_add_plan_restriction(cls, plan):
        """
            Checks whether plan setting were specified correctly during creation

            Args:
                plan (models.Plan): plan to verify

            Raises:
                RestrictionError: if plan has incorrect attributes

        """
        if plan.end_type == me.PlanCompletionCriteria.AFTER_DATE.value and plan.end_date is None:
            raise ex.RestrictionError("You've chosen to stop plan after the date, but haven't specified this date")
        if plan.end_type == me.PlanCompletionCriteria.AFTER_NUMBER_OF_TIMES.value and plan.repetitions_number is None:
            raise ex.RestrictionError(("You've chosen to stop plan after number of creations,"
                                       " but haven't specified this this number"))

    @write_enter_function_log()
    def check_update_plan_restriction(self, args):
        if mo.PLAN_ATTR_END_TYPE in args:
            end_type = args[mo.PLAN_ATTR_END_TYPE]
            if end_type == me.PlanCompletionCriteria.AFTER_DATE.value and mo.PLAN_ATTR_END_DATE not in args:
                raise ex.RestrictionError("You've chosen to stop plan after the date, but haven't specified this date")
            if (end_type == me.PlanCompletionCriteria.AFTER_NUMBER_OF_TIMES.value and
                    mo.PLAN_ATTR_REPETITIONS_NUMBER not in args):
                raise ex.RestrictionError(("You've chosen to stop plan after number of creations,"
                                           " but haven't specified this this number"))

    @write_enter_function_log()
    def check_task_can_be_executed(self, task):
        """
            Checks whether task can be executed due to sub tasks

            Args:
                task (models.Task): task to check

            Raises:
                DependencyError: if task can't be executed because of child blocking relation

        """
        with session_scope() as session:
            # Get all subtask relations
            relations = self.ds.get_task_subtask_relations(task.task_id, session=session)
            for relation in relations:
                # If only one of subtask is not executed and relation is blocking,
                # raise exception
                if (relation.sub_task.status != me.TaskStatus.EXECUTED.value
                        and relation.relation_type == me.TaskRelation.BLOCKING.value):
                    raise ex.DependencyError(("Task (or dependent subtask) can't be executed, because of blocking "
                                              "subtask dependecy (subtask id {})".format(relation.sub_task.task_id)))

    @write_enter_function_log()
    def check_update_date_restriction(self, date_id, new_date):
        """
            Checks whether date can be updated whith the new date
            by looking for date task and examinating its dates

            Args:
                date_id (int): id of task you want to update
                new_date (date): date to update

            Raises:
                RestrictionError: if date object can't be updated with new date

        """

        date = self.ds.get_date(date_id)

        check_object_existence(date, date_id, "date")

        task_dates = self.ds.get_task_dates(date.task_id)

        # Check that if date type is start it gets along well with end-date of task
        if date.date_type == me.DateType.START.value:
            for task_date in task_dates:
                if task_date.date_type == me.DateType.END.value and task_date.date < new_date:
                    raise ex.RestrictionError(
                        "Task already has end date and it's earlier than one you're trying to update")

        # Check that if date type is end it gets along well with start-date of task
        if date.date_type == me.DateType.END.value:
            for task_date in task_dates:
                if task_date.date_type == me.DateType.START.value and task_date.date > new_date:
                    raise ex.RestrictionError(
                        "Task already has start date and it's later than one you're trying to update")

        # If type of date is event it doesn't matter

    @write_enter_function_log()
    def check_subtask_loop_dependency(self, sub_task_id, parent_task_id):
        """"
            Checks loop connection between two task,
            ensuring that subtask is not parent of parent task
            and parent task is not subtask of sub task

            Args:
                sub_task_id (int): id of sub task
                parent_task_id (int): id of parent task

            Raises:
                RestrictionError: if such subtask relation forms loop

        """
        sub_task = self.ds.get_task(sub_task_id)
        check_object_existence(sub_task, sub_task_id, "sub-task")
        parent_task = self.ds.get_task(parent_task_id)
        check_object_existence(parent_task, parent_task_id, "parent-task")

        # Get all parents ids
        parents_id = set([task.task_id for task in self.ds.get_parent_tasks(sub_task_id)])

        # And recursively add all parent id of parents and so on
        def add_parents(task_id):
            parent_tasks = self.ds.get_parent_tasks(task_id)
            for task in parent_tasks:
                add_parents(task.task_id)
            parents_id.add(task_id)

        add_parents(parent_task_id)

        # If sub task id is parent id, it forms a loop, so lets raise exception
        if sub_task_id in parents_id:
            raise ex.RestrictionError("Can't add subtask relation, as it forms a loop")


class DuplicationConstraint:
    """
        Provides functions to check relationship duplications

        Functions:
            check_task_group_relation_duplication
            check_subtask_relation_duplication
            check_user_executor_duplication

        Attributes:
            data_storage: object implementing data-storage interface

    """
    def __init__(self, data_storage):
        """
            Args:
                data_storage: object implementing data-storage interface

        """
        self.ds = data_storage

    @write_enter_function_log()
    def check_task_group_relation_duplication(self, task_group_relation):
        """
            Checks that task is not in the group,
            and this relation is unique

            Args:
                task_group_relation (models.TaskGroupRelation): task-group relation to check

            Raises:
                DuplicatedObjectError: if given task group relation is not unique

        """
        args = {
            mo.GROUP_RELATION_ATTR_GROUP_ID: task_group_relation.group_id,
            mo.GROUP_RELATION_ATTR_TASK_ID: task_group_relation.task_id
        }
        if self.ds.is_duplication(mo.TaskGroupRelation, args):
            raise ex.DuplicatedObjectError("Task group relation already exists")

    @write_enter_function_log()
    def check_subtask_relation_duplication(self, subtask_relation):
        """
            Checks that this subtask relation doesn't exist,
            so this can be addes

            Args:
                subtask_relation (modles.SubtaskRelaton): relation to check

            Raises:
                DuplicatedObjectError: if sub task relation already exists

        """
        args = {
            mo.SUBTASK_RELATION_ATTR_PARENT_ID: subtask_relation.parent_task_id,
            mo.SUBTASK_RELATION_ATTR_CHILD_ID: subtask_relation.sub_task_id
        }
        if self.ds.is_duplication(mo.SubtaskRelation, args):
            raise ex.DuplicatedObjectError("Subtask relation already exists")

    @write_enter_function_log()
    def check_user_executor_duplication(self, user_executor_relation):
        """
            Check if user is not executor of this task already,
            so relation can be added

            Args:
                user_executor_relation (models.UserExecutorRelation): relation to check

            Raises:
                DuplicatedObjectError: if this user-executor relation already exists

        """
        args = {
            mo.EXECUTOR_RELATION_ATTR_EXECUTOR_ID: user_executor_relation.user_executor_id,
            mo.EXECUTOR_RELATION_ATTR_TASK_ID: user_executor_relation.task_id
        }

        if self.ds.is_duplication(mo.UserExecutorRelation, args):
            raise ex.DuplicatedObjectError("User executor relation already exists")


class AuthorizationConstraint:
    """
        This provides functions for checking user rights

        Functions:
            authorize_for_task
            authorize_for_group
            authorize_for_task_group_relation
            authorize_for_date
            authorize_for_subtask_relation
            authorize_for_subtask_relation_by_id
            authorize_for_message
            authorize_for_plan
            authorize_for_notification
            authorize_for_reminder
            authorize_for_user_executor_relation
            authorize_for_condition_relation

        Attributes:
            data_storage: object implementing data-storage interface

    """
    def __init__(self, data_storage):
        """
            Args:
                data_storage: object implementing data-storage interface

        """
        self.ds = data_storage

    @write_enter_function_log()
    def authorize_for_task(self, user_id, task_id, executor=True):
        """
            Check user rights for given task.

            By default user has rights, as executor.
            Specify executor=False to restrict rights to user-creator

            Args:
                 user_id (int):
                 task_id (int):
                 executor (bool) = True:

            Raises:
                 AuthorizationError: if user doesn't have right to work with this task

        """
        task = self.ds.get_task(task_id)
        check_object_existence(task, task_id, "task")

        user = self.ds.get_user(user_id)
        check_object_existence(user, user_id, "user")

        if task.user_creator == user_id:
            return True

        if executor:
            users_executors = self.ds.get_users_executors(task_id)
            users_executors_id = set([user.user_id for user in users_executors])
            if user_id in users_executors_id:
                return True

        # If user is not creator nor executor, raise error
        raise ex.AuthorizationError("User doesn't have rights to work with task (id {})".format(task_id))

    @write_enter_function_log()
    def authorize_for_group(self, user_id, group_id):
        """
            Check user's rights for given task.

            Args:
                user_id (int):
                group_id (int):

            Raises:
                AuthorizationError: if user doesn't have rights to work with this group

        """
        group = self.ds.get_group(group_id)
        check_object_existence(group, group_id, "group")

        if group.user_creator_id != user_id:
            raise ex.AuthorizationError("User doesn't have rights to work with group (id {})".format(group_id))

    @write_enter_function_log()
    def authorize_for_task_group_relation(self, user_id, group_id, task_id):
        """
            Checks whether task can be added to group

            Args:
                user_id (int):
                group_id (int):
                task_id (int):

            Raises:
                AuthorizationError: is user doesn't have right for group or (and) task

        """

        self.authorize_for_task(user_id, task_id)
        self.authorize_for_group(user_id, group_id)

    @write_enter_function_log()
    def authorize_for_date(self, user_id, date_id, executor=True):
        """
            Check whether user have permission to work with this date

            Args:
                 user_id (int)
                 date_id (int)
                 executor (bool)

            Raises:
                AuthorizationError: if user doesn't have rights to work with this date

        """
        with session_scope() as session:
            date = self.ds.get_date(date_id, session=session)
            check_object_existence(date, date_id, "date")
            self.authorize_for_task(user_id, date.task.task_id, executor)

    @write_enter_function_log()
    def authorize_for_subtask_relation(self, user_id, parent_task_id, sub_task_id, executor=True):
        """
            Check user right for parent task and sub task.

            Args:
                 user_id (int)
                 parent_task_id (int)
                 sub_task_id (int)
                 executor (bool)

            Raises:
                AuthorizationError: if user doesn't have rights to work with these tasks

        """

        self.authorize_for_task(user_id, parent_task_id, executor)
        self.authorize_for_task(user_id, sub_task_id, executor)

    @write_enter_function_log()
    def authorize_for_subtask_relation_by_id(self, user_id, relation_id, executor=True):
        """
            Checks user rights for adding this subtask relation

            Args:
                 user_id (int)
                 relation_id (int)
                 executor (bool)

        """
        relation = self.ds.get_subtask_relation_by_id(relation_id)
        check_object_existence(relation, relation_id, "subtask relation")

        self.authorize_for_task(user_id, relation.parent_task_id, executor)
        self.authorize_for_task(user_id, relation.sub_task_id, executor)

    @write_enter_function_log()
    def authorize_for_message(self, user_id, message_id):
        """
            Checks whether user have rights to work with this message

            Args:
                 user_id (int):
                 message_id (int)

            Raises:
                AuthorizationError: if user doesn't have rights to work with this message

        """
        user = self.ds.get_user(user_id)
        check_object_existence(user, user_id, "user")

        message = self.ds.get_task_message(message_id)
        check_object_existence(message, message_id, "message")

        if message.user_id != user_id:
            raise ex.AuthorizationError("User doesn't have rights to work with this message (id {})".format(message_id))

    @write_enter_function_log()
    def authorize_for_plan(self, user_id, plan_id):
        """
            Checks user rights to work with plan

            Args:
                user_id (int)
                plan_id (int)

            Raises:
                AuthorizationError: if user doesn't have rights to work with plan

        """
        plan = self.ds.get_plan(plan_id)
        check_object_existence(plan, plan_id, "plan")

        task = self.ds.get_task(plan.task_id)
        if task.user_creator != user_id:
            raise ex.AuthorizationError("User doesn't have rights to work with plan (id {})".format(plan_id))

    @write_enter_function_log()
    def authorize_for_notification(self, user_id, notification_id, executor=True):
        """
            Check user rights to work with notification

            Args:
                user_id (int)
                notification_id (int)
                executor (bool)

        """
        with session_scope() as session:
            notification = self.ds.get_notification(notification_id, session=session)

            check_object_existence(notification, notification_id, "notification")

            task = notification.date.task
            self.authorize_for_task(user_id, task.task_id, executor)

    @write_enter_function_log()
    def authorize_for_reminder(self, user_id, reminder_id, executor=True):
        """
            Check user rights for working with specified reminder

            Args:
                user_id (int)
                reminder_id (int)
                executor (bool)

        """
        with session_scope() as session:
            reminder = self.ds.get_reminder(reminder_id, session=session)
            check_object_existence(reminder, reminder_id, "reminder")

            task = reminder.task
            self.authorize_for_task(user_id, task.task_id, executor)

    @write_enter_function_log()
    def authorize_for_user_executor_relation(self, user_id, user_executor_relation_id):
        """
            Checks user rights to work with user-executor relation

            Args:
                 user_id (int)
                 user_executor_relation_id (int)

            Raises:
                AuthorizationError: if user doesn't have rights to work with relation

        """
        relation = self.ds.get_user_executor_relation(user_executor_relation_id)
        check_object_existence(relation, user_executor_relation_id, "user executor relation")

        task = self.ds.get_task(relation.task_id)
        if user_id != task.user_creator:
            raise ex.AuthorizationError("""User doesn't have rights to add executor
                                        relations for task (id {})""".format(user_id))

    @write_enter_function_log()
    def authorize_for_condition_relation(self, user_id, condition_relation_id):
        """"
            Check user rights to work with this relation

            Args:
                user_id (int)
                condition_relation_id (int)

        """
        with session_scope() as session:
            relation = self.ds.get_condition_task_relation(condition_relation_id, session=session)
            check_object_existence(relation, condition_relation_id, "condition task relation")

            self.authorize_for_task(user_id, relation.condition_task.task_id)
