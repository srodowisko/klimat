"""
    This module contains exceptions that are used in services package

    Planner4UError - base exception for others
    AuthorizationError - raises if user doesn't have rights to perform operation
    ObjectIsNotFoundError - raises if there is no required object in storage
    RestrictionError - raises if user tries to perform illogical operation
    DuplicatedObjectError - raises if user tries to add duplicated object in storage
    DependencyError - raises if relation operation can't be performed

"""


class Planner4UError(Exception):
    """
        Planner4UError - base exception for others

        Args:
            message (str) - describes reason of the exception

    """
    def __init__(self, message):
        super().__init__(message)


class AuthorizationError(Planner4UError):
    """
        AuthorizationError - raises if user doesn't have rights to perform operation

        For example, trying to add his own task to the group of another user

        Args:
            message (str) - describes reason of the exception

    """

    def __init__(self, message):
        super().__init__(message)


class ObjectIsNotFoundError(Planner4UError):
    """
        ObjectIsNotFoundError - raises if there is no required object in storage

        Args:
            object_id (int) - id of object that wasn't found
            object_type (str) - name of object class

    """
    def __init__(self, object_id, object_type):
        super().__init__("Couldn't find {type} with id {id}".format(type=object_type, id=object_id))


class RestrictionError(Planner4UError):
    """
        RestrictionError - raises if user tries to perform illogical operation

        For example, trying to add date on plan template task

        Args:
            message (str) - describes reason of the exception

    """
    def __init__(self, message):
        super().__init__(message)


class DuplicatedObjectError(Planner4UError):
    """
        DuplicatedObjectError - raises if user tries to add duplicated object in storage

        For example, to add the same task to the same group again

        Args:
            message (str) - describes reason of the exception

    """
    def __init__(self, message):
        super().__init__(message)


class DependencyError(Planner4UError):
    """
        DependencyError - raises if relation operation can't be performed

        For example, trying to execute task with unexecuted subtask will raise this exception

        Args:
            message (str) - describes reason of the exception

    """
    def __init__(self, message):
        super().__init__(message)
