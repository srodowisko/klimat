"""    Contains functions-initializers for parser and custom parser

    Calsses:
        Planner4UParser - custom parser for proper error handling

    Naming convention for functions:
        initialize_"action-command"_FIRST_COMMAND_parser

    Global variables:
        TASK_COMMAND
        PLAN_COMMAND
        CONDITION_COMMAND
        GROUP_COMMAND
        DATE_COMMAND
        REMINDER_COMMAND
        NOTIFICATION_COMMAND
        RELATION_COMMAND
        MESSAGE_COMMAND
        SUBTASK_COMMAND
        EXECUTOR_COMMAND
        TASKS_COMMAND
        GROUPS_COMMAND
        MESSAGES_COMMAND
        USERS_COMMAND
        SUBTASKS_COMMAND

        ADD_COMMAND
        SHOW_COMMAND
        DELETE_COMMAND
        UPDATE_COMMAND

    Functions:
        get_parsed_namespace - entry point for initialization
        initialize_task_subparser
        initialize_plan_subparser
        initialize_condition_subparser
        initialize_group_subparser
        initialize_date_subparser
        initialize_reminder_subparser
        initialize_notification_subparser
        initialize_relation_subparser
        initialize_message_subparser
        inititalize_show_subparser

        initialize_create_task_parser
        initialize_create_plan_parser
        initialize_create_condition_parser
        initialize_create_group_parser
        initialize_create_date_parser
        initialize_create_reminder_parser
        initialize_create_notification_parser
        initialize_crete_relation_parser
        initialize_create_relation_executor_parser
        initialize_create_task_group_relation
        initialize_create_subtask_relation

        initialize_show_task_parser
        initialize_show_plan_parser
        initialize_show_tasks_parser
        initialize_show_relation_parser
        initialize_show_relation_condition_parser
        initialize_show_user_executor_relation_parser
        initialize_show_subtsk_relation_parser
        initialize_show_date_parser
        initialize_show_group_parser
        initialize_show_reminder_parser
        initialize_show_notification_parser
        initialize_show_subtasks_parser
        initialize_show_message_parser

        initialize_delete_task_parser
        initialize_delete_plan_parser
        initialize_delete_relation_parser
        initialize_delete_task_group_relation_parser
        initialize_delete_user_executor_relation_parser
        initialize_delete_subtask_relation_parser
        initialize_delete_date_parser
        initialize_delete_group_parser
        initialize_delete_reminder_parser
        initialize_delete_notification_parser

        initialize_update_task_parser
        initialize_update_task_status_parser
        initialize_update_relation_parser
        initialize_update_condition_relation
        initialize_update_subtask_relation
        initialize_update_date_parser
        initialize_update_group_parser
        initialize_update_remindre_parser

        valid_date - checker function for date (format "YYYY-MM-DD")
        valid_time - checker function for time (format "HH:MM")
        positive_integer - checker function for int value

"""
import argparse
import sys

from datetime import datetime

from planner4ucore.models import enums as me


TASK_COMMAND = "task"
PLAN_COMMAND = "plan"
CONDITION_COMMAND = "condition"
GROUP_COMMAND = "group"
DATE_COMMAND = "date"
REMINDER_COMMAND = "reminder"
NOTIFICATION_COMMAND = "notification"
RELATION_COMMAND = "relation"
MESSAGE_COMMAND = "message"
SUBTASK_COMMAND = "subtask"
EXECUTOR_COMMAND = "executor"
TASKS_COMMAND = "tasks"
GROUPS_COMMAND = "groups"
MESSAGES_COMMAND = "messages"
USERS_COMMAND = "users"
SUBTASKS_COMMAND = "subtasks"
INIT_COMMAND = "init"

ADD_COMMAND = "add"
SHOW_COMMAND = "show"
DELETE_COMMAND = "delete"
UPDATE_COMMAND = "update"

FIRST_COMMAND = "first_command"
SECOND_COMMAND = "second_command"
THIRD_COMMAND = "third_command"


class Planner4UParser(argparse.ArgumentParser):
    """
        Derives from argparse.ArgumentParser and
        overrides error method for help printing

    """
    def error(self, message):
        self.print_help()
        print("Cause: {}".format(message))
        sys.exit(2)


def get_parsed_namespace():
    """
        Initializes parser mainly specifying options for
        first, second and third argument

        Returns:
            Initialized parser

    """
    parser = Planner4UParser(prog="Planner4U",
                             description=("This program allows create simple, plans and condition tasks. You can track "
                                          "important dates and set notifications on them. Add executors and gather "
                                          "tasks into groups."),
                             epilog="(c) Miroslav Ganevich MKiS 2021",
                             formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    # action command subparsers
    subparsers = parser.add_subparsers(dest=FIRST_COMMAND,
                                       title="Possible object-commands",
                                       description="""This commands should go as first parameter 
                                       specifying object which you want to work with""")
    subparsers.required = True

    task_subparser = subparsers.add_parser(TASK_COMMAND, help="Type to work with task")
    initialize_task_subparser(task_subparser)

    plan_subparser = subparsers.add_parser(PLAN_COMMAND, help="Type to work with plan")
    initialize_plan_subparser(plan_subparser)

    condition_subparser = subparsers.add_parser(CONDITION_COMMAND,
                                                help="Type to work with condition task and its relation")
    initialize_condition_subparser(condition_subparser)

    group_subparser = subparsers.add_parser(GROUP_COMMAND, help="Type to work with group")
    initialize_group_subparser(group_subparser)

    date_subparser = subparsers.add_parser(DATE_COMMAND, help="Type to work with dates")
    initialize_date_subparser(date_subparser)

    reminder_subparser = subparsers.add_parser(REMINDER_COMMAND, help="Type to work with reminder")
    initialize_reminder_subparser(reminder_subparser)

    notification_subparser = subparsers.add_parser(NOTIFICATION_COMMAND, help="Type to work with notification")
    initialize_notification_subparser(notification_subparser)

    relation_subparser = subparsers.add_parser(RELATION_COMMAND, help="Type to work with relations")
    initialize_relation_subparser(relation_subparser)

    message_subparser = subparsers.add_parser(MESSAGE_COMMAND, help="Type to work with message")
    initialize_message_subparser(message_subparser)

    show_tasks_parser = subparsers.add_parser(TASKS_COMMAND, help="Show user's tasks ")
    initialize_show_tasks_parser(show_tasks_parser)

    subparsers.add_parser(GROUPS_COMMAND, help="Show all user groups")

    subparsers.add_parser(MESSAGES_COMMAND, help="Show user's messages")

    show_subtasks_parser = subparsers.add_parser(SUBTASKS_COMMAND, help="Show subtasks of specified task")
    initialize_show_subtasks_parser(show_subtasks_parser)

    subparsers.add_parser(USERS_COMMAND, help="Show all users")

    init_subparser = subparsers.add_parser(INIT_COMMAND, help="Initial command to set up database and add user")
    initialize_init_subparser(init_subparser)

    return parser.parse_args()


def initialize_task_subparser(parser):
    subparsers = parser.add_subparsers(dest=SECOND_COMMAND, title="Possible actions",
                                       description="""Task is core object, has title and description, priority, dates.
                                       Can be contained in group.""")
    subparsers.required = True

    create_task_parser = subparsers.add_parser(ADD_COMMAND, help="Create task specifying its parameters")
    initialize_create_task_parser(create_task_parser)

    show_task_parser = subparsers.add_parser(SHOW_COMMAND, help="Show user's task giving its id")
    initialize_show_task_parser(show_task_parser)

    delete_task_parser = subparsers.add_parser(DELETE_COMMAND, help="""Delete task and all its relations (subtask,
                                               user-executor, condition) and object connected with it (dates,
                                               notifications, reminders)""")
    initialize_delete_task_parser(delete_task_parser)

    update_task_parser = subparsers.add_parser(UPDATE_COMMAND, help="Specify id of the task you want to update")
    initialize_update_task_parser(update_task_parser)


def initialize_plan_subparser(parser):
    subparsers = parser.add_subparsers(dest=SECOND_COMMAND, title="Action to perform an operation ",
                                       description="""Plan will be repeatedly create template task""")
    subparsers.required = True

    create_plan_parser = subparsers.add_parser(ADD_COMMAND, help="Create periodic plan specifying template task")
    initialize_create_plan_parser(create_plan_parser)

    show_plan_parser = subparsers.add_parser(SHOW_COMMAND, help="Show plan and its template task")
    initialize_show_plan_parser(show_plan_parser)

    delete_plan_parser = subparsers.add_parser(DELETE_COMMAND, help="Delete plan and its template task")
    initialize_delete_plan_parser(delete_plan_parser)

    update_plan_parser = subparsers.add_parser(UPDATE_COMMAND, help="Update plan")
    initialize_update_plan_parser(update_plan_parser)


def initialize_condition_subparser(parser):
    subparsers = parser.add_subparsers(dest=SECOND_COMMAND, title="Possible actions on condition",
                                       description="""Condition tracks status of specified task and if 
                                       task status was changed on suitable - defined task created""")
    subparsers.required = True

    create_condition_parser = subparsers.add_parser(ADD_COMMAND, help="""Create condition task 
                                                    specifying its parameters and trigger task""")
    initialize_create_condition_parser(create_condition_parser)

    show_condition_parser = subparsers.add_parser(SHOW_COMMAND, help="Show condition task giving its id")
    initialize_show_relation_condition_parser(show_condition_parser)

    update_condition_relation = subparsers.add_parser(UPDATE_COMMAND, help="Update condition task relation")
    initialize_update_condition_relation(update_condition_relation)

    delete_condition_parser = subparsers.add_parser(DELETE_COMMAND, help="""Delete condition task and all its relations 
                                                (subtask, user-executor, condition) and object connected with it (dates,
                                                notifications, reminders)""")
    initialize_delete_condition_relation_parser(delete_condition_parser)


def initialize_group_subparser(parser):
    subparsers = parser.add_subparsers(dest=SECOND_COMMAND, title="Action to perform an operation",
                                       description="""Group unites set of tasks""")
    subparsers.required = True

    create_group_parser = subparsers.add_parser(ADD_COMMAND, help="Create group specifying its title")
    initialize_create_group_parser(create_group_parser)

    show_group_parser = subparsers.add_parser(SHOW_COMMAND, help="Show group information")
    initialize_show_group_parser(show_group_parser)

    delete_group_parser = subparsers.add_parser(DELETE_COMMAND, help="Delete group and connected task-group relations")
    initialize_delete_group_parser(delete_group_parser)

    update_group_parser = subparsers.add_parser(UPDATE_COMMAND, help="Specify id of group you want to update")
    initialize_update_group_parser(update_group_parser)


def initialize_date_subparser(parser):
    subparsers = parser.add_subparsers(dest=SECOND_COMMAND, title="Action to perform an operation",
                                       description="""Date is connected with task. There are three types of dates.
                                       Event - simple date when task should be executed. Start date - 
                                       Start date of period. End date - end date of period""")
    subparsers.required = True

    create_date_parser = subparsers.add_parser(ADD_COMMAND, help="Create date for task")
    initialize_create_date_parser(create_date_parser)

    show_date_parser = subparsers.add_parser(SHOW_COMMAND, help="Show task date")
    initialize_show_date_parser(show_date_parser)

    delete_date_parser = subparsers.add_parser(DELETE_COMMAND, help="Delete date(s) connected with task")
    initialize_delete_date_parser(delete_date_parser)

    update_date_parser = subparsers.add_parser(UPDATE_COMMAND, help="Specify id of date you want to update")
    initialize_update_date_parser(update_date_parser)


def initialize_reminder_subparser(parser):
    subparsers = parser.add_subparsers(dest=SECOND_COMMAND, title="Action to perform an operation",
                                       description="""Reminder will notify you of task repeatedly""")
    subparsers.required = True

    create_reminder_parser = subparsers.add_parser(ADD_COMMAND, help="Create periodic reminder for task")
    initialize_create_reminder_parser(create_reminder_parser)

    show_reminder_parser = subparsers.add_parser(SHOW_COMMAND, help="Show reminder parameters")
    initialize_show_reminder_parser(show_reminder_parser)

    delete_reminder_parser = subparsers.add_parser(DELETE_COMMAND, help="Delete reminder specifying its id")
    initialize_delete_reminder_parser(delete_reminder_parser)

    update_reminder_parser = subparsers.add_parser(UPDATE_COMMAND, help="Specify id of reminder you want to update")
    initialize_update_reminder_parser(update_reminder_parser)


def initialize_notification_subparser(parser):
    subparsers = parser.add_subparsers(dest=SECOND_COMMAND, title="Action to perform an operation",
                                       description="""Notification will remind you of date""")
    subparsers.required = True

    create_notification_parser = subparsers.add_parser(ADD_COMMAND, help="Create notification for date")
    initialize_create_notification_parser(create_notification_parser)

    show_notification_parser = subparsers.add_parser(SHOW_COMMAND, help="Show notification parameters")
    initialize_show_notification_parser(show_notification_parser)

    delete_notification_parser = subparsers.add_parser(DELETE_COMMAND, help="Delete notification specifying its id")
    initialize_delete_notification_parser(delete_notification_parser)

    update_notification_parser = subparsers.add_parser(UPDATE_COMMAND,
                                                       help="Specify id of notification you want to update")
    initialize_update_notification_parser(update_notification_parser)


def initialize_relation_subparser(parser):
    subparsers = parser.add_subparsers(dest=SECOND_COMMAND, title="Choose relation to work with",
                                       description="""Relations connect two objects (user and task, task and group,
                                       parent task and subtask)""")
    subparsers.required = True

    subtask_relation_parser = subparsers.add_parser(SUBTASK_COMMAND, help="Work with subtask relation")
    initialize_subtask_relation_parser(subtask_relation_parser)

    task_group_relation_parser = subparsers.add_parser("group", help="Work with task-group relation")
    initialize_task_group_relation_parser(task_group_relation_parser)

    executor_relation_parser = subparsers.add_parser(EXECUTOR_COMMAND, help="Work with executor relation")
    initialize_executor_relation_parser(executor_relation_parser)


def initialize_subtask_relation_parser(parser):
    subparsers = parser.add_subparsers(dest=THIRD_COMMAND, title="Action to perform an operation",
                                       description="""Subtask relations connects two tasks. Relation can be blocking 
                                       it means that you can't execute parent task without executing
                                       subtask. If subtask depends on parent task - it will be executed
                                       if parent task was executed. If tasks just connected they don't 
                                       constraint or depend on each other and connected only logically""")
    subparsers.required = True

    add_subtask_relation = subparsers.add_parser(ADD_COMMAND, help="Add new subtask relation")
    initialize_create_subtask_relation(add_subtask_relation)

    show_subtask_relation = subparsers.add_parser(SHOW_COMMAND, help="Show subtask relation")
    initialize_show_subtask_relation_parser(show_subtask_relation)

    delete_subtask_relation = subparsers.add_parser(DELETE_COMMAND, help="Delete subtask relation")
    initialize_delete_subtask_relation_parser(delete_subtask_relation)

    update_subtask_relation = subparsers.add_parser(UPDATE_COMMAND, help="Update subtask relation")
    initialize_update_subtask_relation(update_subtask_relation)


def initialize_task_group_relation_parser(parser):
    subparsers = parser.add_subparsers(dest=THIRD_COMMAND, title="Action to perform an operation",
                                       description="""This relations specifies whether group contains the task""")
    subparsers.required = True

    add_task_group_relation = subparsers.add_parser(ADD_COMMAND, help="Add task-group relation")
    initialize_create_task_group_relation(add_task_group_relation)

    delete_task_group_relation = subparsers.add_parser(DELETE_COMMAND, help="Delete task-group relation")
    initialize_delete_task_group_relation_parser(delete_task_group_relation)


def initialize_executor_relation_parser(parser):
    subparsers = parser.add_subparsers(dest=THIRD_COMMAND, title="Action to perform an operation.",
                                       description="""This relation specifies executor for task""")
    subparsers.required = True

    add_executor_relation = subparsers.add_parser(ADD_COMMAND, help="Add new user-executor")
    initialize_create_relation_executor(add_executor_relation)

    delete_relation_executor = subparsers.add_parser(DELETE_COMMAND, help="Delete user-executor")
    initialize_delete_user_executor_relation_parser(delete_relation_executor)


def initialize_message_subparser(parser):
    subparsers = parser.add_subparsers(dest=SECOND_COMMAND, title="Possible actions",
                                       description="Action to perform an operation")
    subparsers.required = True

    show_message_parser = subparsers.add_parser(SHOW_COMMAND, help="Show message")
    initialize_show_message_parser(show_message_parser)

    delete_message_parser = subparsers.add_parser(DELETE_COMMAND, help="Delete message")
    initialize_show_message_parser(delete_message_parser)


def initialize_init_subparser(parser):
    parser.add_argument("--connection-string", required=True, type=str,
                        help="Connection string of database to set up")
    parser.add_argument("--username", type=str, required=True,
                        help="Name of user to add")


# region Create parsers initialization
def initialize_create_task_parser(parser):
    parser.add_argument("-t", "--title", required=True, type=str,
                        help="Title of the task")
    parser.add_argument("-d", "--description", type=str,
                        help="Brief description of the task")
    parser.add_argument("-p", "--priority", choices=me.STRING_TO_TASK_PRIORITY_ENUM.keys(),
                        default=me.PRIORITY_REGULAR, help="Task priority")
    parser.add_argument("--overdue-priority", choices=me.STRING_TO_TASK_PRIORITY_ENUM.keys(),
                        help="Priority which will be set on task if task is overdue")
    parser.add_argument("--sub-dependent", choices=me.STRING_TO_SUBTASK_DEPENDENCY_ENUM.keys(), default=me.NO,
                        help="Close task if last sub task was executed")
    parser.add_argument("--date", help="Date for this task", type=valid_date)
    parser.add_argument("--date-start", help="Start date for this task", type=valid_date)
    parser.add_argument("--date-end", help="End date for this task", type=valid_date)


def initialize_create_plan_parser(parser):
    # Plan part
    parser.add_argument("-s", "--start-date", required=True, type=valid_date_in_future,
                        help="Date when first task of plan will be created")
    parser.add_argument("-r", "--repeat-type", required=True, choices=me.STRING_TO_REPETITION_TYPE_ENUM.keys(),
                        help="Specifying time measure for plan")
    parser.add_argument("-i", "--interval", required=True, type=positive_integer,
                        help="Interval of measured units defined in --repeat-type")
    parser.add_argument("-d", "--end-date", type=valid_date_in_future,
                        help="Plan will be deactivated after this date")
    parser.add_argument("-n", "--repetitions-number", type=positive_integer,
                        help="Plan will be deactivated after template task was created specified number of times")
    # Task part
    parser.add_argument("-t", "--title", required=True, type=str,
                        help="Title of the task")
    parser.add_argument("-D", "--description", type=str,
                        help="Brief description of the task")
    parser.add_argument("-p", "--priority", choices=me.STRING_TO_TASK_PRIORITY_ENUM.keys(),
                        default=me.PRIORITY_REGULAR, help="Task priority")
    parser.add_argument("--overdue-priority", choices=me.STRING_TO_TASK_PRIORITY_ENUM.keys(),
                        help="Priority which will be set on task if task is overdue")
    parser.add_argument("--sub-dependent", choices=me.STRING_TO_SUBTASK_DEPENDENCY_ENUM.keys(),
                        default=me.NO, help="Close task if last sub task was executed")


def initialize_create_condition_parser(parser):
    parser.add_argument("-i", "--trigger-task-id", required=True, type=positive_integer,
                        help="Id of trigger task")
    parser.add_argument("-s", "--trigger-task-status", required=True,
                        choices=me.STRING_TO_UPDATABLE_TASK_STATUSES.keys(),
                        help="Status of a trigger task, on which condition task will be created")
    parser.add_argument("-t", "--title", required=True, type=str,
                        help="Title of condition the task")
    parser.add_argument("-d", "--description", type=str,
                        help="Brief description of condition task")
    parser.add_argument("-p", "--priority", choices=me.STRING_TO_TASK_PRIORITY_ENUM.keys(),
                        default=me.PRIORITY_REGULAR, help="Condition ask priority")
    parser.add_argument("--overdue-priority", choices=me.STRING_TO_TASK_PRIORITY_ENUM.keys(),
                        help="Priority which will be set on task if condition task is overdue")
    parser.add_argument("--sub-dependent", choices=me.STRING_TO_SUBTASK_DEPENDENCY_ENUM.keys(), default=me.NO,
                        help="Close condition task if last sub task was executed")


def initialize_create_group_parser(parser):
    parser.add_argument("-t", "--title", required=True, type=str, help="Title of the group")


def initialize_create_date_parser(parser):
    parser.add_argument("-i", "--task-id", required=True, type=positive_integer,
                        help="Id of task to which this date will be assigned")
    parser.add_argument("-d", "--date", required=True, type=valid_date,
                        help="Date in format YYYY-MM-DD")
    parser.add_argument("-t", "--type", choices=me.STRING_TO_DATE_TYPE_ENUM.keys(),
                        help="Choose one date option", default=me.DATE_TYPE_EVENT)


def initialize_create_reminder_parser(parser):
    parser.add_argument("-i", "--task-id", required=True, type=positive_integer,
                        help="id of the task on wich reminder will be hanged")
    parser.add_argument("-d", "--start-date", required=True, type=valid_date_in_future,
                        help="Date in format YYYY-MM-DD from which we will start reminding you")
    parser.add_argument("-t", "--time", required=True, type=valid_time,
                        help="Time in format HH:MM when you will be reminded")
    parser.add_argument("-D", "--days-interval", required=True, type=positive_integer,
                        help="Interval in days with which you will be informed")


def initialize_create_notification_parser(parser):
    parser.add_argument("-i", "--date-id", required=True, type=positive_integer,
                        help="id of the date on which this notification will be hanged")
    parser.add_argument("-t", "--time", required=True, type=valid_time,
                        help="Time in format HH:MM when you will be notified")
    parser.add_argument("-d", "--days", required=True, type=positive_integer,
                        help="You will be notified specified number of days before date")


def initialize_create_relation_executor(parser):
    parser.add_argument("-t", "--task-id", required=True, type=positive_integer,
                        help="Id of task to which user executor will be assigned")
    parser.add_argument("-e", "--executor-id", required=True, type=positive_integer,
                        help="Specify user executor")


def initialize_create_task_group_relation(parser):
    parser.add_argument("-t", "--task-id", required=True, type=positive_integer,
                        help="Id of task to add in the group")
    parser.add_argument("-g", "--group-id", required=True, type=positive_integer,
                        help="Id of group to whick task will be added")


def initialize_create_subtask_relation(parser):
    parser.add_argument("-p", "--parent-id", required=True, type=positive_integer,
                        help="Specify id of a parent task")
    parser.add_argument("-c", "--child-id", required=True, type=positive_integer,
                        help="Id of a sub task")
    parser.add_argument("-t", "--relation-type", choices=me.STRING_TO_TASK_RELATION_ENUM.keys(),
                        default=me.SUBTASK_CONNECTED, help="Relation type beteen this two tasks")
# endregion


# region Show parsers initialization
def initialize_show_task_parser(parser):
    parser.add_argument("-i", "--task-id", required=True, type=positive_integer,
                        help="Id of task you want to see")


def initialize_show_plan_parser(parser):
    parser.add_argument("-i", "--plan-id", required=True, type=positive_integer,
                        help="Id of plan you want to see")


def initialize_show_tasks_parser(parser):
    parser.add_argument("-d", "--details", action="store_const", const=True, default=False,
                        help="Show full description of each task")


def initialize_show_relation_condition_parser(parser):
    parser.add_argument("-i", "--relation-id", required=True, type=positive_integer,
                        help="Id of condition-task relation")


def initialize_show_user_executor_relation_parser(parser):
    parser.add_argument("-i", "--relation-id", required=True, type=positive_integer,
                        help="Id of user-executor relation")


def initialize_show_subtask_relation_parser(parser):
    parser.add_argument("-i", "--relation-id", required=True, type=positive_integer,
                        help="Id of task-subtask relation")


def initialize_show_date_parser(parser):
    parser.add_argument("-i", "--date-id", required=True, type=positive_integer,
                        help="Id of date you want to see information about")


def initialize_show_group_parser(parser):
    parser.add_argument("-i", "--group-id", required=True, type=positive_integer,
                        help="Id of group")


def initialize_show_reminder_parser(parser):
    parser.add_argument("-i", "--reminder-id", required=True, type=positive_integer,
                        help="Id of reminder")


def initialize_show_notification_parser(parser):
    parser.add_argument("-i", "--notification-id", required=True, type=positive_integer,
                        help="Id of notification")


def initialize_show_subtasks_parser(parser):
    parser.add_argument("-i", "--task-id", required=True, type=positive_integer,
                        help="Id of patent task")


def initialize_show_message_parser(parser):
    parser.add_argument("-i", "--message-id", required=True, type=positive_integer,
                        help="Id of message you want to read")
# endregion


# region Delete parsers initialization
def initialize_delete_task_parser(parser):
    parser.add_argument("-i", "--task-id", required=True, type=positive_integer,
                        help="Id of task you want to delete")


def initialize_delete_plan_parser(parser):
    parser.add_argument("-i", "--plan-id", required=True, type=positive_integer,
                        help="Id of plan you want to delete. Template task also will be deleted")


def initialize_delete_condition_relation_parser(parser):
    parser.add_argument("-i", "--relation-id", required=True, type=positive_integer,
                        help="Condition relation id")


def initialize_delete_task_group_relation_parser(parser):
    parser.add_argument("-t", "--task-id", required=True, type=positive_integer,
                        help="Task id")
    parser.add_argument("-g", "--group-id", required=True, type=positive_integer,
                        help="Group-id")


def initialize_delete_user_executor_relation_parser(parser):
    parser.add_argument("-t", "--task-id", required=True, type=positive_integer,
                        help="Task id")
    parser.add_argument("-e", "--executor-id", required=True, type=positive_integer,
                        help="User-executor id")


def initialize_delete_subtask_relation_parser(parser):
    parser.add_argument("-p", "--parent-id", required=True, type=positive_integer,
                        help="Parent task id")
    parser.add_argument("-c", "--child-id", required=True, type=positive_integer,
                        help="Sub (child) task id ")


def initialize_delete_date_parser(parser):
    parser.add_argument("-i", "--date-id", required=True, type=positive_integer,
                        help="Specify date id")


def initialize_delete_group_parser(parser):
    parser.add_argument("-i", "--group-id", required=True, type=positive_integer,
                        help="Specify group id")


def initialize_delete_reminder_parser(parser):
    parser.add_argument("-i", "--reminder-id", required=True, type=positive_integer,
                        help="Id of reminder you want to delete")


def initialize_delete_notification_parser(parser):
    parser.add_argument("-i", "--notification-id", required=True, type=positive_integer,
                        help="Id of notification you want to delete")
# endregion


# region Update parsers initialization
def initialize_update_task_parser(parser):
    parser.add_argument("-i", "--task-id", required=True, type=positive_integer,
                        help="Id of task you want to update")
    parser.add_argument("-t", "--title", type=str, help="Title of the task")
    parser.add_argument("-d", "--description", type=str,
                        help="Brief description of the task")
    parser.add_argument("-p", "--priority", choices=me.STRING_TO_TASK_PRIORITY_ENUM.keys(),
                        help="Task priority")
    parser.add_argument("--overdue-priority", choices=me.STRING_TO_TASK_PRIORITY_ENUM.keys(),
                        help="Priority which will be set on task if task is overdue")
    parser.add_argument("--sub-dependent", choices=me.STRING_TO_SUBTASK_DEPENDENCY_ENUM.keys(),
                        help="Close task if last sub task was executed")
    parser.add_argument("-s", "--status",
                        choices=me.STRING_TO_UPDATABLE_TASK_STATUSES.keys(),
                        help="New status of the task")
    parser.add_argument("--date", help="Date for this task", type=valid_date)
    parser.add_argument("--date-start", help="Start date for this task", type=valid_date)
    parser.add_argument("--date-end", help="End date for this task", type=valid_date)


def initialize_execute_task_parser(parser):
    parser.add_argument("-i", "--task-id", required=True, type=positive_integer,
                        help="Specify id of task you want to execute")


def initialize_close_task_parser(parser):
    parser.add_argument("-i", "--task-id", required=True, type=positive_integer,
                        help="Specify id of task you want to close")


def initialize_update_plan_parser(parser):
    parser.add_argument("-i", "--plan-id", required=True, help="Id of plan you want to update")
    parser.add_argument("-a", "--active", help="You can acitvate of disacitvate plan",
                        choices=me.STRING_TO_ACTIVE_ENUM.keys())
    parser.add_argument("-s", "--start-date", type=valid_date_in_future,
                        help="Date when next task of plan will be created")
    parser.add_argument("-r", "--repeat-type", choices=me.STRING_TO_REPETITION_TYPE_ENUM.keys(),
                        help="Specifying time measure for plan")
    parser.add_argument("-I", "--interval", type=positive_integer,
                        help="Interval of measured units defined in --repeat-type")
    parser.add_argument("-e", "--end-type", choices=me.STRING_TO_PLAN_COMPLETION_CRITERIA_ENUM.keys(),
                        help="Specify end type of plan")
    parser.add_argument("-d", "--end-date", type=valid_date_in_future,
                        help="Plan will be deactivated after this date")
    parser.add_argument("-n", "--repetitions-number", type=positive_integer,
                        help="Plan will be deactivated after template task was created specified number of times")


def initialize_update_condition_relation(parser):
    parser.add_argument("-i", "--relation-id", type=positive_integer, required=True,
                        help="Id of the relation you want to update")
    parser.add_argument("-s", "--trigger-task-status", required=True,
                        choices=me.STRING_TO_UPDATABLE_TASK_STATUSES.keys(),
                        help="Status of a trigger task, on which condition task will be created")


def initialize_update_subtask_relation(parser):
    parser.add_argument("-i", "--relation-id", type=positive_integer, required=True,
                        help="Id of the relation you want to update")
    parser.add_argument("-t", "--relation-type", required=True, choices=me.STRING_TO_TASK_RELATION_ENUM.keys(),
                        help="New relation type between parent task and subtask")


def initialize_update_date_parser(parser):
    parser.add_argument("-i", "--date-id", required=True, type=positive_integer,
                        help="Id of date to be updated")
    parser.add_argument("-d", "--date", type=valid_date,
                        help="Date in format YYYY-MM-DD")


def initialize_update_group_parser(parser):
    parser.add_argument("-i", "--group-id", required=True, type=positive_integer,
                        help="Id of group you want to update")
    parser.add_argument("-t", "--title", required=True, type=str,
                        help="New group title")


def initialize_update_reminder_parser(parser):
    parser.add_argument("-i", "--reminder-id", required=True, type=positive_integer,
                        help="Id of reminder you want to update")
    parser.add_argument("-d", "--start-date", type=valid_date_in_future,
                        help="Next date to remind you")
    parser.add_argument("-t", "--time", type=valid_time,
                        help="Time in format HH:MM when you will be reminded")
    parser.add_argument("-D", "--days-interval", type=positive_integer,
                        help="Interval in days with which you will be informed")
    parser.add_argument("-a", "--active", choices=me.STRING_TO_ACTIVE_ENUM.keys(),
                        help="You can activate of deactivate reminder")


def initialize_update_notification_parser(parser):
    parser.add_argument("-i", "--notification-id", required=True, type=positive_integer,
                        help="Id of notification you want to update")
    parser.add_argument("-t", "--time", type=valid_time,
                        help="Time in format HH:MM when you will be reminded")
    parser.add_argument("-d", "--days", type=positive_integer,
                        help="Interval in days with which you will be informed")
    parser.add_argument("-u", "--used", choices=me.STRING_TO_USED_ENUM.keys())

# endregion


def valid_date(s):
    try:
        return datetime.strptime(s, "%Y-%m-%d").date()
    except ValueError:
        msg = "Not a valid date: '{0}'.".format(s)
        raise argparse.ArgumentTypeError(msg)


def valid_date_in_future(s):
    try:
        date = datetime.strptime(s, "%Y-%m-%d").date()
    except ValueError:
        msg = "Not a valid date: '{0}'.".format(s)
        raise argparse.ArgumentTypeError(msg)
    if date < datetime.now().date():
        raise argparse.ArgumentTypeError("Not a valid date {}. It should be in the future".format(s))
    return date


def valid_time(s):
    try:
        return datetime.strptime(s, "%H:%M").time()
    except ValueError:
        msg = "Not a valid time: {}.".format(s)
        raise argparse.ArgumentTypeError(msg)


def positive_integer(i):
    try:
        i = int(i)
        if i > 0:
            return i
        else:
            raise ValueError()
    except ValueError:
        msg = "Not a positive integer: {}.".format(i)
        raise argparse.ArgumentTypeError(msg)
