import planner4u.urls
from planner4u import views
from django.contrib.auth import views as auth_views
from django.contrib.auth.views import LoginView, LogoutView
from django.conf.urls import url, include


app_name = "planner4u"

urlpatterns = [
    url(r"^ministry/$", views.ministry, name="ministry"),


    url(r"^bhp-home/$", views.bhp_home, name="bhp-home"),
    url(r"^bif-home/$", views.bif_home, name="bif-home"),
    url(r"^home/$", views.home, name="home"),
    url(r"^wesi-home/$", views.wesi_home, name="wesi-home"),
    url(r"^wk-home/$", views.wk_home, name="wk-home"),
    url(r"^wl-home/$", views.wl_home, name="wl-home"),
    url(r"^wnpp-home/$", views.wnpp_home, name="wnpp-home"),
    url(r"^woa-home/$", views.woa_home, name="woa-home"),
    url(r"^wrz-home/$", views.wrz_home, name="wrz-home"),
    url(r"^wzn-home/$", views.wzn_home, name="wzn-home"),
    url(r"^wzp-home/$", views.wzp_home, name="wzp-home"),
    url(r"^zp-home/$", views.zp_home, name="zp-home"),
    url(r"^zuit-home/$", views.zuit_home, name="zuit-home"),

    url(r"^gkm-home/$", views.gkm_home, name="gkm-home"),
    url(r"^wb-home/$", views.wb_home, name="wb-home"),
    url(r"^wk2-home/$", views.wk2_home, name="wk2-home"),
    url(r"^wp-home/$", views.wp_home, name="wp-home"),
    url(r"^wrp-home/$", views.wrp_home, name="wrp-home"),

    url(r"^a-home/$", views.a_home, name="a-home"),
    url(r"^iod-home/$", views.iod_home, name="iod-home"),
    url(r"^polis-home/$", views.polis_home, name="polis-home"),
    url(r"^rl-home/$", views.rl_home, name="rl-home"),
    url(r"^rll-home/$", views.rll_home, name="rll-home"),
    url(r"^za-home/$", views.za_home, name="za-home"),
    url(r"^zswp-home/$", views.zswp_home, name="zswp-home"),

    url(r"^p-home/$", views.p_home, name="p-home"),
    url(r"^ssp-home/$", views.ssp_home, name="ssp-home"),
    url(r"^wok-home/$", views.wok_home, name="wok-home"),
    url(r"^za1-home/$", views.za1_home, name="za1-home"),
    url(r"^zklz-home/$", views.zklz_home, name="zklz-home"),
    url(r"^zp1-home/$", views.zp1_home, name="zp1-home"),

    url(r"^gkr-home/$", views.gkr_home, name="gkr-home"),
    url(r"^wbrl-home/$", views.wbrl_home, name="wbrl-home"),
    url(r"^wesll-home/$", views.wesll_home, name="wesll-home"),
    url(r"^wpr-home/$", views.wpr_home, name="wpr-home"),
    url(r"^wsa-home/$", views.wsa_home, name="wsa-home"),

    url(r"^wee-home/$", views.wee_home, name="wee-home"),
    url(r"^wesi-home/$", views.wesi_home, name="wesi-home"),
    url(r"^wkic-home/$", views.wkic_home, name="wkic-home"),

    url(r"^we-home/$", views.we_home, name="we-home"),
    url(r"^weg-home/$", views.weg_home, name="weg-home"),

    url(r"^wij-home/$", views.wij_home, name="wij-home"),
    url(r"^wis-home/$", views.wis_home, name="wis-home"),
    url(r"^wsr-home/$", views.wsr_home, name="wsr-home"),

    url(r"^waf-home/$", views.waf_home, name="waf-home"),
    url(r"^wep-home/$", views.wep_home, name="wep-home"),
    url(r"^wkm-home/$", views.wkm_home, name="wkm-home"),

    url(r"^sok-home/$", views.sok_home, name="sok-home"),
    url(r"^wg-home/$", views.wg_home, name="wg-home"),
    url(r"^wr-home/$", views.wr_home, name="wr-home"),
    url(r"^wrd-home/$", views.wrd_home, name="wrd-home"),
    url(r"^wrh-home/$", views.wrh_home, name="wrh-home"),
    url(r"^wte-home/$", views.wte_home, name="wte-home"),

    url(r"^wfn-home/$", views.wfn_home, name="wfn-home"),
    url(r"^wip-home/$", views.wip_home, name="wip-home"),
    url(r"^wkf-home/$", views.wkf_home, name="wkf-home"),
    url(r"^wksn-home/$", views.wksn_home, name="wksn-home"),
    url(r"^wkw-home/$", views.wkw_home, name="wkw-home"),
    url(r"^wm-home/$", views.wm_home, name="wm-home"),
    url(r"^wn-home/$", views.wn_home, name="wn-home"),
    url(r"^wnsr-home/$", views.wnsr_home, name="wnsr-home"),
    url(r"^wpe-home/$", views.wpe_home, name="wpe-home"),
    url(r"^wprpt-home/$", views.wprpt_home, name="wprpt-home"),
    url(r"^ww-home/$", views.ww_home, name="ww-home"),
    url(r"^wz-home/$", views.wz_home, name="wz-home"),
    url(r"^wzf-home/$", views.wzf_home, name="wzf-home"),

    url(r"^f-home/$", views.f_home, name="f-home"),
    url(r"^whg-home/$", views.whg_home, name="whg-home"),
    url(r"^wig-home/$", views.wig_home, name="wig-home"),
    url(r"^wks-home/$", views.wks_home, name="wks-home"),
    url(r"^ww2-home/$", views.ww2_home, name="ww2-home"),
    url(r"^zdk-home/$", views.zdk_home, name="zdk-home"),
    url(r"^zoe-home/$", views.zoe_home, name="zoe-home"),
    url(r"^zol-home/$", views.zol_home, name="zol-home"),

    url(r"^bdo-home/$", views.bdo_home, name="bdo-home"),
    url(r"^ok-home/$", views.ok_home, name="ok-home"),
    url(r"^op-home/$", views.op_home, name="op-home"),
    url(r"^opz-home/$", views.opz_home, name="opz-home"),
    url(r"^po-home/$", views.po_home, name="po-home"),
    url(r"^sp-home/$", views.sp_home, name="sp-home"),
    url(r"^ssop-home/$", views.ssop_home, name="ssop-home"),

    url(r"^l-home/$", views.l_home, name="l-home"),
    url(r"^ll-home/$", views.ll_home, name="ll-home"),
    url(r"^lll-home/$", views.lll_home, name="lll-home"),
    url(r"^lv-home/$", views.lv_home, name="lv-home"),
    url(r"^v-home/$", views.v_home, name="v-home"),

    url(r"^wgl-home/$", views.wgl_home, name="wgl-home"),
    url(r"^wko-home/$", views.wko_home, name="wko-home"),
    url(r"^wl-home/$", views.wl_home, name="wl-home"),
    url(r"^wmm-home/$", views.wmm_home, name="wmm-home"),
    url(r"^wnl-home/$", views.wnl_home, name="wnl-home"),
    url(r"^wopl-home/$", views.wopl_home, name="wopl-home"),
    url(r"^wsl-home/$", views.wsl_home, name="wsl-home"),

    url(r"^bo-home/$", views.bo_home, name="bo-home"),
    url(r"^m-home/$", views.m_home, name="m-home"),
    url(r"^oz-home/$", views.oz_home, name="oz-home"),
    url(r"^wop-home/$", views.wop_home, name="wop-home"),
    url(r"^wpn-home/$", views.wpn_home, name="wpn-home"),
    url(r"^wps-home/$", views.wps_home, name="wps-home"),

    url(r"^gmo-home/$", views.gmo_home, name="gmo-home"),
    url(r"^wk1-home/$", views.wk1_home, name="wk1-home"),
    url(r"^wos-home/$", views.wos_home, name="wos-home"),
    url(r"^wpn1-home/$", views.wpn1_home, name="wpn1-home"),
    url(r"^zf-home/$", views.zf_home, name="zf-home"),
    url(r"^zp2-home/$", views.zp2_home, name="zp2-home"),

    url(r"^l1-home/$", views.l1_home, name="l1-home"),
    url(r"^ll2-home/$", views.ll2_home, name="ll2-home"),
    url(r"^lll3-home/$", views.lll3_home, name="lll3-home"),
    url(r"^lv4-home/$", views.lv4_home, name="lv4-home"),

    url(r"^wop1-home/$", views.wop1_home, name="wop1-home"),
    url(r"^wop2-home/$", views.wop2_home, name="wop2-home"),
    url(r"^woplll-home/$", views.woplll_home, name="woplll-home"),
    url(r"^woplv-home/$", views.woplv_home, name="woplv-home"),
    url(r"^wopv-home/$", views.wopv_home, name="wopv-home"),
    url(r"^wopvl-home/$", views.wopvl_home, name="wopvl-home"),

    url(r"^we1-home/$", views.we1_home, name="we1-home"),
    url(r"^wep1-home/$", views.wep1_home, name="wep1-home"),
    url(r"^wjp-home/$", views.wjp_home, name="wjp-home"),
    url(r"^wma-home/$", views.wma_home, name="wma-home"),

    url(r"^wism-home/$", views.wism_home, name="wism-home"),
    url(r"^wjpt-home/$", views.wjpt_home, name="wjpt-home"),
    url(r"^wrrn-home/$", views.wrrn_home, name="wrrn-home"),
    url(r"^zopp-home/$", views.zopp_home, name="zopp-home"),

    url(r"^ue-home/$", views.ue_home, name="ue-home"),
    url(r"^wd-home/$", views.wd_home, name="wd-home"),
    url(r"^ww1-home/$", views.ww1_home, name="ww1-home"),

    url(r"^czk-home/$", views.czk_home, name="czk-home"),
    url(r"^ktmk-home/$", views.ktmk_home, name="ktmk-home"),
    url(r"^poin-home/$", views.poin_home, name="poin-home"),
    url(r"^wcb-home/$", views.wcb_home, name="wcb-home"),
    url(r"^wrs-home/$", views.wrs_home, name="wrs-home"),
    url(r"^wso-home/$", views.wso_home, name="wso-home"),
    url(r"^wzk-home/$", views.wzk_home, name="wzk-home"),
    url(r"^zb-home/$", views.zb_home, name="zb-home"),

    url(r"^tk-home/$", views.tk_home, name="tk-home"),
    url(r"^wiz-home/$", views.wiz_home, name="wiz-home"),
    url(r"^wjre-home/$", views.wjre_home, name="wjre-home"),
    url(r"^wnm-home/$", views.wnm_home, name="wnm-home"),
    url(r"^wpe1-home/$", views.wpe1_home, name="wpe1-home"),
    url(r"^wze-home/$", views.wze_home, name="wze-home"),
    url(r"^zpe-home/$", views.zpe_home, name="zpe-home"),

    url(r"^report/$", views.report, name="report"),
    url(r"^index/$", views.index, name="index"),

    url(r"^origin/$", views.origin, name="origin"),


    url(r"^bdg-department/$", views.bdgdepartment, name="bdg-department"),
    url(r"^bf-department/$", views.bfdepartment, name="bf-department"),
    url(r"^bka-department/$", views.bkadepartment, name="bka-department"),
    url(r"^bm-department/$", views.bmdepartment, name="bm-department"),
    url(r"^db-department/$", views.dbgdepartment, name="db-department"),
    url(r"^dc-department/$", views.dcdepartment, name="dc-department"),
    url(r"^deg-department/$", views.degdepartment, name="deg-department"),
    url(r"^dej-department/$", views.dejdepartment, name="dej-department"),
    url(r"^dek-department/$", views.dekdepartment, name="dek-department"),
    url(r"^delg-department/$", views.delgdepartment, name="delg-department"),
    url(r"^dfe-department/$", views.dfedepartment, name="dfe-department"),
    url(r"^dgk-department/$", views.dgkdepartment, name="dgk-department"),
    url(r"^dgo-department/$", views.dgodepartment, name="dgo-department"),
    url(r"^dis-department/$", views.disdepartment, name="dis-department"),
    url(r"^dll-department/$", views.dlldepartment, name="dll-department"),
    url(r"^dngs-department/$", views.dngsdepartment, name="dngs-department"),
    url(r"^dop-department/$", views.dopdepartment, name="dop-department"),
    url(r"^doze-department/$", views.dozedepartment, name="doze-department"),
    url(r"^dp-department/$", views.dpdepartment, name="dp-department"),
    url(r"^dpm-department/$", views.dpmdepartment, name="dpm-department"),
    url(r"^drp-department/$", views.drpdepartment, name="drp-department"),
    url(r"^dsm-department/$", views.dsmdepartment, name="dsm-department"),
    url(r"^dso-department/$", views.dsodepartment, name="dso-department"),
    url(r"^dsp-department/$", views.dspdepartment, name="dsp-department"),
    url(r"^invoice/$", views.GeneratePdf.as_view()),


    url(r"^bdghome/$", views.BDGhome, name="bdghome"),

    #url(r"^create_task_bdg/$", views.create_task_bdg, name="create_task_bdg"),

    url(r'^task/', include('planner4u.urlpatterns.task_urls')),
    url(r'^group/', include('planner4u.urlpatterns.group_urls')),
    url(r'^date/', include('planner4u.urlpatterns.date_urls')),
    url(r'^plan/', include('planner4u.urlpatterns.plan_urls')),
    url(r'^condition/', include('planner4u.urlpatterns.condition_urls')),
    url(r'^ajax/', include('planner4u.urlpatterns.api_urls')),

    url(r"^register/$", views.register, name="register"),
    #url(r'^login/$', auth_views.LoginView.as_view(), {"template_name": "planner4u/registration/login.html"}, name='login'),
    url(r"login/$", LoginView.as_view(template_name="planner4u/registration/login.html"),name="login"),
    #url(r'^logout/$', auth_views.LogoutView.as_view(), {"next_page": "/planner4u/login/"}, name='logout'),
    url(r"^login/$", LogoutView.as_view(template_name="planner4u/registration/login.html"), name="logout"),
]
