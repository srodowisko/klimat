from django.conf.urls import url
from planner4u import views

urlpatterns = [
    url(r"^create/$", views.create_condition, name="create_condition"),
]
