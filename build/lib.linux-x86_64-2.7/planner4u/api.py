from planner4u import get_service
from planner4ucore.models import models as mo
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_protect
from planner4ucore.services import exceptions as ex


# region AJAX handlers
def get_task_groups(request):
    username = request.GET.get("username")
    task_id = request.GET.get("task_id", None)
    user_groups = {g.group_id: g.title for g in get_service().get_filtered_groups(user_creator_id=username)}
    task_groups = {g.group_id: g.title for g in get_service().get_filtered_groups(user_creator_id=username,
                                                                                  task_id=task_id)}
    return JsonResponse({"task": task_groups, "user": user_groups})


def get_tasks_for_group(request):
    username = request.GET.get("username")
    group_id = int(request.GET.get("group_id"))

    user_tasks = get_service().get_user_tasks(user_id=username)
    executable_tasks = get_service().get_filtered_tasks(user_executor_id=username)
    user_tasks.extend(executable_tasks)
    user_tasks = {t.task_id: t.title for t in user_tasks}

    group_tasks = get_service().get_group_tasks(user_id=username, group_id=group_id)
    group_tasks = {t.task_id: t.title for t in group_tasks}
    return JsonResponse({"user_tasks": user_tasks, "group_tasks": group_tasks})


@csrf_protect
def modify_task_group_relation(request):
    username = request.POST.get("username")
    task_id = int(request.POST.get("task_id"))
    group_id = int(request.POST.get("group_id"))
    add_relation = request.POST.get("add_relation", None) == "true"

    if add_relation:
        relation = mo.TaskGroupRelation(task_id, group_id)
        get_service().add_task_group_relation(user_id=username, task_group_relation=relation)
    else:
        get_service().delete_task_group_relation(user_id=username, task_id=task_id, group_id=group_id)

    return JsonResponse({"successful": True})


def get_subtasks(request):
    username = request.GET.get("username")
    task_id = int(request.GET.get("task_id"))
    relations = []
    tasks = []

    subtask_relations = get_service().get_subtask_relations(user_id=username, task_id=task_id)

    for r, t in subtask_relations:
        relations.append(r)
        tasks.append(t)

    relations = mo.get_representative_models(relations, mo.SUBTASK_RELATION_ENUM_TRANSFORMER)

    response = []
    for relation, task in zip(relations, tasks):
        object_dict = dict()
        object_dict["child_id"] = task.task_id
        object_dict["child_title"] = task.title
        object_dict["number_of_subtasks"] = len(get_service().get_subtasks(user_id=username, task_id=task.task_id))
        object_dict["relation_type"] = relation.relation_type
        response.append(object_dict)

    return JsonResponse(response, safe=False)


@csrf_protect
def add_subtask_relation(request):
    username = request.POST.get("username")
    task_id = int(request.POST.get("task_id"))
    subtask_id = int(request.POST.get("subtask_id"))

    relation = mo.SubtaskRelation(parent_task_id=task_id, sub_task_id=subtask_id)

    try:
        get_service().add_subtask_relation(user_id=username, subtask_relation=relation)
    except ex.Planner4UError as e:
        return JsonResponse({"message": str(e), "added": False})
    return JsonResponse({"message": "Relation added successfully!", "added": True})


@csrf_protect
def add_executor_relation(request):
    username = request.POST.get("username")
    task_id = int(request.POST.get("task_id"))
    # relation_type = int(request.POST.get("relation_type"))
    user_id = int(request.POST.get("user_id"))

    relation = mo.UserExecutorRelation(user_executor_id=user_id, task_id=task_id)
    try:
        get_service().add_user_executor_relation(user_id=username, user_executor_relation=relation)
    except ex.Planner4UError as e:
        return JsonResponse({"message": str(e), "added": False})
    return JsonResponse({"message": "Relation added successfully!", "added": True})


@csrf_protect
def modify_subtask_relation(request):
    username = request.POST.get("username")
    task_id = int(request.POST.get("task_id"))
    child_id = int(request.POST.get("child_id"))
    add_relation = request.POST.get("add_relation", None) == "true"

    if add_relation:
        relation = mo.SubtaskRelation(parent_task_id=task_id, sub_task_id=child_id)
        get_service().add_subtask_relation(user_id=username, subtask_relation=relation)
    else:
        get_service().delete_subtask_relation(user_id=username, parent_task_id=task_id, sub_task_id=child_id)

    return JsonResponse({"successful": True})


def get_users(request):
    from_user = int(request.GET.get("from_user"))
    number = int(request.GET.get("delta"))
    username_pattern = request.GET.get("username_pattern")

    users = get_service().get_filtered_users(start_from=from_user, end_by=from_user+number,
                                             username_pattern=username_pattern)

    users = [{"user_id": u.user_id, "username": u.name} for u in users]
    return JsonResponse(users, safe=False)


def get_executors(request):
    username = request.GET.get("username")
    task_id = int(request.GET.get("task_id"))

    executors = get_service().get_users_executors(user_id=username, task_id=task_id)
    executors = [{"user_id": e.user_id, "username": e.name} for e in executors]
    return JsonResponse(executors, safe=False)


@csrf_protect
def modify_executor_relation(request):
    username = request.POST.get("username")
    task_id = int(request.POST.get("task_id"))
    user_id = int(request.POST.get("user_id"))
    add_relation = request.POST.get("add_relation", None) == "true"

    if add_relation:
        relation = mo.UserExecutorRelation(user_executor_id=user_id, task_id=task_id)
        get_service().add_user_executor_relation(user_id=username, user_executor_relation=relation)
    else:
        get_service().delete_user_executor_relation(user_id=username, task_id=task_id, executor_id=user_id)

    return JsonResponse({"successful": True})

from planner4ucore.models import models as mo
from planner4ucore.models import enums as me

#TASK_STATUS_ENUM_TO_STRING = reverse_dictionary(TASK_STATUS_ENUM_TO_STRINGTASK_STATUS_ENUM_TO_STRING)

def get_tasks(request):
    from_task = int(request.GET.get("from_task"))
    number = int(request.GET.get("delta"))
    title_pattern = request.GET.get("title_pattern")
    username = request.GET.get("username")
    tasks = get_service().get_filtered_tasks(user_creator_id=username, start_from=from_task,
                                             end_by=from_task+number, title_pattern=title_pattern)

    tasks = [{"task_id": t.task_id, "title": t.title, "description": t.description, "status": t.status, "priority_on_overdue": t.priority_on_overdue } for t in tasks]
    return JsonResponse(tasks, safe=False)
# endregion
