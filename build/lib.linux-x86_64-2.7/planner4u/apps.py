from django.apps import AppConfig


class Planner4UConfig(AppConfig):
    name = 'planner4u'
