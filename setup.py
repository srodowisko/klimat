from setuptools import setup, find_packages

setup(
    name="Planner4UTerminal",
    description="Console interface for Planner4U",
    version="2.3",
    packages=find_packages(),
    entry_points={
        "console_scripts":
            ["pu = terminal.main:main"]
    }
)
