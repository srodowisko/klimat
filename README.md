# Planner4U Home

### Web application
Skopiuj repozytorium:

```bash
user$ git clone git@bitbucket.org:srodowisko/min.klimatu.git
```
```bash
python3 manage.py runserver
```

### Terminal application

Aplikacja zapewnia wygodny i zlokalizowany interfejs konsoli do pracy z powyższymi funkcjami. 

**Jak zainstalować program:**

Przejdź do katalogu Desktop i zainstaluj program:

```bash
user$ python3 setup.py install
```
**_Uwaga_! ** Przed pierwszym użyciem uruchom polecenie init z parametrami połączenia z bazą danych (_MySQL, PostgreSQL, Oracle, Microsoft SQL Server, SQLite_) i nazwą użytkownika. Dostęp do programu uzyskuje się za pomocą polecenia **_pu_**.

```bash
user$ pu init --connection-string="db://USERNAME:PASSWORD@HOST:PORT/DB_NAME" 
--username="miroslav"
Database was set up
User was added: miroslav (id 1)
```
Następnie umieść plik konfiguracyjny planner4uconfig.json w swoim katalogu domowym.

``` json
{
    // Your user id from database or just username
	"user id": 1,
	"connection string" : "db://USERNAME:PASSWORD@HOST:PORT/DB_NAME",
    // Loggin details
	"log output": {
		"file":{
			"level": "debug",
			"file path": "file/path/to/write/log"
		},
		"terminal":{
			"level": "critical"	
		}
	}
	
}
```

Gratulacje! Teraz możesz uruchomić program za pomocą polecenia **_pu_**!

**Przykład użycia**

``` bash
user$ pu task add -t "Have a good day"
------ Task was created: 
  Have a good day
 Task id: 6758
 Type: simple
 Description: -
 Created on: 2021-06-07
 Priority: regular
 Status: created
 Close on last subtask execution: no
 Priority on overdue: regular
 
user$ pu tasks
------ Creator: 
 3 - Prepare for exam (simple, executed)
 14 - Perpare documents for Department of Analyzes (simple, work)
 6758 - Have a good day (simple, created)
 
user$ pu task update -i 6758 --status overdue
------ Task was updated:
  Have a good day
 Task id: 6758
 Type: simple
 Description: -
 Created on: 2021-06-07
 Priority: regular
 Status: overdue
 Close on last subtask execution: no
 Priority on overdue: regular
```



### Library 

Biblioteka znajduje się w katalogu Library tego repozytorium i jest interfejsem API do pracy z aplikacją.  

**Instalacja biblioteki:**

Skopiuj repozytorium:

``` bash 
user$ git clone git clone git@bitbucket.org:srodowisko/min.klimatu.git
```
Przejdź do katalogu Library i zainstaluj moduł biblioteki:

``` bash
user$ cd Library
user$ python3 setup.py install
```
Istnieje możliwość uruchomić testy bibliotek:

``` bash
user$ python3 setup.py test
```
**Jak korzystać z biblioteki::**

1. Zaimportuj moduł biblioteki
2. Zaimportuj moduł o tej samej nazwie z pakietu _services_.
3. Pakiet _storage_ zawiera moduły implementacji DAL dla różnych typów pamięci, wybierz moduł, który Ci odpowiada i zaimportuj go
4. Korzystając z zaimportowanego modułu, utwórz _data storage class_ 
5. Zaimportuj klasę _Service_ z modułu usług
6. Utwórz obiekt klasy Service, podając _data storage class_ 

**Przykład wykorzystania biblioteki:**

``` python
>>> import planner4ucore
>>> from planner4ucore.services import services
>>> from planner4ucore.storage import sqlalchemy_storage
>>> connection_string = "mysql://USERNAME:PASSWORD@HOST:PORT/DB_NAME?charset=utf8"
>>> data_storage = sqlalchemy_storage.SQLAlchemyStorage(connection_string)
>>> service = services.Service(data_storage)
>>> from planner4ucore.models import models as mo
>>> user = service.add_user(mo.User("miroslav"))
>>> task = service.add_task(mo.Task(user.user_id, "Eat, pray, love, code"))
>>> service.get_user_tasks(user.user_id)[0].title
'Eat, pray, love, code'
```

### Contacts

miroslaw.haniewicz@mos.gov.pl / miroslavadmiral@gmail.com

