from django import forms
from planner4ucore.models import enums as me
from datetime import datetime
from django.forms import ValidationError, PasswordInput, EmailInput

from django.contrib.auth.models import User

from django.contrib.auth import get_user_model
from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.tokens import default_token_generator
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from django.conf import settings
from django.template.loader import render_to_string

class SomeForm(forms.Form):

    picked = forms.ChoiceField(choices=me.TASK_PRIORITY_ENUM_TO_STRING.items())

class MainForm(forms.Form):
    start_date = forms.DateField(widget=forms.SelectDateWidget, required=False)
    end_date = forms.DateField(widget=forms.SelectDateWidget, required=False)

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date

class TaskFormBDG(forms.Form):
    title = forms.CharField(label="Task title", max_length=50, required=True)
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(),
                               required=True, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items())
    priority_on_overdue = forms.ChoiceField(choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1)
    description = forms.CharField(widget=forms.Textarea, required=False, max_length=500)

    start_date = forms.DateField(widget=forms.SelectDateWidget, required=True)
    end_date = forms.DateField(widget=forms.SelectDateWidget, required=True)

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date

default_errors = {
    "required": "",
    "invalid": ""
}

class TaskForm(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=0, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=109, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormBHP(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=0, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=2, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormBIF(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=0, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=3, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormWESI(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=0, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=50, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormWK(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=0, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=64, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormWL(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=0, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=74, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormWNPP(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=0, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=82, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormWOA(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=0, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=84, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormWRZ(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=0, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=110, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormWZN(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=0, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=123, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormWZP(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=0, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=124, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormZP(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=0, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=134, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormZUIT(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=0, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=139, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormGKM(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=1, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=7, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormWB(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=1, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=39, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormWK2(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=1, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=66, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormWP(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=1, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=95, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormWRP(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=1, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=106, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormA(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=2, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=0, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormIOD(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=2, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=16, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormPOLIS(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=2, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=27, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormRL(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=2, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=29, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormRLL(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=2, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=30, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormZA(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=2, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=125, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormZSWP(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=2, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=138, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormP(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=3, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=25, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormSSP(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=3, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=34, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormWOK(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=3, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=85, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormZA1(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=3, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=126, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormZKLZ(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=3, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=130, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormZP1(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=3, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=135, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormGKR(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=4, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=8, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormWBRL(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=4, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=40, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormWESLL(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=4, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=50, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormWPR(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=4, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=100, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormWSA(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=4, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=111, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormWEE(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=5, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=46, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormWESI(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=5, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=50, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors,  initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormWKIC(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=5, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=68, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormWE(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=6, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=44, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormWEG(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=6, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=47, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormWIJ(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=7, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=56, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormWIS(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=7, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=58, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormWSR(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=7, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=114, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormWAF(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=8, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=38, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormWEP(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=8, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=48, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormWKM(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=8, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=69, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date



class TaskFormSOK(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=9, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=31, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormWG(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=9, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=52, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormWR(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=9, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=103, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormWRD(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=9, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=104, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormWRH(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=9, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=105, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormWTE(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=9, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=115, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormWFN(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=10, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=51, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormWIP(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=10, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=57, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormWKF(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=10, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=67, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormWKSN(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=10, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=72, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormWKW(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=10, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=73, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormWM(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=10, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=76, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormWN(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=10, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=79, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormWNSR(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=10, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=83, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormWPE(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=10, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=96, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormWPRPT(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=10, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=101, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormWW(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=10, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=116, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormWZ(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=10, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=119, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormWZF(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=10, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=121, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormF(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=11, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=6, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormWHG(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=11, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=54, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormWIG(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=11, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=55, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormWKS(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=11, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=71, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormWW2(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=11, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=118, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormZDK(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=11, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=128, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormZOE(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=11, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=131, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormZOL(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=11, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=132, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormBDO(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=12, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=1, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormOK(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=12, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=21, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormOP(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=12, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=22, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormOPZ(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=12, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=23, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormPO(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=12, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=26, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormSP(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=12, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=32, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormSSOP(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=12, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=33, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormL(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=13, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=10, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormLL(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=13, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=12, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormLLL(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=13, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=14, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormLV(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=13, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=17, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormV(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=13, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=37, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date

'''
class TaskFormV(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=0, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=109, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=False)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, widget=forms.SelectDateWidget(attrs={"class": "date"}), required=False)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date
'''

class TaskFormWGL(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=14, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=53, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormWKO(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=14, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=70, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormW_L(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=14, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=75, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormWMM(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=14, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=78, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormWNL(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=14, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=80, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormWOPL(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=14, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=91, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormWSL(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=14, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=112, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormBO(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=15, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=4, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormM(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=15, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=20, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormOZ(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=15, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=24, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormWOP(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=15, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=86, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormWPN(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=15, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=98, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormWPS(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=15, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=102, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormGMO(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=16, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=9, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormWK1(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=16, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=65, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormWOS(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=16, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=94, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormWPN1(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=16, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=99, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormZF(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=16, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=129, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormZP2(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=16, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=136, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormL1(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=17, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=11, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormLL2(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=17, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=13, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormLLL3(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=17, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=15, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormLV4(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=17, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=18, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormWOP1(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=18, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=87, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormWOP2(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=18, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=88, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date

'''
class TaskFormWOPLL(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=0, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=89, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=False)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, widget=forms.SelectDateWidget(attrs={"class": "date"}), required=False)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date
'''

class TaskFormWOPLLL(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=18, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=89, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormWOPLV(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=18, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=90, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormWOPV(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=17, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=92, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormWOPVL(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=18, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=93, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormWE1(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=19, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=45, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormWEP1(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=19, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=49, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormWJP(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=19, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=61, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormWMA(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=19, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=77, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormWISM(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING, initial=20, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=59, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormWJPT(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=20, widget=forms.Select())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=62, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormWRRN(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=20, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=107, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormZOPP(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=20, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=133, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormUE(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=21, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=36, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormWD(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=21, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=43, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormWW1(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=21, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=117, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormCZK(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=22, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=5, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormKTMK(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=22, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=19, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormPOIN(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=22, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=28, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormWCB(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=22, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=42, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormWRS(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=22, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=108, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormWSO(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=22, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=113, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormWZK(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=22, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=122, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormZB(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=22, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=127, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormTK(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=23, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=35, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormWIZ(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=23, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=60, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormWJRE(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=23, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=63, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormWNM(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=23, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=81, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormWPE1(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=23, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=97, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormWZE(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=23, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=120, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={ "class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class TaskFormZPE(forms.Form):
    title = forms.CharField(label="Tytuł", max_length=213, required=True, error_messages=default_errors, widget=forms.TextInput(attrs={"class": "title"}))
    status = forms.ChoiceField(choices=me.TASK_STATUS_ENUM_TO_STRING.items(), initial=23, widget=forms.HiddenInput())
    priority = forms.ChoiceField(label="Wydział", choices=me.TASK_PRIORITY_ENUM_TO_STRING.items(), initial=137, widget=forms.HiddenInput())
    priority_on_overdue = forms.ChoiceField(label="Rodzaj zadania", choices=me.TASK_TWO_ENUM_TO_STRING.items(), initial=1, widget=forms.Select(attrs={"class": "priority_on_overdue"}))
    description = forms.CharField(label="Opis zadania", error_messages=default_errors, widget=forms.Textarea(attrs={"class": "description"}), required=True, max_length=5500)

    start_date = forms.DateField(label="Data początkowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)
    end_date = forms.DateField(label="Data końcowa", error_messages=default_errors, initial=datetime.now(), widget=forms.SelectDateWidget(attrs={"class": "date"}), required=True)

    error_css_class = "error"

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date


class UserForm(forms.Form):
    first_name= forms.CharField(max_length=100)
    last_name= forms.CharField(max_length=100)
    email= forms.EmailField()


class DateForm(forms.Form):
    date_type = forms.ChoiceField(choices=me.DATE_TYPE_ENUM_TO_STRING.items(),
                                  initial=me.DateType.START.value, required=False, error_messages=default_errors, widget=forms.Select(attrs={"class": "select"}))
    date = forms.DateField(widget=forms.SelectDateWidget(attrs={"class": "date"}))

    error_css_class = "error"

class GroupForm(forms.Form):
    title = forms.CharField(label="Task title", max_length=50, required=True)


class PlanForm(forms.Form):
    create_task_date = forms.DateField(widget=forms.SelectDateWidget, required=False, initial=datetime.now().date(),
                                       label="Template task will be created on")
    repeat_type = forms.ChoiceField(choices=me.REPETITION_TYPE_ENUM_TO_STRING.items(),
                                    initial=me.RepetitionType.DAILY.value)
    plan_interval = forms.IntegerField(min_value=1, widget=forms.NumberInput(), initial=1)
    end_type = forms.ChoiceField(choices=me.PLAN_COMPLETION_CRITERIA_ENUM_TO_STRING.items(), label="End by",
                                 initial=me.PlanCompletionCriteria.NEVER.value, widget=forms.RadioSelect())
    # Save order:end_type -> end_date -> repetitions_number
    end_date = forms.DateField(widget=forms.SelectDateWidget, required=False)
    repetitions_number = forms.IntegerField(widget=forms.NumberInput(), required=False, min_value=1)
    active = forms.ChoiceField(choices=me.ACTIVE_ENUM_TO_STRING.items(), required=False)

    def clean_create_task_date(self):
        form_data = self.cleaned_data
        date = form_data.get("create_task_date", None)
        if date is not None:
            if date < datetime.now().date():
                raise ValidationError("Next creation date of template task can't be in the past")
        return date

    def clean_end_date(self):
        form_data = self.cleaned_data
        end_type = form_data.get("end_type", None)
        end_type = int(end_type) if end_type else None
        end_date = form_data.get("end_date", None)
        if end_type is not None:
            if end_type == me.PlanCompletionCriteria.AFTER_DATE.value and end_date is None:
                raise ValidationError("Please, specify date after which plan will be turned off")

        if end_date is not None:
            start_date = form_data.get("create_task_date")
            if start_date > end_date:
                raise ValidationError("End date of plan can't be earlier than the start date")

        return end_date

    def clean_repetitions_number(self):
        form_data = self.cleaned_data
        end_type = form_data.get("end_type", None)
        end_type = int(end_type) if end_type else None
        repetitions_number = form_data.get("repetitions_number", None)
        if end_type is not None:
            if end_type == me.PlanCompletionCriteria.AFTER_NUMBER_OF_TIMES.value and repetitions_number is None:
                raise ValidationError("Please, specify number of creations after which plan will be turned off")
        return repetitions_number


class TasksSelectionForm(forms.Form):
    tasks = forms.MultipleChoiceField(widget=forms.CheckboxSelectMultiple, required=False)
