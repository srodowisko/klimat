"""
    This package contains all necessary  modules and packages
    to set up terminal interface for Planner4U application

    Modules:
        initializations - responsible for terminal arguments initializations
        handlers - responsible for terminal argument handling
        main - entry point, configures settings

    Packages:
        representations - package for objects representation

"""