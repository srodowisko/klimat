"""
    This module contains functions responsible for
    printing models objects. Almost each function
    maintenance detailed and oneline output.

    Functions:
        show_task
        show_condition_relation
        show_date
        show_group
        show_user
        show_reminder
        show_notification
        show_task_group_relation
        show_user_executor_relation
        show_subtask_relation
        show_plan
        show_message

"""
from planner4ucore.models import enums as me
from planner4ucore.models import models as mo


def show_task(task, oneline=False, header=None):
    if task is None:
        return

    if header:
        print(header)

    task = mo.get_representative_model(task, mo.TASK_ENUM_TRANSFORMER)
    if oneline:
        print(" {task_id} - {title} ({task_type}, {status})".format(**vars(task)))
    else:
        print(("  {title}\n"
               " Task id: {task_id}\n"
               " Type: {task_type}\n"
               " Description: {description}\n"
               " Created on: {creation_date}\n"
               " Priority: {priority}\n"
               " Status: {status}\n"
               " Close on last subtask execution: {last_subtask_dependency}\n"
               " Priority on overdue: {priority_on_overdue}")
              .format(**vars(task)))


def show_condition_relation(condition, header=None):
    if condition is None:
        return

    if header:
        print(header)

    condition = mo.get_representative_model(condition, mo.CONDITION_TASK_RELATION_ENUM_TRANSFORMER)
    print((" Relation id: {relation_id}\n"
           " Defined task id: {defined_task_id}\n"
           " Trigger task id: {condition_task_id}\n"
           " Trigger task status: {condition_task_status}")
          .format(**vars(condition)))


def show_date(date, oneline=False, header=None):
    if date is None:
        return

    if header:
        print(header)

    date = mo.get_representative_model(date, mo.TASK_DATE_ENUM_TRANSFORMER)
    if oneline:
        print(" {date_id} - {date_type} {date}".format(**vars(date)))
    else:
        print((" Task date id: {date_id}\n"
               " Type: {date_type}\n"
               " Date: {date}")
              .format(**vars(date)))


def show_group(group, oneline=False, header=None):
    if group is None:
        return

    if header:
        print(header)

    if oneline:
        print(" {} - {}".format(group.group_id, group.title))
    else:
        print((" Group id: {group_id}\n"
               " Title: {title}")
              .format(**vars(group)))


def show_user(user, header=None):
    if user is None:
        return

    if header:
        print(header)

    print(" {user_id} - {name}".format(**vars(user)))


def show_reminder(reminder, oneline=False, header=None):
    if reminder is None:
        return
    reminder = mo.get_representative_model(reminder, mo.REMINDER_ENUM_TRANSFORMER)

    if header:
        print(header)

    if oneline:
        print((" {reminder_id} - (task id: {task_id}, remind date: {raise_date}, time: {raise_time},"
               " days interval: {days_interval}, active: {active})".format(**vars(reminder))))
    else:
        print((" Reminder id: {reminder_id}\n"
               " Task id: {task_id}\n"
               " Next remind date: {raise_date}\n"
               " Time: {raise_time}\n"
               " Interval in days: {days_interval}\n"
               " Active: {active}")
              .format(**vars(reminder)))


def show_notification(notification, oneline=False, header=None):
    if notification is None:
        return

    if header:
        print(header)

    notification = mo.get_representative_model(notification, mo.NOTIFICATION_ENUM_TRANSFORMER)
    if oneline:
        print(" {notification_id} - (used: {used}, date id: {date_id}, days to date {days_to_date}, time: {time})"
              .format(**vars(notification)))
    else:
        print((" Notification id: {notification_id}\n"
               " Used: {used}\n"
               " Date id: {date_id}\n"
               " Days to date: {days_to_date}\n"
               " Time: {time}")
              .format(**vars(notification)))


def show_task_group_relation(relation, header=None):
    if relation is None:
        return

    if header:
        print(header)

    print((" Relation id: {relation_id}\n"
           " Task id: {task_id}\n"
           " Group id: {group_id}")
          .format(**vars(relation)))


def show_user_executor_relation(relation, header=None):
    if relation is None:
        return

    if header:
        print(header)

    print((" Executor id: {user_executor_id}\n"
           " Task id: {task_id}")
          .format(**vars(relation)))


def show_subtask_relation(relation, header=None):
    if relation is None:
        return

    if header:
        print(header)

    relation = mo.get_representative_model(relation, mo.SUBTASK_RELATION_ENUM_TRANSFORMER)

    print((" Relation id: {relation_id}\n"
           " Parent task id: {parent_task_id}\n"
           " Sub task id: {sub_task_id}\n"
           " Relation type: {relation_type}")
          .format(**vars(relation)))


def show_plan(plan, header=None):
    if plan is None:
        return

    if header:
        print(header)

    plan = mo.get_representative_model(plan, mo.PLAN_ENUM_TRANSFORMER)

    print((" Plan id: {plan_id}\n"
           " Active: {active}\n"
           " Template task id: {task_id}\n"
           " Next creation date: {create_task_date}\n"
           " Repeated times: {repetitions_count}\n"
           " Repetitions type: {repeat_type}\n"
           " Interval: {plan_interval}\n"
           " End type: {end_type}")
          .format(**vars(plan)))

    if plan.end_type == me.PLAN_END_NUMBER:
        print(" Times to repeat: {}".format(plan.repetitions_number))
    elif plan.end_type == me.PLAN_END_DATE:
        print(" Will be turned off after: {}".format(plan.end_date))


def show_message(message, oneline=False, header=None):
    if message is None:
        return

    if header:
        print(header)

    if oneline:
        print(" {task_message_id} - {message_title} ".format(**vars(message)))
    else:
        print((" Title: {message_title}\n"
               " Message: \n"
               " {message_text}\n"
               " Date: {date}\n"
               " Time: {time}"
               .format(**vars(message))))
