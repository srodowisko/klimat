from setuptools import setup, find_packages


setup(
    name="Planner4ULibrary",
    version="2.3",
    description="Core library for Planner4U",
    packages=find_packages(),
    test_suite="planner4ucore.services.tests.test_runner.get_services_test_suite",
    install_requires = ["sqlalchemy"]
)