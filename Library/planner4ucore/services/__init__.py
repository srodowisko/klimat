"""
    This package provides modules to work with library

    Modules:
        services - provides data access and interaction
        constraints - contains constraints for data
        exceptions - lists exceptions that can rise within this package
        log_functions - contains log functions for this library
        configurations - provides classes and methods for configurations

    How to use it:
        >>> import planner4ucore
        >>> from planner4ucore.services import services
        >>> from planner4ucore.storage import sqlalchemy_storage
        >>> connection_string = "mysql://USERNAME:PASSWORD@HOST:PORT/DB_NAME?charset=utf8"
        >>> data_storage = sqlalchemy_storage.SQLAlchemyStorage(connection_string)
        >>> service = services.Service(data_storage)
        >>> from planner4ucore.models import models as mo
        >>> user = service.add_user(mo.User("sergio"))
        >>> task = service.add_task(mo.Task(user.user_id, "Eat, pray, love, code"))
        >>> service.get_user_tasks(user.user_id)[0].title
        'Eat, pray, love, code'

"""

